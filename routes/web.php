<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group([
    'middleware' => 'auth',
    'prefix'     => 'admin',
    'as'         => 'admin.',
    'namespace'  => 'Admin',
], function() {
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    // staff roles
    Route::resource('staff/roles', 'RoleController');
    // staff privileges
    Route::resource('staff/privileges', 'PrivilegeController');
    // staff
    Route::resource('staff', 'StaffController');
    Route::post('staff/{staff}/suspend', 'StaffController@suspend')->name('staff.suspend');
    Route::post('staff/{staff}/unsuspend', 'StaffController@unsuspend')->name('staff.unsuspend');
    // staff nok
    Route::get('staff/{staff}/noks', 'StaffNokController@index')->name('staff.noks');
    Route::get('staff/{staff}/noks/create', 'StaffNokController@create')->name('staff.noks.create');
    Route::post('staff/{staff}/noks/store', 'StaffNokController@store')->name('staff.noks.store');
    Route::get('staff/{staff}/noks/{nok}' , 'StaffNokController@show')->name('staff.noks.show');
    Route::get('staff/{staff}/noks/{nok}/edit', 'StaffNokController@edit')->name('staff.noks.edit');
    Route::put('staff/{staff}/noks/{nok}/update', 'StaffNokController@update')->name('staff.noks.update');
    Route::delete('staff/{staff}/noks/{nok}/destroy', 'StaffNokController@destroy')->name('staff.noks.destroy');
    // branches
    Route::post('staff/{staff}/branches', 'StaffController@updateBranches')->name('staff.branches.update');
    // staff role and privileges..
    Route::get('staff/{staff}/roles','StaffRoleController@index')->name('staff.roles');
    Route::put('staff/{staff}/roles', 'StaffRoleController@update')->name('staff.roles.update');
    Route::get('staff/{staff}/roles/edit', 'StaffRoleController@edit')->name('staff.roles.edit');
    Route::put('staff/{staff}/privileges', 'StaffRoleController@updatePrivileges')->name('staff.privileges.update');

    // customers
    Route::resource('customers', 'CustomerController');
    Route::get('customers/{customer}/vehicles', 'CustomerVehicleController@index')->name('customers.vehicles');

    // branch
    Route::resource('settings/branches', 'BranchController');
    Route::get('settings/branches/{branch}/staff', 'BranchController@staff')->name('branches.staff');
    Route::get('settings/branches/{branch}/customers', 'BranchController@customers')->name('branches.customers');
    // services
    Route::resource('settings/services', 'ServiceController');

    // product type
    Route::resource('products/product-type', 'ProductTypeController');

    // product
    Route::resource('products', 'ProductController');

    // vehicle fuels
    Route::resource('vehicles/vehicle-fuels', 'VehicleFuelController');
    // vehicle make
    Route::resource('vehicles/vehicle-makes', 'VehicleMakeController');
    // vehicles
    Route::resource('vehicles', 'VehicleController');
    // supplies
    Route::resource('supplies', 'SupplyController');

    Route::post('supplies/{supply}/confirm', 'SupplyController@confirmSupply')->name('supplies.confirm');
    Route::post('supplies/{supply}/items', 'SupplyItemController@store')->name('supplies.add.item');
    Route::get('supplies/{supply}/items/{id}', 'SupplyItemController@show')->name('supplies.show.item');
    Route::put('supplier/{supply}/items/{id}', 'SupplyItemController@update')->name('supplies.update.item');
    Route::delete('supplier/{supply}/items/{id}', 'SupplyItemController@destroy')->name('supplies.destroy.item');
    // suppliers
    Route::resource('suppliers', 'SupplierController');
    // job order
    Route::resource('job-orders', 'JobOrderController');
    Route::post('job-orders/{order}/manager', 'JobOrderController@assignManager')->name('job-orders.manager');

    Route::get('job-orders/{order}/inspection', 'JobOrderInspectionController@index')->name('job-orders.inspection');
    Route::put('job-orders/{order}/inspection', 'JobOrderInspectionController@update')->name('job-orders.inspection.update');
    Route::post('job-orders/{order}/inspection/results', 'JobOrderInspectionController@addItem')->name('job-orders.inspection.add-item');
    Route::put('job-orders/{order}/inspection/results/{result}', 'JobOrderInspectionController@updateItem')->name('job-orders.inspection.update-item');
    Route::delete('job-orders/{order}/inspection/results/{result}', 'JobOrderInspectionController@removeItem')->name('job-orders.inspection.remove-item');

    Route::get('job-orders/{order}/faults', 'JobOrderFaultController@index')->name('job-orders.faults');
    Route::post('job-orders/{order}/faults', 'JobOrderFaultController@store')->name('job-orders.faults.store');
    Route::get('job-orders/{order}/faults/create', 'JobOrderFaultController@create')->name('job-orders.faults.create');
    Route::put('job-orders/{order}/faults/{id}', 'JobOrderFaultController@update')->name('job-orders.faults.update');
    Route::delete('job-orders/{order}/faults/{id}', 'JobOrderFaultController@destroy')->name('job-orders.faults.destroy');

    Route::get('job-orders/{order}/services', 'JobOrderServiceController@index')->name('job-orders.services');
    Route::post('job-orders/{order}/services', 'JobOrderServiceController@store')->name('job-orders.services.store');
    Route::get('job-orders/{order}/services/{service}', 'JobOrderServiceController@show')->name('job-orders.services.show');
    Route::put('job-orders/{order}/services/{service}', 'JobOrderServiceController@update')->name('job-orders.services.update');
    Route::delete('job-orders/{order}/services/{service}', 'JobOrderServiceController@destroy')->name('job-orders.services.destroy');
    Route::get('job-orders/{order}/services/{service}/edit', 'JobOrderServiceController@edit')->name('job-orders.services.edit');

});