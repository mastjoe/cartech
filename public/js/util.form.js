let submitFormData = function(data) {

    const form = $(data.form);

    let formData = form.serialize();
    const btn           = form.find('button[type="submit"]');
    const btnOldContent = btn.html();
    let   url         = form.attr('action');
    let   method      = form.attr('method').toLowerCase();
    let   loadingText = "...";
    let   resp        = false;
    const respTarget  = form.find('.form_response');

    if (objectHasProp('url', data)) {
        url = data.url;
    }

    if (objectHasProp('btnLoadingText', data)) {
        loadingText = data.btnLoadingText;
    }

    if (objectHasProp('response', data)) {
        resp = true;
    }

    if (objectHasProp('method', data)) {
        method = data.method;
    }

    if (
        method.toLowerCase() == "post" &&
        form.hasAttr('enctype') && 
        form.attr('enctype') == "multipart/form-data"
    ) {
        formData = new FormData(form[0]);
    }

    btn.html(loadingText).prop('disabled', true);

    if (objectHasProp('beforeSubmit', data)) {
        data.beforeSubmit();
    }

    return new Promise(function(resolve, reject) {
        axios[method](url, formData)
        .then(function({data}) {
            resolve(data);
        })
        .catch(function({response}) {
            if (resp) {
                handleAxiosFormError(form, response, respTarget);
            } else {
                handleAxiosFormError(form, response);
            }
            reject(response);
        })
        .finally(function() {
            btn.html(btnOldContent).prop('disabled', false);
        });
    });
}