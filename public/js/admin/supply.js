const deleteSupplyBtn = $('.delete_supply_btn');

const addItemModal = $('#add_item_modal');
const addItemBtn = $('.add_item_btn');

const itemDetailsBtn = $('.item_details_btn');
const itemDetailsModal = $('#item_details_modal');

const deleteItemBtn = $('.delete_item_btn');

const confirmSupplyBtn = $('.confirm_supply_btn');

const showAddItemModal = function() {
    addItemModal.modal({backdrop: 'static'});
}

const hideAddItemModal = function() {
    addItemModal.modal('hide');
}

$(document).ready(function() {
    formLabelFocus();

    // handle datatable
    if (document.querySelectorAll('.datatable').length) {
        simpleDataTable('.datatable');
    }

    // delete supply function
    deleteSupplyBtn.on('click', function () {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to delete supply with reference '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // show add item modal
    addItemBtn.on('click', function() {
        showAddItemModal();
    });

    // submit add item form
    addItemModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        let method = "post";
        if ($('input[name="_method"]').length) {
            method = 'put'
        }

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: method,
        })
            .then(function(data) {
                if (data.success) {
                    thisForm.get(0).reset();
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // item details
    itemDetailsBtn.on('click', function() {
        const thisBtn = $(this);
        const product = thisBtn.data('product');
        const item = thisBtn.data('item');
        const productType = thisBtn.data('productType');

        itemDetailsModal.find('td.product').text(product.name);
        itemDetailsModal.modal({backdrop: 'static'});
        
    });

    // delete supply item btn
    deleteItemBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete supply item '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // confirm supply fxn
    confirmSupplyBtn.on('click', function() {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to confirm supply with reference, '+name+'?',
            method: 'post',
            confirmBtnClass: 'btn-success',
            confirmBtnText: 'Yes, Confirm',
            url: url,
        });
    });
});