const addProductModal = $('#add_product_modal');
const addProductBtn = $('.add_product_btn');

const editProductBtn = $('.edit_product_btn');
const editProductModal = $('#edit_product_modal');

const deleteProductBtn = $('.delete_product_btn');

const showAddProductModal = function() {
    addProductModal.modal({backdrop: 'static'})
}

const hideAddProductModal = function() {
    addProductModal.modal('hide');
}

const showEditProductModal = function() {
    editProductModal.modal({backdrop: 'static'});
}

const hideEditProductModal = function() {
    editProductModal.modal('hide');
}

$(document).ready(function() {
    formLabelFocus();

    // datatable
    if(document.querySelector('.datatable')) {
        simpleDataTable('.datatable');
    }

    // show add product modal
    addProductBtn.on('click', function() {
        showAddProductModal();
    });

    // submit add product form
    addProductModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        let method = "post";

        if (thisForm.find('input[name="_method"]').length) {
            method = "put";
        }

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: method,
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });

    });


    // delete product function
    deleteProductBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete product, '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // edit product function 
    editProductBtn.on('click', function() {
        showEditProductModal();
    });

    // submission of edit form
    editProductModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    })
    
})