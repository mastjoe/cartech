const deleteUserBtn = $('.delete_user_btn');
const suspendUserBtn = $('.suspend_user_btn');
const unsuspendUserBtn = $('.unsuspend_user_btn');
const deleteNokBtn = $('.delete_nok_btn');

const assignBranchModal = $('#assign_branches_modal');
const assignBranchBtn = $('.assign_branches_btn');

const showAssignBranchModal = function() {
    assignBranchModal.modal({backdrop: 'static'});
}

$(document).ready(function() {
    formLabelFocus();
    // delete user 
    deleteUserBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete staff '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // suspend user fxn
    suspendUserBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to suspend staff '+name+'?',
            method: 'post',
            confirmBtnText: 'Yes, Suspend',
            confirmBtnClass: 'btn-warning',
            url: url,
        });
    });

    // unsuspend user fxn
    unsuspendUserBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');
        pushRequest({
            dialogText: 'you are about to unsuspend staff '+name+'?',
            method: 'post',
            confirmBtnText: 'Yes, Unsuspend',
            confirmBtnClass: 'btn-success',
            url: url,
        });
    });
    
    // delete nok 
    deleteNokBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete next of kin '+name+'?',
            method: 'delete',
            url: url,
        });
    })

    // drop_uploader..
    if (document.querySelectorAll('#image').length) {

        $('#image').drop_uploader({
            uploader_text: 'Drop image to upload, or',
            browse_css_class: 'btn btn-dark',
        });
    }

    // show assign branch modal
    assignBranchBtn.on('click', function() {
        showAssignBranchModal();
    });

    // assign branches to staff
    assignBranchModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

});