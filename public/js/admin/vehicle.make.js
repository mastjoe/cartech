const addVehicleMakeModal = $('#add_vehicle_make_modal');
const addVehicleMakeBtn = $('.add_vehicle_make_btn');

const deleteVehicleMakeBtn = $('.delete_vehicle_make_btn');

const editVehicleMakeBtn = $('.edit_vehicle_make_btn');
const editVehicleMakeModal = $('#edit_vehicle_make_modal');

const showAddVehicleMakeModal = function() {
    addVehicleMakeModal.modal({backdrop: 'static'});
}

const hideAddVehicleMakeModal = function() {
    addVehicleMakeModal.modal('hide');
}

const showEditVehicleMakeModal = function() {
    editVehicleMakeModal.modal({backdrop: 'static'});
}

const hideEditVehicleMakeModal = function() {
    editVehicleMakeModal.modal('hide');
}

$(document).ready(function() {
    formLabelFocus();

    simpleDataTable('.datatable');

    // add vehicle make modal
    addVehicleMakeBtn.on('click', function() {
        showAddVehicleMakeModal();
    });

    // submission of make form
    addVehicleMakeModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if(data.success) {
                    hideAddVehicleMakeModal();
                    thisForm.get(0).reset();
                    refreshPage();
                }
            })
            .catch(function(e) {
                console.log(e);
            });

    });

    // delete vehicle make
    deleteVehicleMakeBtn.on('click', function() {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to delete vehice make '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // edit vehicle make function
    editVehicleMakeBtn.on('click', function() {
        showEditVehicleMakeModal();
    });

    // submit edit form
    editVehicleMakeModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if(data.success) {
                    hideEditVehicleMakeModal();
                    thisForm.get(0).reset();
                    refreshPage();
                }
            })
            .catch(function(e) {
                console.log(e);
            });

    });
});