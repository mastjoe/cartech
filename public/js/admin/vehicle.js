const deleteVehicleBtn = $('.delete_vehicle_btn');

$(document).ready(function() {
    formLabelFocus();

    if (document.querySelector('.datatable')) {
        simpleDataTable('.datatable');
    }

    // delete vehicle btn function
    deleteVehicleBtn.on('click', function() {
        const thisBtn = $(this);
        const name = thisBtn.data('name');
        const url = thisBtn.data('url');

        pushRequest({
            dialogText: 'you are about to delete vehicle with plate number, '+name+'?',
            method: 'delete',
            url: url,
        });
    });
})