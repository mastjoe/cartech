const addProductTypeBtn = $('.add_product_type');
const addProductTypeModal = $('#add_product_type_modal');

const deleteProductTypeBtn = $('.delete_product_type');

const showAddProductTypeModal = function() {
    addProductTypeModal.modal({backdrop: 'static'});
}

const hideAddProductTypeModal = function() {
    addProductTypeModal.modal('hide');
}

$(document).ready(function() {
    formLabelFocus();

    // datatable
    if(document.querySelectorAll('.datatable')) {
        simpleDataTable('.datatable');
    }

    // show add product type modal
    addProductTypeBtn.on('click', function() {
        showAddProductTypeModal();
    });

    // submission of add product form
    addProductTypeModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        let method = "post";

        if (thisForm.find('input[name="_method"]').length) {
            method = "put"
        }

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: method,
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete product type btn
    deleteProductTypeBtn.on('click', function() {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to delete product type '+name+'?',
            method: 'delete',
            url: url,
        });
    });
});