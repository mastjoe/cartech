const deleteCustomerBtn = $('.delete_customer_btn');


$(document).ready(function() {
    formLabelFocus();

    if (document.querySelector('.datatable')) {
        simpleDataTable('.datatable');        
    }
    
    // delete customer fxn
    deleteCustomerBtn.on('click', function () {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete customer '+name+'?',
            method: 'delete',
            url: url,
        });
    })
});