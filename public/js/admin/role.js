const deleteRoleBtn = $('.delete_role_btn');


$(document).ready(function() {
    formLabelFocus();

    simpleDataTable('.datatable');

    // delete role function
    deleteRoleBtn.on('click', function() {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to delete role '+name+'?',
            method: 'delete',
            url: url,
        });
    });

});