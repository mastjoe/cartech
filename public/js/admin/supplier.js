const deleteSupplierBtn = $('.delete_supplier_btn');

$(document).ready(function() {
    formLabelFocus();

    // deletion operation
    deleteSupplierBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete supplier '+name+'?',
            method: 'delete',
            url: url,
        });
    });
});