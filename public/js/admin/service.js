const addServiceBtn = $('.add_service_btn');
const addServiceModal = $('#add_service_modal');

const showAddServiceModal = function() {
    addServiceModal.modal({backdrop: 'static'});
}

const hideAddServiceModal = function() {
    addServiceModal.modal('hide');
}

const editServiceModal = $('#edit_service_modal');
const editServiceBtn = $('.edit_service_btn');

const showEditServiceModal = function() {
    editServiceModal.modal({backdrop:'static'});
}

const hideEditServiceModal = function() {
    editServiceModal.modal('hide');
}

const deleteServiceBtn = $('.delete_service_btn');

$(document).ready(function() {

    formLabelFocus();

    // handle datatable
    if (document.querySelectorAll('.datatable').length) {
        simpleDataTable('.datatable');
    }

    // add service modal
    addServiceBtn.on('click', function() {
        showAddServiceModal();
    });

    // submit add service form
    addServiceModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...'
        })
            .then(function(data) {
                if (data.success) {
                    thisForm.get(0).reset();
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // edit service modal
    editServiceBtn.on('click', function() {
        showEditServiceModal();
    });

    // submit edit service modal
    editServiceModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if (data.success) {
                    thisForm.get(0).reset();
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete service fxn
    deleteServiceBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete service, '+name+'?',
            method: 'delete',
            url: url,
        });
    });

});