const editPrivilegeBtn = $('.edit_privilege_btn');
const editPrivilegeModal = $('#edit_privilege_modal');

const showEditPrivilegeModal = function() {
    editPrivilegeModal.modal({backdrop: 'static'});
}

const hideEditPrivilegeModal = function() {
    editPrivilegeModal.modal('hide');
}


$(document).ready(function() {
    // show edit privilege modal
    editPrivilegeBtn.on('click', function() {
        showEditPrivilegeModal();
    });

    // submit edit privilege form
    editPrivilegeModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });
})