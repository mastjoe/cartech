const addServiceBtn = $('.add_service_btn');

const addServiceModal = $('#add_service_modal');

const showAddServiceModal = function() {
    addServiceModal.modal({backdrop: 'static'});
}

const hideAddServiceModal = function() {
    addServiceModal.modal('hide');
}

const deleteServiceBtn = $('.delete_service_btn');

const editServiceBtn = $('.edit_service_btn');
const editServiceModal = $('#edit_service_modal');

const showEditServiceModal = function() {
    editServiceModal.modal({backdrop:'static'});
}

const hideEditServiceModal = function() {
    editServiceModal.modal('hide');
}


$(document).ready(function() {
    // form label focus
    formLabelFocus();

    // add service fxn
    addServiceBtn.on('click', function() {
        showAddServiceModal();
    });

    // submit add service form
    addServiceModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    $('#outsourced').on('change', function() {
        const outsourcedTarget = $('.outsource_target');

        if ($(this).is(':checked')) {
            outsourcedTarget.removeClass('d-none');
            outsourcedTarget.find('.form-control').prop('required', true);
        } else {
            outsourcedTarget.addClass('d-none');
            outsourcedTarget.find('.form-control').prop('required', false);
        }
    });

    // delete service btn
    deleteServiceBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to remove service '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // show edit service btn
    editServiceBtn.on('click', function() {
        showEditServiceModal();
    });

    // submit edit service form
    editServiceModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

});