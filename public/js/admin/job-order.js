
const showCustomerDetailWhenVehicleIsPicked = function(selectTarget) {
    $('.hiddeable-form-group').addClass('d-none');
    if (selectTarget.val() != "") {
        const selectedOption = selectTarget.find('option:selected');
        const optionName = selectedOption.data('name');
        const optionPhone = selectedOption.data('phone');
        $('#customerPhone').val(optionPhone);
        $('#customerName').val(optionName);

        $('.hiddeable-form-group').removeClass('d-none');
    }
}

const assignManagerBtn = $('.assign_manager_btn');
const assignManagerModal = $('#assign_manager_modal');

const showAssignManagerModal = function() {
    assignManagerModal.modal({backdrop: 'static'});
}

const hideAssignManagerModal = function() {
    assignManagerModal.modal('hide');
}

const deleteJobOrderBtn = $('.delete_job_order_btn');

const updateInspectionModal = $('#update_inspection_modal');
const updateInspectionBtn = $('.update_inspection_btn');

const showUpdateInspectionModal = function() {
    updateInspectionModal.modal({backdrop: 'static'});
}

const hideUpdateInspectionModal = function() {
    updateInspectionModal.modal('hide');
}

const addResultBtn = $('.add_result_btn');
const updateResultModal = $('#update_result_modal');

const addResultModal = $('#add_result_modal');

const showAddResultModal = function() {
    addResultModal.modal({backdrop: 'static'});
}

const hideAddResultModal = function () {
    addResultModal.modal('hide');
}

const deleteResultBtn = $('.delete_result_btn');
const updateResultBtn = $('.update_result_btn');

const addFaultModal = $('#add_fault_modal');
const addFaultBtn = $('.add_fault_btn');

const showAddFaultModal = function() {
    addFaultModal.modal({backdrop: 'static'});
}

const hideAddFaultModal = function() {
    addFaultModal.modal('hide');
}

const deleteFaultBtn = $('.delete_fault_btn');
const updateFaultBtn = $('.update_fault_btn');
const updateFaultModal = $('#update_fault_modal');

const showUpdateFaultModal = function() {
    updateFaultModal.modal({backdrop: 'static'});
}

const hideUpdateFaultModal = function() {
    updateFaultModal.modal('hide');
}

$(document).ready(function() {
    // form label focus
    formLabelFocus();

    // assign manager modal
    assignManagerBtn.on('click', function() {
        showAssignManagerModal();
    });

    // submit assign manager form
    assignManagerModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete job order fxn
    deleteJobOrderBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete job order with reference '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // update inspection modal
    updateInspectionBtn.on('click', function() {
        showUpdateInspectionModal();
    });

    // submission of update form
    updateInspectionModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        const method = "put";

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: method,
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // add reult modal fx
    addResultBtn.on('click', function() {
        showAddResultModal();
    });

    // submission of add result form
    addResultModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete result fxn
    deleteResultBtn.on('click', function() {
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to remove result item ?',
            method: 'delete',
            url: url,
        });
    });

    // update result function
    updateResultBtn.on('click', function() {
        const url = $(this).data('url');
        const result = $(this).data('result');
        updateResultModal.find('textarea').val(result);
        updateResultModal.find('form').attr('action', url);
        updateResultModal.modal({backdrop:'static'});
    });

    // aubmission of the update result
    updateResultModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put'
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // show add fault modal...
    addFaultBtn.on('click', function() {
        showAddFaultModal();
    });

    // submit add fault form
    addFaultModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // delete fault function
    deleteFaultBtn.on('click', function() {
        const url = $(this).data('url');
        const name = $(this).data('name');

        pushRequest({
            dialogText: 'you are about to delete job order fault '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // update fault function
    updateFaultBtn.on('click', function() {
        const url = $(this).data('url');
        const fault = $(this).data('fault');

        const form = updateFaultModal.find('form');
        form.attr('action', url);
        form.find('input[name=\'fault\'').val(fault);
        showUpdateFaultModal();
    });

    // submission of update form 
    updateFaultModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
        })
            .then(function(data) {
                if (data.success) {
                    refreshPage(100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    })
});