const addFuelBtn = $('.add_fuel_btn')
const addFuelModal = $('#add_fuel_modal');

const editFuelBtn = $('.edit_fuel_btn');
const editFuelModal = $('#edit_fuel_modal');


const deleteFuelBtn = $('.delete_fuel_btn');

const showAddFuelModal = function() {
    addFuelModal.modal({backdrop: 'static'});
}

const hideAddFuelModal = function() {
    addFuelModal.modal('hide');
}

const showEditFuelModal = function() {
    editFuelModal.modal({backdrop: 'static'});
}

const hideEditFuelModal = function() {
    editFuelModal.modal('hide');
}

$(document).ready(function() {
    formLabelFocus();

    if (document.querySelector('.datatable')) {
        simpleDataTable('.datatable');
    }

    // add fuel modal
    addFuelBtn.on('click', function() {
        showAddFuelModal();
    });

    // delete fuel btn
    deleteFuelBtn.on('click', function() {
        const name = $(this).data('name');
        const url = $(this).data('url');

        pushRequest({
            dialogText: 'you are about to delete vehicle fuel '+name+'?',
            method: 'delete',
            url: url,
        });
    });

    // submit add fuel form
    addFuelModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);
        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'post',
        })
            .then(function(data) {
                if (data.success) {
                    hideAddFuelModal();
                    thisForm.get(0).reset();

                    delayRedirect|(data.redirect, 100);
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

    // edit fueld modal
    editFuelBtn.on('click', function() {
        showEditFuelModal();
    });

    // submit edit fuel form
    editFuelModal.find('form').on('submit', function(e) {
        e.preventDefault();
        const thisForm = $(this);

        submitFormData({
            form: thisForm,
            btnLoadingText: 'Saving...',
            method: 'put',
        })
            .then(function(data) {
                if (data.success) {
                    hideEditFuelModal();
                    refreshPage(100)
                }
            })
            .catch(function(e) {
                console.log(e)
            });
    });

})