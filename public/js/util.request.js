let pushRequest = function (data) {
    if (!objectHasProp('dialogTitle', data)) {
        data.dialogTitle = 'Are you sure?';
    }

    if (!objectHasProp('dialogText', data)) {
        data.dialogText = `you are about deleting item`;
    }

    if (!objectHasProp('dialogType', data)) {
        data.dialogType = `question`;
    }

    if (!objectHasProp('confirmBtnText', data)) {
        data.confirmBtnText = `Yes, Delete`;
    }

    if (!objectHasProp('confirmBtnClass', data)) {
        data.confirmBtnClass = `btn-danger`;
    }

    if (!objectHasProp('showCloseBtn', data)) {
        data.showCloseButton = false;
    }

    if (!objectHasProp('successTitle', data)) {
        data.successTitle = 'Delete Successful';
    }

    if (!objectHasProp('method', data)) {
        data.method = "post";
    }

    if (objectHasProp('html', data)) {
        data.text = null;
    }

    let errorAlert = function () {
        return swal.fire(
            'Oops Error',
            'Something went wrong!',
            'error',
        );
    }

    if (objectHasProp('onBefore', data)) {
        data.onBefore();
    }

    swal.fire({
            title: data.dialogTitle,
            text: data.dialogText,
            html: data.html,
            icon: data.dialogType,
            showCancelButton: true,
            showLoaderOnConfirm: true,
            showCloseButton: data.showCloseBtn,
            confirmButtonText: data.confirmBtnText,
            customClass: {
                confirmButton: 'btn mr-2 ' + data.confirmBtnClass,
                cancelButton: 'btn btn-secondary',
            },
            buttonsStyling: false,
            preConfirm: function () {
                return axios[data.method](data.url)
                    .then((data) => {
                        return data;
                    })
                    .catch((err) => {
                        console.log(err.response);
                        if (objectHasProp('onError', data)) {
                            data.onError();
                        }
                        return errorAlert();
                    });
            },
            allowOutsideClick: () => !swal.isLoading()
        })
        .then((res) => {
            if (res.value) {
                if (objectHasProp('redirect_url', res.value.data)) {
                    // delayRedirect(res.value.data.redirect_url, 4000);
                    delayRedirect(res.value.data.redirect_url, 100);
                }
                if (objectHasProp('redirect', res.value.data)) {
                    delayRedirect(res.value.data.redirect, 100);
                }
                if (objectHasProp('onSuccess', data)) {
                    data.onSuccess(res.value.data);
                }
            }
        });
}