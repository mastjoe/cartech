@extends('layouts.admin.app')

@section('title', 'All Suppliers')
@section('page_title', 'All Suppliers')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Suppliers
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($suppliers->isNotEmpty())
                <div class="text-right mb-2">
                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.suppliers.create') }}">New Supplier</a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Suppliers</h4>
                        <p class="font-13 sub-header"></p>
                        <div>
                            <table class="table table-hover nowrap w-100 dt-responsive datatable">
                                <thead>
                                    <tr>
                                        <th>Supplier</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Supplies</th>
                                        <th>Added</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($suppliers as $supplier)
                                        <tr>
                                            <td><a href="{{ route('admin.suppliers.show', $supplier->id) }}">{{ $supplier->name }}</a></td>
                                            <td>{{ $supplier->phone }}</td>
                                            <td>{{ $supplier->email }}</td>
                                            <td>0</td>
                                            <td>{{ $supplier->created_at->diffForHumans() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted my-2">No supplier found!</h2>
                    <a href="{{ route('admin.suppliers.create') }}" class="btn btn-secondary waves-effect">Add New Supplier</a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/supplier.js') }}"></script>
@endpush