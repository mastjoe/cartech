@extends('layouts.admin.app')

@section('title', 'Edit Supplier - '.$supplier->name)
@section('page_title', 'Edit Supplier - '.$supplier->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.suppliers.index') }}">Suppliers</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.suppliers.show', $supplier->id) }}">{{ $supplier->reference }}</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.suppliers.update', $supplier->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title">
                            Edit Supplier
                        </h4>
                        <p class="font-13 sub-header">Update supplier's record</p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <x-form.input
                                        name="name"
                                        id="name"
                                        value="{{ old('name') ?? $supplier->name }}"
                                        required
                                    ></x-form.input>
                                </div>
                                
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        value="{{ old('phone') ?? $supplier->phone }}"
                                        type="tel"
                                        required
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        id="email"
                                        value="{{ old('email') ?? $supplier->email }}"
                                        type="email"
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <x-form.input
                                        name="website"
                                        id="website"
                                        type="url"
                                        value="{{ old('website') ?? $supplier->website }}"
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="addresss"
                                    >{{ old('address') ?? $supplier->address }}</x-form.textarea>
                                </div>
    
                                <div class="form-group text-center my-4">
                                    <button class="btn btn-secondary waves-effect px-5 btn-lg" type="submit">
                                        Save Record
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/supplier.js') }}"></script>
@endpush