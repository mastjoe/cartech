@extends('layouts.admin.app')

@section('title', 'New Supplier')
@section('page_title', 'New Supplier')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.suppliers.index') }}">Suppliers</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.suppliers.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <h4 class="header-title">
                            New Supplier
                        </h4>
                        <p class="font-13 sub-header">Fill form fields to create a supplier's record</p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <x-form.input
                                        name="name"
                                        id="name"
                                        value="{{ old('name') }}"
                                        required
                                    ></x-form.input>
                                </div>
                                
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        value="{{ old('phone') }}"
                                        type="tel"
                                        required
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        id="email"
                                        value="{{ old('email') }}"
                                        type="email"
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="website">Website</label>
                                    <x-form.input
                                        name="website"
                                        id="website"
                                        type="url"
                                        value="{{ old('website') }}"
                                    ></x-form.input>
                                </div>
    
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="addresss"
                                    >{{ old('address') }}</x-form.textarea>
                                </div>
    
                                <div class="form-group text-center my-4">
                                    <button class="btn btn-secondary waves-effect px-5 btn-lg" type="submit">
                                        Save Record
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/supplier.js') }}"></script>
@endpush