@extends('layouts.admin.app')

@section('title', 'Supplier - '.$supplier->name)
@section('page_title', 'Supplier - '.$supplier->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.suppliers.index') }}">Suppliers</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $supplier->reference }}
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="jumbotron profile-jumbotron jumbotron-bg-1">
        <div class="text-center">
            <div class="py-2">
                <h2 class="text-white">{{ $supplier->name }}</h2>
                @if ($supplier->email)                    
                    <p><a class="text-warning" href="mailto:{{ $supplier->email }}">{{ $supplier->email }}</a></p>
                @endif
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 order-md-2 order-1">
            <button class="btn btn-secondary btn-block d-md-none mb-3"><i class="fa fa-nav"></i>Toggle Actions</button>
            <div class="mobile-menu d-none d-md-block">
                <a href="{{ route('admin.suppliers.edit', $supplier->id) }}" class="btn btn-warning btn-block waves-effect mb-2">
                    Edit 
                </a>
                <a href="" class="btn btn-secondary waves-effect btn-block mb-2">
                    Supplies
                </a>
                @if ($supplier->canDelete())                    
                <button class="btn btn-danger btn-block mb-2 waves-effect delete_supplier_btn"
                    data-name="{{ $supplier->name }}"
                    data-url="{{ route('admin.suppliers.destroy', $supplier->id) }}"
                >
                    Delete Account
                </button>
                @endif
            </div>
        </div>
        <div class="col-md-8 order-md-1 order-2">
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardPriv" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0"> Profile Information</h5>
                    <div id="cardPriv" class="collapse pt-3 show">
                        <h4 class="font-13 text-muted text-uppercase">Email :</h4>
                        <p class="mb-3">{{ $supplier->email }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Phone :</h4>
                        <p class="mb-3">{{ $supplier->phone }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Website :</h4>
                        <p class="mb-3">{{ $supplier->website }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Address :</h4>
                        <p class="mb-3">{{ $supplier->address ?? '-' }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Created :</h4>
                        <p class="mb-3">{{ $supplier->created_at->format('jS M, Y @ h:i a') ?? '-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/supplier.js') }}"></script>
@endpush