@extends('layouts.admin.app')

@section('title', 'Edit Role - '.$role->name)
@section('page_title', 'Edit Role - '.$role->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>

    <li class="breadcrumb-item">
        <a href="{{ route('admin.roles.index') }}">Roles</a>
    </li>

    <li class="breadcrumb-item">
        <a href="{{ route('admin.roles.show', $role->id) }}">{{ Str::limit($role->name, 15) }}</a>
    </li>

    <li class="breadcrumb-item active">
        Edit
    </li>    
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.roles.update', $role->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title">Edit Role</h4>
                        <p class="font-13 sub-header"></p>

                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <x-form.input
                                        name="name"
                                        id="name"
                                        value="{{ old('name') ?? $role->name }}"
                                        required
                                    ></x-form.input>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <x-form.textarea
                                        name="description"
                                        id="description"
                                    >{{ old('description') ?? $role->description }}</x-form.textarea>
                                </div>
                                <div class="form-group">
                                    <label for="privileges">Privileges</label>
                                    <x-form.select
                                        name="privileges[]"
                                        id="privileges"
                                        required
                                        multiple
                                    >
                                        @foreach ($privileges as $privilege)
                                            <option value="{{ $privilege->id }}"
                                                @if (old('privileges') && in_array($privilege->id, old('privileges')))
                                                    selected
                                                @elseif ($role->privileges->count() && in_array($privilege->id, $role->privileges->pluck('id')->toArray()))
                                                    selected
                                                @endif
                                            >
                                                {{ $privilege->name }}
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>
                                <div class="form-group my-4 text-center">
                                    <button class="btn btn-secondary px-5 waves-effect" type="submit">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/role.js') }}"></script>

    <script>
        $(document).ready(function() {
             // select2
            $('#privileges').select2({
                placeholder: 'Select privileges'
            });
        })
    </script>
@endpush