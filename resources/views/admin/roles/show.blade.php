@extends('layouts.admin.app')

@section('title', 'Role - '.$role->name)
@section('page_title', 'Role - '.$role->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.roles.index') }}">Roles</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($role->name, 15) }}
    </li>
@endpush

@push('css')
     <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card-box">
                <div class="my-1">
                    <p class="text-muted my-0">Role</p>
                    <h5 class="text-dark">{{ $role->name }}</h5>
                </div>
                <div class="my-3">
                    <p class="text-muted my-0">Description</p>
                    <h5 class="text-dark">{{ $role->description ?? '-' }}</h5>
                </div>
                <div class="my-3">
                    <p class="text-muted">Created</p>
                    <h5 class="text-dark">{{ $role->created_at->diffForHumans() }}</h5>
                </div>

                <div class="my-3">
                    <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-block btn-warning wave-effect mb-3">
                        <span class="fe-edit mr-1"></span>
                        Edit
                    </a>
                    <button class="btn btn-block btn-danger wave-effect mb-3 delete_role_btn"
                        data-url="{{ route('admin.roles.destroy', $role->id) }}"
                        data-name="{{ $role->name }}"
                    >
                        <span class="fe-delete mr-1"></span>
                        Delete
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            {{-- privileges --}}
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardPriv" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">
                        Privileges
                    </h5>
                    <div id="cardPriv" class="collapse pt-3 show">
                        @if ($role->privileges->isNotEmpty())
                            <div class="row">
                                @foreach ($role->privileges as $privilege)
                                    <div class="col-md-3 col-6">
                                        <div class="chip">{{ $privilege->name }}</div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="text-center my-3">
                                <h4 class="text-muted">
                                    No privilege found!
                                </h4>
                                <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-secondary waves-effects my-2">Add Privileges</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            {{-- users --}}
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardUser" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">
                        Staff
                    </h5>
                    <div id="cardUser" class="collapse pt-3 show">
                        @if ($role->users->isNotEmpty())
                           <table class="table w-100 nowrap dt-responsive datatable">
                               <thead>
                                   <tr>
                                       <th>Staff</th>
                                       <th>Email</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   @foreach ($role->users as $user)
                                       <tr>
                                           <td>{{ $user->full_name }}</td>
                                           <td>{{ $user->email }}</td>
                                       </tr>
                                   @endforeach
                               </tbody>
                           </table>
                        @else
                            <div class="text-center my-3">
                                <h4 class="text-muted">
                                    No staff found!
                                </h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/role.js') }}"></script>
@endpush