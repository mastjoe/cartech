{{-- create new role --}}
<div class="modal fade" id="add_role_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.roles.store') }}" method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">
                        New Role
                    </h4>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            name="name"
                            id="description"
                            required
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            name="description"
                            id="description"
                        ></x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-defualt waves-effect" type="button" data-dismiss="modal">
                        Dismiss
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Create
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>