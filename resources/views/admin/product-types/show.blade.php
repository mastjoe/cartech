@extends('layouts.admin.app')

@section('title', 'Product Types - '.$product_type->name)
@section('page_title', 'Product Types - '.$product_type->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.product-type.index') }}">Product Types</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($product_type->name, 15) }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
       <div class="col-md-4">
           <div class="card-box">
                <h4>{{ $product_type->name }}</h4>
                <p class="mb-2 mt-3">
                    <strong class="text-muted">Description</strong>
                    <h5 class="mt-0">{{ $product_type->description ?? '-' }}</h5>
                </p>
                <p class="mb-2 mt-3">
                    <strong class="text-muted">Created</strong>
                    <h5 class="mt-0">{{ $product_type->created_at->format('jS M, Y @ h:i a') }}</h5>
                </p>
                <div class="my-3">
                    <button class="btn btn-block btn-warning waves-effect my-2 add_product_type">
                        Edit
                    </button>
                    @if ($product_type->canDelete())                    
                    <button class="btn btn-block btn-danger waves-effect my-2 delete_product_type"
                        data-name="{{ $product_type->name }}"
                        data-url="{{ route('admin.product-type.destroy', $product_type->id) }}"
                    >
                        Delete
                    </button>
                    @endif
                </div>
           </div>
       </div>
       <div class="col-md-8">

       </div>
    </div>

    {{-- modal --}}
    @include('admin.product-types.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/product.type.js') }}"></script>
@endpush