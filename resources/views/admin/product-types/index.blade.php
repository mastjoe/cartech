@extends('layouts.admin.app')

@section('title', 'Product Types')
@section('page_title', 'Product Types')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Product Types
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($product_types->isNotEmpty())                
                <div class="mb-2 text-right">
                    <button class="btn btn-secondary waves-effect add_product_type">
                        New Product Type
                    </button>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">
                            All Product Types
                        </h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 nowrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($product_types as $type)
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.product-type.show', $type->id) }}">{{ $type->name }}</a>
                                        </td>
                                        <td>
                                            @if ($type->description)
                                                {{ Str::limit($type->description, 20) }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            {{ $type->created_at->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">No product type found!</h2>
                    <button class="btn btn-secondary my-3 add_product_type">
                        Add New Product Type
                    </button>
                </div>
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.product-types.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/product.type.js') }}"></script>
@endpush