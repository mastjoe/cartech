<div class="modal fade" tabindex="-1" id="edit_service_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.services.update', $service->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h4 class="modal-title">New Service</h4>
                    <button class="close" type="button" modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            id="service"
                            name="name"
                            required
                            value="{{ $service->name }}"
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            id="description"
                            name="description"
                            rows="3"
                        >{{ $service->description }}</x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-secondary waves-effect">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>