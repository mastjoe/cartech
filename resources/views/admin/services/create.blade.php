<div class="modal fade" tabindex="-1" id="add_service_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.services.store') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">New Service</h4>
                    <button class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            id="service"
                            name="name"
                            required
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            id="description"
                            name="description"
                            rows="3"
                        ></x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-secondary waves-effect">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>