@extends('layouts.admin.app')

@section('title', 'Services')
@section('page_title', 'Services')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Services
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($services->isNotEmpty())
                <div class="mb-2 text-right">
                    <button class="btn btn-secondary waves-effect add_service_btn">
                        New Service
                    </button>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">
                            All Services
                        </h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 nowrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Job Orders</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($services as $service)
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.services.show', $service->id) }}">{{ $service->name }}</a>
                                        </td>
                                        <td>
                                            {{ Str::limit($service->description, 40) }}
                                        </td>
                                        <td>
                                            0
                                        </td>
                                        <td>
                                            {{ $service->created_at->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted mt-3">No service found</h2>
                    <button class="btn btn-secondary my-2 add_service_btn">
                        Add New Service
                    </button>
                </div>
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.services.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/service.js') }}"></script>

    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddServiceModal();
            @endif
        });
    </script>
@endpush