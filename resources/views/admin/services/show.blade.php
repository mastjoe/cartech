@extends('layouts.admin.app')

@section('title', 'Services - '.$service->name)
@section('page_title', 'Services - '.$service->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.services.index') }}">Services</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $service->name }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row mt-2">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title">{{ $service->name }}</h4>
                    <div class="my-4">
                        <h4 class="font-13 text-muted text-uppercase">Description:</h4>
                        <p class="mb-3">{{ $service->description ?? '-' }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Added:</h4>
                        <p class="mb-3">{{ $service->created_at->format('jS F, Y @ h:i a') }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Last Updated:</h4>
                        <p class="mb-3">{{ $service->updated_at->format('jS F, Y @ h:i a') }}</p>
                    </div>
                    <div class="my-3">
                        <button class="btn btn-warning waves-effect btn-block mb-2 edit_service_btn">
                            Edit
                        </button>
                        <button class="btn btn-danger waves-effect btn-block mb-2 delete_service_btn"
                            data-name="{{ $service->name }}"
                            data-url="{{ route('admin.services.destroy', $service->id) }}"
                        >
                            Delete
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">

        </div>
    </div>

    {{-- modal --}}
    @include('admin.services.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/service.js') }}"></script>

    <script>
        $(document).ready(function() {
            @if(session('edit'))
                showEditServiceModal();
            @endif
        });
    </script>
@endpush