@extends('layouts.admin.app')

@section('title', $branch->name.' - Branch')
@section('page_title', $branch->name.' - Branch')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.branches.index') }}">Branches</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($branch->name, 15) }}
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="jumbotron profile-jumbotron jumbotron-bg-1 pb-3">
        <h2 class="text-center text-white">{{ $branch->name }} Branch</h2>
    </div>
    <div class="row">
        <div class="col-md-3 order-1 order-md-2">
            <button class="btn btn-secondary btn-block d-md-none mb-3"><i class="fa fa-nav"></i>Toggle Actions</button>
            <div class="mobile-menu d-none d-md-block">
                <a href="{{ route('admin.branches.edit', $branch->id) }}" class="btn btn-warning waves-effect btn-block mb-2">
                    Edit
                </a>
                <a href="{{ route('admin.branches.staff', $branch->id) }}" class="btn btn-secondary waves-effect btn-block mb-2">
                    Staff
                </a>
                <a href="{{ route('admin.branches.customers', $branch->id) }}" class="btn btn-secondary waves-effect btn-block mb-2">
                    Customers
                </a>
                <button class="btn btn-danger waves-effect btn-block">
                    Delete
                </button>
            </div>
        </div>
        <div class="col-md-9 order-md-1 order-2">
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardPriv" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0"> Branch Information</h5>
                    <div id="cardPriv" class="collapse pt-3 show">
                        <h4 class="font-13 text-muted text-uppercase">Email :</h4>
                        <p class="mb-3">{{ $branch->email }}</p>

                        <h4 class="font-13 text-muted text-uppercase">City :</h4>
                        <p class="mb-3">{{ $branch->city }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Address :</h4>
                        <p class="mb-3">{{ $branch->address ?? '-' }}</p>
                        
                        <h4 class="font-13 text-muted text-uppercase">State :</h4>
                        <p class="mb-3">{{ $branch->state ?? '-' }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Country :</h4>
                        <p class="mb-3">{{ $branch->country ?? '-' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    
@endpush