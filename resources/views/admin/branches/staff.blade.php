@extends('layouts.admin.app')

@section('title', $branch->name.' - Branch')
@section('page_title', $branch->name.' - Branch')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.branches.index') }}">Branches</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.branches.show', $branch->id) }}">{{ Str::limit($branch->name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        Staff
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($branch->users->isNotEmpty())   
                <div class="mb-2 text-right">
                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.branches.create') }}">
                        Branch Staff
                    </a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">
                            All Staff who has access to branch
                        </h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 nowrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($branch->users as $user)
                                    <tr>
                                        <td>
                                            <a href="">{{ $user->full_name }}</a>
                                        </td>
                                        <td>
                                            {{ $user->email }}
                                        </td>
                                        <td>
                                            {{ $user->phone }}
                                        </td>
                                        <td>
                                            {{ $customer->created_at->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="text-center my-3">
                    <h2 class="text-center mt-4 text-muted">
                        No staff found!
                    </h2>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    
@endpush