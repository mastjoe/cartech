@extends('layouts.admin.app')

@section('title', 'Edit Branch - '.$branch->name)
@section('page_title', 'Edit Branch - '.$branch->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.branch.index') }}">Branches</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.branch.show', $branch->id) }}">
            {{ Str::limit($branch->name, 15) }}
        </a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" action="{{ route('admin.branch.store') }}">
                    @csrf
                    <div class="card-body">
                        <h4 class="header-title">
                            Update Branch
                        </h4>
                        <p class="font-13 sub-header">
                            fill form correctly to update branch record
                        </p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-dismissable alert-danger">
                                        {{ $errors->first() }}
                                        <button class="close">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <x-form.input
                                        name="name"
                                        id="name"
                                        value="{{ old('name') ?? $branch->name }}"
                                        required
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        id="email"
                                        type="email"
                                        required
                                        value="{{ old('email') ?? $branch->email }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        type="tel"
                                        value="{{ old('phone') ?? $branch->phone }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="address"
                                        rows="2"
                                    >{{ old('address') ?? $branch->address }}</x-form.textarea>
                                </div>

                                <div class="form-group">
                                    <label for="city">City</label>
                                    <x-form.input
                                        name="city"
                                        id="city"
                                        required
                                        value="{{ old('city') ?? $branch->city }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <x-form.select
                                        name="country"
                                        id="country"
                                        required
                                        data-country="{{ old('country') ?? $branch->country }}"
                                    ></x-form.select>
                                </div>

                                <div class="form-group">
                                    <label for="state">State</label>
                                    <x-form.select
                                        name="state"
                                        id="state"
                                        required
                                        data-state="{{ old('state') ?? $branch->state }}"
                                    ></x-form.select>
                                </div>

                                <div class="form-group my-4 text-center">
                                    <button class="btn btn-secondary waves-effect px-5" type="submit">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/util.countries.js') }}"></script>
    <script src="{{ asset('js/admin/branch.js') }}"></script>

    <script>
        $(document).ready(function() {
            countriesStateHandler(_countries, 'country', 'state');
        });
    </script>
@endpush