@extends('layouts.admin.app')

@section('title', $user->full_name. ' - NOKs Update')
@section('page_title', $user->full_name. ' - Next Of Kins Update')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.noks', $user->id) }}">NOKs</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.noks.show', [$user->id, $nok->id]) }}">{{ $nok->reference }}</a>
    </li>
    <li class="breadcrumb-item">
        Edit
    </li>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/drop_uploader.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.staff.noks.update',[ $user->id, $nok->id]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card-body">
                         <h4 class="header-title">Edit Next of Kin</h4>
                        <p class="font-13 sub-header"></p>

                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        {{ $errors->first() }}
                                        <button class="close" type="submit"><span>&times;</span></button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <x-form.input
                                        name="first_name"
                                        id="first_name"
                                        required
                                        value="{{ old('first_name') ?? $nok->first_name }}"
                                    ></x-form.input>
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <x-form.input
                                        name="last_name"
                                        id="last_name"
                                        value="{{ old('last_name') ?? $nok->last_name }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        type="email"
                                        id="email"
                                        value="{{ old('email') ?? $nok->email }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        value="{{ old('phone') ?? $nok->phone }}"
                                        required
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="relationship">Relationship</label>
                                    <x-form.input
                                        name="relationship"
                                        id="relationship"
                                        value="{{ old('relationship') ?? $nok->relationship}}"
                                        required
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="occupation">occupation</label>
                                    <x-form.input
                                        name="occupation"
                                        id="occupation"
                                        value="{{ old('occupation') ?? $nok->occupation }}"
                                        required
                                    ></x-form.input>
                                </div>
                                
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="address"
                                    >{{ old('address') ?? $nok->address }}</x-form.textarea>
                                </div>

                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <x-form.input
                                        name="image"
                                        id="image"
                                        type="file"
                                    ></x-form.input>
                                </div>

                                <div class="form-group text-center my-4">
                                    <button type="submit" class="btn btn-secondary waves-effect px-5">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/drop_uploader.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>

@endpush