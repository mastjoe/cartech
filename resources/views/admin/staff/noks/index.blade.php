@extends('layouts.admin.app')

@section('title', $user->full_name. ' - NOKs')
@section('page_title', $user->full_name. ' - Next Of Kins')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        NOKs
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    @if ($user->noks->isNotEmpty())
        <div class="row">
            <div class="col-12 text-right mb-2">
                <a href="{{ route('admin.staff.noks.create', $user->id) }}" class="btn btn-secondary waves-effect">
                    New Next of Kin
                </a>
            </div>
            @foreach ($user->noks as $nok)
                <div class="col-12 mb-2">
                    <div class="card-box">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <img src="{{ $nok->avatarThumbnail() }}" class="img-thumbnail avatar-xxl" />
                            </div>
                            <div class="col-md-9">
                                <h4>{{ $nok->full_name }}</h4>
                                <p>
                                    <i class="fa fa-phone mr-1"></i>
                                    Telephone:
                                    <span class="text-muted ml-2"><a href="tel:{{ $nok->phone }}">{{ $nok->phone }}</a></span>
                                </p>
                                <p>
                                    <i class="fa fa-envelope mr-1"></i>
                                    Email:
                                    @if ($nok->email)
                                        <span class="text-muted ml-2"><a href="mailto:{{ $nok->email }}">{{ $nok->email }}</a></span>
                                    @else
                                        '-'
                                    @endif
                                </p>

                                <p>
                                    <i class="fa fa-user-friends mr-1"></i>
                                    Relationship:
                                    <span class="text-muted ml-2">{{ $nok->relationship }}</span>
                                </p>
                            </div>
                        </div>
                        <div class="text-right">
                            <a class="btn btn-secondary waves-effect" href="{{ route('admin.staff.noks.show', [$user->id, $nok->id]) }}">
                                View
                            </a>
                            <a class="btn btn-warning waves-effect" href="{{ route('admin.staff.noks.edit', [$user->id, $nok->id]) }}"
                                title="Edit"    
                            >
                                <span class="fa fa-edit"></span>
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @else
        <div class="text-center my-5">
            <h2 class="text-muted">No Next of Kin found!</h2>
            <a href="{{ route('admin.staff.noks.create', $user->id) }}" class="btn btn-secondary waves-effect my-2">
                Add New Next of Kin
            </a>
        </div>
    @endif
@endsection

@push('js')
    
@endpush