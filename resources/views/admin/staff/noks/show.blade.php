@extends('layouts.admin.app')

@section('title', $user->full_name. ' - NOKs')
@section('page_title', $user->full_name. ' - Next Of Kins Details')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.staff.noks', $user->id) }}">NOKs</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $nok->reference }}
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
   <div class="row">
       <div class="col-12">
            <div class="mb-2 text-right">
                <a class="btn btn-warning waves-effect" href="{{ route('admin.staff.noks.edit', [$user->id, $nok->id]) }}">
                    Edit
                </a>
                <button class="btn btn-danger waves-effect delete_nok_btn"
                    data-name="{{ $nok->full_name }}"
                    data-url="{{ route('admin.staff.noks.destroy', [$user->id, $nok->id]) }}"
                >
                    Delete
                </button>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                    </div>
                    <h5 class="card-title mb-0"> Profile Information</h5>
                    <div id="cardPriv" class="collapse pt-3 show">
                        <div class="row">
                            <div class="col-md-3 text-center">
                                <img src="{{ $nok->avatarThumbnail() }}"  class="img-thumbnail avatar-xxl d-block mx-auto" />
                                <h5 class="my-2">{{ $nok->full_name }}</h5>
                            </div>
                            <div class="col-md-9">
                                <h4 class="font-13 text-muted text-uppercase">Relationship :</h4>
                                <p class="mb-3">{{ $nok->relationship ?? '-' }}</p>

                                <h4 class="font-13 text-muted text-uppercase">Occupation :</h4>
                                <p class="mb-3">{{ $nok->occupation ?? '-' }}</p>

                                <h4 class="font-13 text-muted text-uppercase">Email :</h4>
                                <p class="mb-3">{{ $nok->email ?? '-' }}</p>
        
                                <h4 class="font-13 text-muted text-uppercase">Phone :</h4>
                                <p class="mb-3">{{ $nok->phone ?? '-' }}</p>
        
                                <h4 class="font-13 text-muted text-uppercase">Address :</h4>
                                <p class="mb-3">{{ $nok->address ?? '-' }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
   </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/staff.js') }}"></script>
@endpush