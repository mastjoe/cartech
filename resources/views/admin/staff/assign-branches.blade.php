{{-- update branches --}}
<div class="modal fade" id="assign_branches_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.staff.branches.update', $user->id) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Branches</h4>
                    <button class="close" data-dismiss="modal" type="button">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="branches">Branches</label>
                        <x-form.select
                            name="branches[]"
                            id="branches"
                            multiple
                        >
                            @foreach ($branches as $branch)
                                <option
                                    value="{{ $branch->id }}"
                                    @if (in_array($branch->id, $user->branches->pluck('id')->toArray()))
                                        selected
                                    @endif
                                >
                                    {{ $branch->city .'-'. $branch->name }}
                                </option>
                            @endforeach
                        </x-form.select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" type="button" data-dismiss="modal">
                        Dismiss
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>