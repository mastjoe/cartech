@extends('layouts.admin.app')

@section('title', 'Edit - '.$user->full_name)
@section('page_title', 'Edit - '.$user->full_name)

@push('breadcrumb')
     <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/drop_uploader.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form method="POST" enctype="multipart/form-data" action="{{ route('admin.staff.update', $user->id) }}">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title">New Staff</h4>
                        <p class="font-13 sub-header"></p>
                        
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        {{ $errors->first() }}
                                        <button class="close" type="button">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <x-form.input
                                        name="first_name"
                                        id="first_name"
                                        required
                                        value="{{ old('first_name') ?? $user->first_name }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="middle_name">Middle Name</label>
                                    <x-form.input
                                        name="middle_name"
                                        id="middle_name"
                                        value="{{ old('middle_name') ?? $user->middle_name }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <x-form.input
                                        name="last_name"
                                        id="last_name"
                                        required
                                        value="{{ old('last_name') ?? $user->last_name }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="gender">Gender</label>
                                    <x-form.select
                                        name="gender"
                                        id="gender"
                                        required
                                    >
                                        <option value="">Choose Gender</option>
                                        @foreach ($genders as $key => $gender)
                                            <option 
                                                value="{{ $key }}"
                                                @if (old('gender'))                                                    
                                                    {{ old('gender') == $key ? "selected" : null }}
                                                @else
                                                    {{ $user->gender  == $key ? 'selected' : null}}
                                                @endif
                                            >{{ $gender }}</option>
                                        @endforeach
                                    </x-form.select>
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        id="email"
                                        required
                                        type="email"
                                        value="{{ old('email') ?? $user->email }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        required
                                        type="tel"
                                        value="{{ old('phone') ??  $user->phone }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="dob">Date of Birth</label>
                                    @php
                                        $dob = null;
                                        if (old('dob')) {
                                            $dob = old('dob');
                                        } elseif ($user->dob) {
                                            $dob = $user->dob->format('Y-m-d');
                                        }
                                    @endphp
                                    <x-form.input
                                        name="dob"
                                        type="date"
                                        id="dob"
                                        value="{{ $dob }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="address"
                                    >{{ old('address') ?? $user->address }}</x-form.textarea>
                                </div>

                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <x-form.input
                                        name="image"
                                        id="image"
                                        type="file"
                                    ></x-form.input>
                                </div>

                                <div class="fornm-group my-4 text-center">
                                    <button class="btn btn-secondary px-5" type="submit">
                                        Save
                                    </button>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/drop_uploader.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>
@endpush