<div class="text-center card-box">
    <div class="pt-2 pb-2">
        <img src="{{ $user->avatarThumbnail() }}" class="rounded-circle img-thumbnail avatar-xl" alt="profile-image">

        <h4 class="mt-3"><a href="extras-profile.html" class="text-dark">{{ $user->full_name }}</a></h4>
        <p class="text-muted">
            <small> <a href="#" class="text-green">{{ $user->email }}</a> </small>
        </p>
        <div class="text-center">
            @if ($user->roles->isNotEmpty())
                <p title="{{ implode(", ", $user->roleNamesArray()) }}">
                    <span class="badge badge-dark">{{ $user->roles->count() }} roles</span>
                </p>
            @else
                <p>No role</p>
            @endif
        </div>

        <a class="btn btn-dark waves-effect mt-2" href="{{ route('admin.staff.show', $user->id) }}">
            See Details
        </a>
    </div>
</div>