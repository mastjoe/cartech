@extends('layouts.admin.app')

@section('title', 'Staff - '.$user->full_name)
@section('page_title', $user->full_name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($user->full_name, 15) }}
    </li>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush

@section('content')
    <div class="jumbotron profile-jumbotron jumbotron-bg-1">
        <div class="text-center">
            <div class="">
                <img src="{{ $user->avatarThumbnail() }}" class="img-thumbnail rounded-circle mx-auto avatar-xl" alt="profile-image">
            </div>
            <div class="py-2">
                <h2 class="text-white">{{ $user->full_names }}</h2>
                <p><a class="text-warning" href="mailto:{{ $user->email }}">{{ $user->email }}</a></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 order-3 order-md-1">
            {{-- profile details --}}
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardPriv" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0"> Profile Information</h5>
                    <div id="cardPriv" class="collapse pt-3 show">
                        <h4 class="font-13 text-muted text-uppercase">Email :</h4>
                        <p class="mb-3">{{ $user->email }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Phone :</h4>
                        <p class="mb-3">{{ $user->phone }}</p>

                        <h4 class="font-13 text-muted text-uppercase">Address :</h4>
                        <p class="mb-3">{{ $user->address ?? '-' }}</p>
                    </div>
                </div>
            </div>
            {{-- branches details --}}
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardBranch" role="button" aria-expanded="false" aria-controls="cardBranch"><i class="mdi mdi-minus"></i></a>
                        <a href="javascript:;" class="assign_branches_btn" role="button"><i class="mdi mdi-tooltip-edit"></i></a>
                    </div>
                    <h5 class="card-title mb-0">Assign Branches</h5>
                    <div id="cardBranch" class="collapse pt-3 show">
                       @if ($user->branches->isNotEmpty())
                           <div class="row">
                               @foreach ($user->branches as $branch)
                                   <div class="col-md-4">
                                       <span class="chip">{{ $branch->city }} - {{ $branch->name }}</span>
                                   </div>
                               @endforeach
                           </div>
                       @else
                           <div class="text-center my-4">
                               <h5 class="text-muted">No branch assigned!</h5>
                           </div>
                       @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1 order-2"></div>
        <div class="col-md-3 order-1 order-md-2">
            <button class="btn btn-secondary btn-block d-md-none mb-3"><i class="fa fa-nav"></i>Toggle Actions</button>
            <div class="mobile-menu d-none d-md-block">
                <a href="{{ route('admin.staff.edit', $user->id) }}" class="btn btn-warning btn-block waves-effect mb-2">
                    Edit Account
                </a>
                <a href="{{ route('admin.staff.noks', $user->id) }}" class="btn btn-dark btn-block waves-effect mb-2">
                    Next of Kin
                </a>
                @if ($user->isSuspended())                    
                    <button class="btn btn-dark btn-block waves-effect mb-2 unsuspend_user_btn"
                        data-url=""
                        data-name="{{ $user->full_name }}"    
                    >
                        Unsuspend Account
                    </button>
                @else
                    <button class="btn btn-dark btn-block waves-effect mb-2 suspend_user_btn"
                        data-url=""
                        data-name="{{ $user->full_name }}"
                    >
                        Suspend Account
                    </button>
                @endif
                
                <a href="{{ route('admin.staff.roles', $user->id) }}" class="btn btn-dark btn-block waves-effect mb-2">
                    Roles & Privileges
                </a>

                @if ($user->canDelete())                    
                <button class="btn btn-danger btn-block mb-2 waves-effect delete_user_btn"
                    data-name="{{ $user->full_name }}"
                    data-ur="{{ route('admin.staff.destroy', $user->id) }}"
                >
                    Delete Account
                </button>
                @endif
                <button class="btn btn-dark btn-block waves-effect mb-2 assign_branches_btn">
                     Branches
                </button>
            </div>
        </div>
    </div>

    {{-- modals --}}
    @include('admin.staff.assign-branches')
@endsection

@push('js')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#branches').select2({
                placeholder: 'pick branches'
            });
        });
    </script>
@endpush