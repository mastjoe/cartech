@extends('layouts.admin.app')

@section('title', $user->full_name. ' - Roles & Privileges')
@section('page_title', $user->full_name. ' - Roles & Privileges')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        Roles
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            {{-- roles --}}
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="header-title mb-4">Roles</h4>
                    <p class="font-13 subheader-title"></p>
                    @if ($user->roles->isNotEmpty())
                        <div class="row">
                            @foreach ($user->roles as $role)
                                <div class="col-md-4">
                                    <span class="chip">{{ $role->name }}</span>
                                </div>
                            @endforeach
                            <div class="text-right col-12 mt-2">
                                <a href="{{ route('admin.staff.roles.edit', $user->id) }}">
                                    <span class="fas fa-pencil-alt"></span>
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="text-center my-4">
                            <h4 class="text-muted">No role found!</h4>
                            <a href="{{ route('admin.staff.roles.edit', $user->id) }}" class="btn btn-dark mt-3 waves-effect">
                                Assign Roles
                            </a>
                        </div>
                    @endif
                </div>
            </div>
            {{-- privileges --}}
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="header-title">Privileges</h4>
                    <p class="subheader-title font-13"></p>
                    @if ($user->privileges->count())
                        <div class="row">
                            @foreach ($user->privileges as $privilege)
                                <div class="col-lg-3 col-md-4">
                                    <span class="chip">{{ $privilege->name }}</span>
                                </div>
                            @endforeach
                            <div class="col-12 text-right mt-3">
                                <a href="{{ route('admin.staff.roles.edit', $user->id) }}">
                                    <span class="fas fa-pencil-alt"></span>
                                </a>
                            </div>
                        </div>
                    @else
                        <div class="text-center my-3">
                            <h4 class="text-muted">No privilege found!</h4>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/staff.js') }}"></script>
@endpush