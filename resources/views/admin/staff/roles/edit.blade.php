@extends('layouts.admin.app')

@section('title', $user->full_name. ' - Edit Roles & Privileges')
@section('page_title', $user->full_name. ' - Edit Roles & Privileges')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>
    <li class="breadcrumb-item ">
        <a href="{{ route('admin.staff.show', $user->id) }}">{{ Str::limit($user->full_name, 15) }}</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.roles', $user->id) }}">Roles</a>
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            {{-- role --}}
            <div class="card mb-4">
                <form action="{{ route('admin.staff.roles.update', $user->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title mb-3">Update Role</h4>
                        <div class="form-group">
                            <label for="roles">Roles</label>
                            <x-form.select
                                id="roles"
                                name="roles[]"
                                required
                                multiple
                            >
                                <option value="">Choose Role(s)</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->id }}"
                                        @if (in_array($role->id, $user->roles->pluck('id')->toArray()))
                                            selected
                                        @endif    
                                    >
                                        {{ $role->name }}
                                    </option>
                                @endforeach
                            </x-form.select>
                        </div>
                        <div class="form-group text-center mt-3">
                            <button class="btn btn-secondary waves-effect" type="submit">
                                Save Role(s)
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            {{-- privileges --}}
            <div class="card mb-4">
                <form action="{{ route('admin.staff.privileges.update', $user->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title mb-3">Update Privilege</h4>
                        <div class="form-group">
                            <label for="privileges">Privileges</label>
                            <x-form.select
                                name="privileges[]"
                                id="privileges"
                                required
                                multiple
                            >
                                <option value="">Choose Privilege(s)</option>
                                @foreach ($privileges->groupBy('category') as $key => $privilege)
                                    <optgroup label="{{ $key }}">
                                        @foreach ($privileges->where('category', $key) as $priv)
                                            <option value="{{ $priv->id }}"
                                                @if (in_array($priv->id, $user->privileges->pluck('id')->toArray()))
                                                    selected
                                                @endif    
                                            >
                                                {{ $priv->name }}
                                            </option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </x-form.select>
                        </div>
                        <div class="form-group mt-3 text-center">
                            <button class="btn btn-secondary waves-effect" type="submit">
                                Save Privilege(s)
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('js/admin/staff.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#roles').select2({
                placeholder: 'Choose Role(s)',
            });

            $('#privileges').select2({
                placeholder: 'Choose Privilege(s)',
            });
        })
    </script>
@endpush