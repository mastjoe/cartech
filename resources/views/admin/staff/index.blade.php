@extends('layouts.admin.app')

@section('title', 'All Staff')
@section('page_title', 'All Staff')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Staff
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row mb-3">
        {{-- search form --}}
        <div class="col-md-4">
            <form class="form-inline">
                <div class="input-group">
                    <x-form.input
                        name="search"
                        value="{{ $search }}"
                        class="bg-transparent"
                        placeholder="Search..."
                    ></x-form.input>
                </div>
                <div class="input-group-append">
                    <button class="btn btn-secondary waves-effect">
                        <span class="fa fa-search"></span>
                    </button>
                </div>
            </form>
        </div>
        <div class="col-md-4"></div>
        {{-- add new --}}
        <div class="col-md-4 text-right">
            <a href="{{ route('admin.staff.create') }}" class="btn btn-secondary waves-effect">
                New Staff
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            @if ($users->isNotEmpty())
                @if ($search)
                    <div class="alert alert-light mb-2">
                        {{ $users->total() }} staff found for <span class="text-warning">{{ $search }}</span>
                    </div>
                @endif
                <div class="row">
                    @foreach ($users as $user)
                        <div class="col-lg-4 col-md-12">
                            @include('admin.staff.card')
                        </div>  
                    @endforeach
                </div>
            @else
                @if ($search)
                    <div class="text-center my-3">
                        <h2 class="text-muted">No staff found for <span class="bg-warning text-white">{{ $search }}</span></h2>

                        <a class="btn btn-secondary waves-effect my-2" href="{{ route('admin.staff.index') }}">Return back</a>
                    </div>
                @else                    
                    <div class="text-center my-3">
                        <h2 class="text-muted">No staff found</h2>
                    </div>
                @endif
            @endif
        </div>
        <div class="col-12">
            {{ $users->links() }}
        </div>
    </div>
@endsection

@push('js')
    
@endpush