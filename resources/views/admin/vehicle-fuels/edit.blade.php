{{-- add vehicle fuel --}}
<div class="modal fade" tabindex="-1" id="edit_fuel_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.vehicle-fuels.update', $vehicle_fuel->id) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Edit Fuel</h4>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            name="name"
                            id="name"
                            required
                            value="{{ $vehicle_fuel->name }}"
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            name="description"
                            id="description"
                        >{{ $vehicle_fuel->description }}</x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-secondary waves-effect">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>