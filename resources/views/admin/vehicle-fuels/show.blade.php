@extends('layouts.admin.app')

@section('title', 'Vehicle Fuels')
@section('page_title', 'Vehicle - Fuels')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.vehicles.index') }}">Vehicles</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.vehicle-fuels.index') }}">Fuels</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($vehicle_fuel->name, 15) }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card-box">
                <h4 class="header-title mb-2">{{ $vehicle_fuel->name }}</h4>
                <div class="my-2 mt-3">
                    <p class="text-muted my-0">Description</p>
                    <h5 class="text-dark">{{ $vehicle_fuel->description }}</h5>
                </div>
                <div class="my-2">
                    <p class="text-muted my-0">Status</p>
                    <h5 class="text-dark">{{ $vehicle_fuel->status->name }}</h5>
                </div>
                <div class="mb-2 mt-4">
                    <button class="btn btn-warning btn-block waves-effect edit_fuel_btn mb-2">
                        Edit
                    </button>
                    <button class="btn btn-danger btn-block waves-effect delete_fuel_btn"
                        data-url="{{ route('admin.vehicle-fuels.destroy', $vehicle_fuel->id) }}"
                        data-name="{{ $vehicle_fuel->name }}"
                    >
                        Delete
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardVehicles" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">Vehicles That uses {{ $vehicle_fuel->name }}</h5>
                    <div id="cardVehicles" class="collapse pt-3 show">
                        @if ($vehicle_fuel->vehicles->isNotEmpty())                            
                            <table class="table table-hover w-100 datatable dt-responsive">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Vehicle Number</th>
                                        <th>Customer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vehicle_fuel->vehicles as $vehicle)
                                        <tr>
                                            <td>
                                                {{ $vehicle->reference }}
                                            </td>
                                            <td>{{ $vehicle->plate_number }}</td>
                                            <td>
                                                {{ $vehicle->customer->name }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center my-4">
                                <h4 class="text-muted">No vehicle was found!</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- modal --}}
    @include('admin.vehicle-fuels.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/vehicle.fuel.js') }}"></script>

    <script>
        $(document).ready(function() {
            @if(session('edit'))
                showEditFuelModal();
            @endif
        })
    </script>
@endpush