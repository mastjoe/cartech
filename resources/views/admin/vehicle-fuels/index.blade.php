@extends('layouts.admin.app')

@section('title', 'Vehicle Fuels')
@section('page_title', 'Vehicle - Fuels')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.vehicles.index') }}">Vehicles</a>
    </li>
    <li class="breadcrumb-item active">
        Fuels
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($vehicle_fuels->isNotEmpty())
                <div class="text-right mb-2">
                    <button class="btn btn-secondary waves-effect add_fuel_btn">
                        New Fuel
                    </button>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Vehicle Fuels</h4>
                        <p class="font-13 sub-header"></p>
                        <div>
                            <table class="table table-hover w-100 dt-responsive datatable">
                                <thead>
                                    <tr>
                                        <th>Fuel</th>
                                        <th>Vehicle</th>
                                        <th>Added</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vehicle_fuels as $fuel)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.vehicle-fuels.show', $fuel->id) }}">
                                                    {{ $fuel->name }}
                                                </a>
                                            </td>
                                            <td>
                                                {{ $fuel->vehicles->count() }}
                                            </td>
                                            <td>
                                                {{ $fuel->created_at->diffForHumans() }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted my-2">
                        No fuel found!
                    </h2>
                </div>
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.vehicle-fuels.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/vehicle.fuel.js') }}"></script>

    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddFuelModal()
            @endif
        })
    </script>
@endpush