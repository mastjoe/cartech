@extends('layouts.admin.app')

@section('title', 'Dashboard')

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @include('admin.dashboard.cards')
        </div>
    </div>
@endsection

@push('js')
    
@endpush