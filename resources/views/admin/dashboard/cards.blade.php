@if ($admin->hasAnyRole(['superadmin']))
    <div class="row">
        {{-- branches --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                            <i class="fe-map font-22 avatar-title text-success"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $branchesCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Branches</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- customers --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                            <i class="fe-users font-22 avatar-title text-info"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $customersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Customers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- vehicles --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-danger border-danger border">
                            <i class="fe-truck font-22 avatar-title text-danger"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $customersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Vehicles</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- job order --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-primary border-primary border">
                            <i class="fe-list font-22 avatar-title text-primary"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $customersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Job Orders</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- staff --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                            <i class="fe-users font-22 avatar-title text-success"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $usersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Staff</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- supplies --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                            <i class="fe-layers font-22 avatar-title text-success"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $usersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Supplies</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- suppliers --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                            <i class="fe-users font-22 avatar-title text-success"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $usersCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Suppliers</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- products --}}
        <div class="col-md-6 col-xl-3">
            <div class="widget-rounded-circle card-box">
                <div class="row">
                    <div class="col-6">
                        <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                            <i class="fe-package font-22 avatar-title text-success"></i>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="text-right">
                            <h3 class="text-dark mt-1"><span data-plugin="counterup">{{ $productsCount }}</span></h3>
                            <p class="text-muted mb-1 text-truncate">Products</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    
@endif