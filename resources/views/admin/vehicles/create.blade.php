@extends('layouts.admin.app')

@section('title', 'New Vehicle')
@section('page_title', 'New Vehicle')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.vehicles.create') }}">Vehicles</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
           <div class="card">
               <form action="{{ route('admin.vehicles.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <h4 class="header-title">New Vehicle Record</h4>
                        <p class="font-13 sub-header"></p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        {{ $errors->first() }}
                                        <button class="close" type="button" data-dismiss="modal">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="customer">Customer</label>
                                    <x-form.select
                                        name="customer_id"
                                        id="customer"
                                        required
                                    >
                                        <option value="">Choose Customer</option>
                                        @foreach ($customers as $customer)
                                            <option value="{{ $customer->id }}"
                                                @if ($customer_id == $customer->id)
                                                    selected
                                                @elseif (old('customer_id') == $customer->id)
                                                    selected
                                                @endif    
                                            >
                                                {{ $customer->name }} ({{ $customer->branch->name }})
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>

                                {{-- branch --}}
                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <x-form.input
                                        name="branch"
                                        id="branch"
                                        readonly
                                        value="{{ $admin->branch->name }}"
                                    ></x-form.input>
                                </div>

                                {{-- vehicle make --}}
                                <div class="form-group">
                                    <label for="vehicle_make">Vehicle Make</label>
                                    <x-form.select
                                        name="vehicle_make_id"
                                        id="vehicle_make"
                                        required
                                    >
                                        <option value="">Choose Vehicle Make</option>
                                        @foreach ($vehicle_makes as $make)
                                            <option value="{{ $make->id }}"
                                                 @if (old('vehicle_make_id') == $make->id)selected @endif     
                                            >
                                                {{ $make->name }}
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>

                                {{-- vehicle fuel --}}
                                <div class="form-group">
                                    <label for="vehicle_fuel">Vehicle Fuel</label>
                                    <x-form.select
                                        name="vehicle_fuel_id"
                                        id="vehicle_fuel"
                                        required
                                    >
                                        <option value="">Choose vehicle fuel</option>
                                        @foreach ($vehicle_fuels as $fuel)
                                            <option value="{{ $fuel->id }}"
                                                @if (old('vehicle_fuel_id') == $fuel->id)selected @endif    
                                            >
                                                {{ $fuel->name }}
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>

                                {{-- plat number --}}
                                <div class="form-group">
                                    <label for="plate_number">Plate Number</label>
                                    <x-form.input
                                        name="plate_number"
                                        id="plate_number"
                                        required
                                        value="{{ old('plate_number') }}"
                                    ></x-form.input>
                                </div>

                                {{-- model --}}
                                <div class="form-group">
                                    <label for="model">Model</label>
                                    <x-form.input
                                        name="model"
                                        id="model"
                                        value="{{ old('model') }}"
                                    ></x-form.input>
                                </div>

                                {{-- model year --}}
                                <div class="form-group">
                                    <label for="model_year">Model Year</label>
                                    <x-form.input
                                        name="model_year"
                                        id="model_year"
                                        value="{{ old('model_year') }}"
                                    ></x-form.input>
                                </div>

                                {{-- engine number  --}}
                                <div class="form-group">
                                    <label for="enginer_number">Engine Number</label>
                                    <x-form.input
                                        name="engine_number"
                                        id="engine_number"
                                        value="{{ old('engine_number') }}"
                                    ></x-form.input>
                                </div>

                                {{-- gear box number --}}
                                <div class="form-group">
                                    <label for="gearbox_number">Gear Box Number</label>
                                    <x-form.input
                                        name="gearbox_number"
                                        id="gearbox_number"
                                        value="{{ old('gearbox_number') }}"
                                    ></x-form.input>
                                </div>

                                {{-- color --}}
                                <div class="form-group">
                                    <label for="color">Color</label>
                                    <x-form.input
                                        name="color"
                                        id="color"
                                        required
                                        type="color"
                                        value="{{ old('color') }}"
                                    ></x-form.input>
                                </div>

                                {{-- description --}}
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <x-form.textarea
                                        name="description"
                                        id="description"
                                    >{{ old('description') }}</x-form.textarea>
                                </div>

                                <div class="form-group mt-5 mb-4 text-center">
                                    <button class="btn btn-secondary btn-lg waves-effect px-5" type="submit">
                                        Save Record
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
               </form>
           </div>
        </div>
    </div>
@endsection

@push('js')
    
@endpush