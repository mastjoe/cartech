@extends('layouts.admin.app')

@section('title', 'Vehicles')
@section('page_title', 'All Vehicles')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Vehicles
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($vehicles->isNotEmpty())
                <div class="text-right mb-2">
                    <a href="{{ route('admin.vehicles.create') }}" class="btn btn-secondary waves-effect">New Vehicle</a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Vehicles</h4>
                        <p class="sub-header font-13"></p>
                        <div>
                            <table class="table nowrap w-100 dt-responsive datatable table-hover">
                                <thead>
                                    <tr>
                                        <th>Reference</th>
                                        <th>Number</th>
                                        <th>Customer</th>
                                        <th>Branch</th>
                                        <th>Added</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vehicles as $vehicle)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.vehicles.show', $vehicle->id) }}">{{ $vehicle->reference }}</a>
                                            </td>
                                            <td>{{ $vehicle->plate_number }}</td>
                                            <td>{{ $vehicle->customer->name }}</td>
                                            <td>{{ $vehicle->branch->name }}</td>
                                            <td>{{ $vehicle->created_at->diffForHumans() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted my-2">
                        No vehicle record found
                    </h2>
                    <a href="{{ route('admin.vehicles.create') }}" class="btn btn-secondary waves-effect px-4">
                        Add Vehicle Record
                    </a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/vehicle.js') }}"></script>
@endpush