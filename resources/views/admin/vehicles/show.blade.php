@extends('layouts.admin.app')

@section('title', 'Vehicle - '.$vehicle->reference)
@section('page_title', 'Vehicle - '.$vehicle->reference)

@push('breadcrumb')
    {{-- <li class="breadcrumb-item">
        <a href="{{ route('admin.customers.index') }}">Customers</a>
    </li> --}}
    <li class="breadcrumb-item">
        <a href="{{ route('admin.customers.show', $vehicle->customer->id) }}">
            {{ Str::limit($vehicle->customer->name, 15) }}
        </a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.customers.vehicles', $vehicle->customer_id) }}">Vehicles</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $vehicle->reference }}
    </li>
@endpush

@push('css')
    
@endpush

@section('content')
    <div class="row">
        <div class="col-md-9 order-2 order-md-1">
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="header-title">Vehicle Details</h4>
                    <p class="font-13 subheader-title"></p>
                    <div>
                        <table class="table table-borderless">
                            <thead>
                                <th width="25%"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Reference</td>
                                    <td>{{ $vehicle->reference }}</td>
                                </tr>
                                <tr>
                                    <td>Plate Number</td>
                                    <td>{{ $vehicle->plate_number }}</td>
                                </tr>
                                <tr>
                                    <td>Customer</td>
                                    <td>
                                        @can('view', $vehicle->customer)
                                            <a href="{{ route('admin.customers.show', $vehicle->customer_id) }}">
                                                {{ $vehicle->customer->name }}
                                            </a>
                                        @else
                                            {{ $vehicle->customer->name }}
                                        @endcan
                                    </td>
                                </tr>
                                <tr>
                                    <td>Phone Number</td>
                                    <td>{{ $vehicle->customer->phone ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Branch</td>
                                    <td>{{ $vehicle->branch->name }}</td>
                                </tr>
                                <tr>
                                    <td>Fuel</td>
                                    <td>{{ $vehicle->fuel }}</td>
                                </tr>
                                <tr>
                                    <td>Make</td>
                                    <td>{{ $vehicle->make->name }}</td>
                                </tr>
                                <tr>
                                    <td>Plate Number</td>
                                    <td>{{ $vehicle->plate_number }}</td>
                                </tr>
                                <tr>
                                    <td>Engine Number</td>
                                    <td>{{ $vehicle->engine_number ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Gearbox Number</td>
                                    <td>{{ $vehicle->gearbox_number ?? '-' }}</td>
                                </tr>
                                <tr>
                                    <td>Color</td>
                                    <td>
                                        <span class="chip chip-sm" 
                                            style="background-color: {{ $vehicle->color }}; padding: 4px; width:65%;"
                                        >
                                            {{ $vehicle->color }}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Added On</td>
                                    <td>{{ $vehicle->created_at->format('jS M, Y @ h:i a') }}</td>
                                </tr>
                                <tr>
                                    <td>Last Updated On</td>
                                    <td>{{ $vehicle->updated_at->format('jS M, Y @ h:i a') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card mb-4">
                <div class="card-body">
                    <h4 class="header-title mb-4">Job Orders</h4>
                    @if ($vehicle->jobOrders->isNotEmpty())
                        <div class="">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="text-center my-3">
                            <h4 class="text-muted">
                                No job order found!
                            </h4>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3 order-1 order-md-2">
            <a href="{{ route('admin.vehicles.edit', $vehicle->id) }}" class="btn btn-warning btn-block mb-2">
                Edit
            </a>
            <button class="btn btn-danger waves-effect btn-block mb-2 delete_vehicle_btn"
                data-url="{{ route('admin.vehicles.destroy', $vehicle->id) }}"
                data-name="{{ $vehicle->plate_number }}"
            >
                Delete
            </button>
            <a href="{{ route('admin.customers.show', $vehicle->customer_id) }}" class="btn btn-block btn-secondary waves-effect mb-2">
                Customer
            </a>
            <a href="" class="btn btn-block btn-secondary waves-effect mb-2">
                New Job Order
            </a>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/vehicle.js') }}"></script>
@endpush