@extends('layouts.admin.app')

@section('title', 'Product')
@section('page_title', 'Product')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.products.index') }}">Product</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($product->name, 20) }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/drop_uploader.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardInfo" role="button"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">Product Details</h5>
                    <div id="cardInfo" class="collapse pt-3 show">
                        <table class="table table-borderless">
                            <thead>
                                <th width="22%"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Product</td>
                                    <td>{{ $product->name }}</td>
                                </tr>
                                <tr>
                                    <td>Code</td>
                                    <td>{{ $product->code }}</td>
                                </tr>
                                <tr>
                                    <td>Product Type</td>
                                    <td>{{ $product->productType->name }}</td>
                                </tr>
                                <tr>
                                    <td>Description</td>
                                    <td>{{ $product->description }}</td>
                                </tr>
                                <tr>
                                    <td>Added</td>
                                    <td>{{ $product->created_at->format('jS M, Y @ h:i a') }}</td>
                                </tr>
                                <tr>
                                    <td>Last Updated</td>
                                    <td>{{ $product->updated_at->format('jS M, Y @ h:i a') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
      </div>
      <div class="col-md-3">
            <button class="btn btn-warning waves-effect btn-block mb-2 edit_product_btn">
                Edit
            </button>
            @if ($product->canDelete())
                <button class="btn btn-danger btn-block waves-effect mb-2 delete_product_btn"
                    data-name="{{ $product->name }}"
                    data-url="{{ route('admin.products.destroy', $product->id) }}"
                >
                    Delete
                </button>
            @endif
            <a class="btn btn-secondary btn-block waves-effect mb-2" href="">
                Gallery
            </a>

      </div>
    </div>

    {{-- modal --}}
    @include('admin.products.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/drop_uploader.js') }}"></script>
    <script src="{{ asset('js/admin/product.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddProductModal();
            @endif

            $('#images').drop_uploader({
                uploader_text: 'Drop image to upload, or',
                browse_css_class: 'btn btn-dark',
            });
        })
    </script>
@endpush