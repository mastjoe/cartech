<div class="modal fade" id="add_product_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">
                        New Product
                    </h4>
                    <button class="close" data-dismiss="modal" type="button">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if ($product_types->isEmpty())
                        <div class="alert alert-danger alert-dismissable">
                            No product type available!
                            <button class="close" type="button" data-dismiss="alert"><span>&times;</span></button>
                        </div>
                    @endif
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            name="name"
                            id="name"
                            required
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="code">Code</label>
                        <x-form.input
                            name="code"
                            id="code"
                            required
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="type">Product Type</label>
                        <x-form.select id="type"
                            name="product_type_id"
                        >
                            <option value="">Choose Product Type</option>
                            @foreach ($product_types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </x-form.select>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            name="description"
                            id="description"
                        ></x-form.textarea>
                    </div>
                    <div class="form-group">
                        <label for="images">Images</label>
                        <x-form.input
                            name="images[]"
                            id="images"
                            type="file"
                            multiple
                        ></x-form.input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" type="button" data-dismiss="modal">
                        Dismiss
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>