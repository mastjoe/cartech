@extends('layouts.admin.app')

@section('title', 'Product')
@section('page_title', 'Product')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Product
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/drop_uploader.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($products->isNotEmpty())                
                <div class="mb-2 text-right">
                    <button class="btn btn-secondary waves-effect add_product_btn">
                        New Product
                    </button>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">
                            All Products
                        </h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 nowrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Type</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td><a href="{{ route('admin.products.show', $product->id) }}">{{ $product->name }}</a></td>
                                        <td>{{ $product->code }}</td>
                                        <td>{{ $product->productType->name }}</td>
                                        <td>
                                            {{ $product->created_at->diffForHumans() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">No product found!</h2>
                    <button class="btn btn-secondary my-3 add_product_btn">
                        Add New Product
                    </button>
                </div>
            @endif
        </div>
    </div>

    {{-- modal --}}
    @include('admin.products.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/drop_uploader.js') }}"></script>
    <script src="{{ asset('js/admin/product.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddProductModal();
            @endif

            $('#images').drop_uploader({
                uploader_text: 'Drop image to upload, or',
                browse_css_class: 'btn btn-dark',
            });
        })
    </script>
@endpush