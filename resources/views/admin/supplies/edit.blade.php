@extends('layouts.admin.app')

@section('title', 'All Supplies')
@section('page_title', 'All Supplies')

@push('breadcrumb')
    <li class="breadcrumb-item">
        Supplies
    </li>
    <li class="breadcrumb-item">
        {{ $supply->reference }}
    </li>
    <li class="breadcrumb-item active">
        Edit
    </li>
@endpush

@push('css')
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.supplies.update', $supply->id) }}" method="POST">
                    @csrf
                    @method('put')
                    <div class="card-body">
                        <h4 class="header-title">
                            Update Supply
                        </h4>
                        <p class="font-13 sub-header">
                            fill form fields to update supply.
                        </p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                <div class="form-group">
                                    @if ($suppliers->isEmpty())
                                        <div class="alert alert-danger alert-dismissable">
                                            No supplier found! Kindly create a supplier record!
                                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                                        </div>
                                    @endif
                                    <label for="supplier">Supplier</label>
                                    <x-form.select id="supplier" name="supplier">
                                        <option value="">Choose Supplier</option>
                                        @foreach ($suppliers as $supplier)
                                            <option value="{{ $supplier->id }}"
                                                @if (old('supplier_id') == $supplier->id) 
                                                    selected 
                                                @elseif($supply->supplier_id == $supplier->id) 
                                                    selected 
                                                @endif  
                                            >
                                                {{ $supplier->name }}
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>

                                <div class="form-group">
                                    <label for="note">Note</label>
                                    <x-form.textarea rows="3" id="note" name="note">{{ old('note') ?? $supply->note }}</x-form.textarea>
                                </div>

                                <div class="form-group mt-3 text-center">
                                    <button class="btn btn-secondary waves-effect px-4" type="submit">
                                        Save Supply
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/supplier.js') }}"></script>
@endpush