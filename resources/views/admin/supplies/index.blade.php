@extends('layouts.admin.app')

@section('title', 'All Supplies')
@section('page_title', 'All Supplies')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Supplies
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($supplies->isNotEmpty())
                <div class="text-right mb-2">
                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.suppliers.create') }}">New Supply</a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Supplies</h4>
                        <p class="font-13 sub-header"></p>
                        <div>
                            <table class="table table-hover nowrap w-100 dt-responsive datatable">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
                                        <th>Supplier</th>
                                        <th>Items</th>
                                        <th>Total Cost</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($supplies as $supply)
                                        <tr>
                                            <td><a href="{{ route('admin.supplies.show', $supply->id) }}">{{ $supply->reference }}</a></td>
                                            <td>{{ $supply->supplier->name }}</td>
                                            <td>{{ $supply->items->count() }}</td>
                                            <td></td>
                                            <td>{{ $supply->created_at->format('jS M, Y') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted my-2">No supply found!</h2>
                    <a href="{{ route('admin.supplies.create') }}" class="btn btn-secondary waves-effect">Add New Supply</a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/supply.js') }}"></script>
@endpush