<div class="modal fade" id="add_item_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.supplies.add.item', $supply->id) }}" method="POST">
                <div class="modal-header">
                    <h4 class="modal-title">New Supply Item</h4>
                    <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    {{-- product --}}
                    <div class="form-group">
                        <label for="product">Product</label>
                        <x-form.select
                            name="product_id"
                            id="product"
                            required
                        >
                            <option value="">Choose Product</option>
                            @foreach ($products as $product)
                            <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </x-form.select>
                    </div>
                    {{-- quantity --}}
                    <div class="form-group">
                        <label for="quantity">Quantity</label>
                        <x-form.input
                            type="number"
                            min="1"
                            step="1"
                            required
                            name="quantity"
                        ></x-form.input>
                    </div>
                    {{-- cost price --}}
                    <div class="form-group">
                        <label for="cost_price">Unit Cost Price</label>
                        <x-form.input
                            name="cost_price"
                            id="cost_price"
                            required
                            class="currency__input"
                        ></x-form.input>
                    </div>
                    {{-- selling price --}}
                    <div class="form-group">
                        <label for="selling_price">Unit Selling Price</label>
                        <x-form.input
                            name="selling_price"
                            id="selling_price"
                            required
                            class="currency__input"
                        ></x-form.input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary waves-effect" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-secondary waves-effect">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>