@extends('layouts.admin.app')

@section('title', 'Supply - '.$supply_item->supply->reference)
@section('page_title', 'Supply - '.$supply_item->supply->reference)

@push('breadcrumb')
    <li class="breadcrumb-item ">
        <a href="{{ route('admin.supplies.index') }}">Supplies</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.supplies.show', $supply_item->supply->id) }}">{{ $supply_item->supply->reference }}</a>
    </li>
    <li class="breadcrumb-item">
        Supply Item
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-9 order-md-1 order-2">
           <div class="card">
               <div class="card-body">
                    <h4 class="header-title mb-3">Supply Item Details</h4>
                    <div>
                        <table class="table table-borderless">
                            <thead>
                                <th width="25%"></th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Product</td>
                                    <td>{{ $supply_item->product->name }}</td>
                                </tr>
                                <tr>
                                    <td>Product Type</td>
                                    <td>{{ $supply_item->product->productType->name }}</td>
                                </tr>
                                <tr>
                                    <td>Supplier</td>
                                    <td>{{ $supply_item->supply->supplier->name }}</td>
                                </tr>
                                <tr>
                                    <td>Supply Reference</td>
                                    <td>{{ $supply_item->supply->reference }}</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>Quantity supplied</td>
                                    <td>{{ number_format($supply_item->quantity_supplied) }}</td>
                                </tr>
                                <tr>
                                    <td>Quantity Left</td>
                                    <td>{{ number_format($supply_item->quantity) }}</td>
                                </tr>
                                <tr>
                                    <td>Cost Price</td>
                                    <td>NGN {{ number_format($supply_item->cost_price, 2) }}</td>
                                </tr>
                                <tr>
                                    <td>Selling Price</td>
                                    <td>NGN {{ number_format($supply_item->selling_price, 2) }}</td>
                                </tr>
                                <tr>
                                    <td>Created On</td>
                                    <td>{{ $supply_item->created_at->format('jS M, Y @ h:i s') }}</td>
                                </tr>
                                <tr>
                                    <td>Updated On</td>
                                    <td>{{ $supply_item->updated_at->format('jS M, Y @ h:i s') }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
               </div>
           </div>
        </div>
        <div class="col-md-3 order-md-2 order-1">
            <button class="btn btn-warning btn-block mb-2 add_item_btn">
               Edit
            </button>
            <button class="btn btn-danger btn-block mb-2 delete_item_btn"
                data-name="{{ $supply_item->product->name }}"
                data-url="{{ route('admin.supplies.destroy.item', [$supply_item->supply_id, $supply_item->id]) }}"
            >
               Delete
            </button>
            <a href="{{ route('admin.supplies.show', $supply_item->supply_id) }}" class="btn btn-secondary btn-block b-2">
               Supply
            </a>
        </div>
    </div>
    {{-- modals --}}
    @include('admin.supplies.items.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/supply.js') }}"></script>
@endpush