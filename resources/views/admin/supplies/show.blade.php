@extends('layouts.admin.app')

@section('title', 'All Supplies')
@section('page_title', 'All Supplies')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.supplies.index') }}">Supplies</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $supply->reference }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card-box">
                <h4 class="header-title">Supply {{ $supply->reference }}</h4>
                <div class="my-4">
                    <h4 class="font-13 text-muted text-uppercase">Supplier:</h4>
                    <p class="mb-3">{{ $supply->supplier->name }}</p>

                    <h4 class="font-13 text-muted text-uppercase">Status:</h4>
                    <p class="mb-3">
                        {!! \App\Helpers\HtmlStatus::supplyStatus($supply->status->name) !!}
                    </p>

                    <h4 class="font-13 text-muted text-uppercase">Note:</h4>
                    <p class="mb-3">{{ $supply->note ?? '-' }}</p>

                    <h4 class="font-13 text-muted text-uppercase">Created On:</h4>
                    <p class="mb-3">{{ $supply->created_at->format('jS, M, Y @h:i a') }}</p>

                    <div class="my-3">
                        @if ($supply->canAddItems())
                        <button class="btn btn-secondary btn-block waves-effect mb-2 add_item_btn">
                            Add Supply Item
                        </button>
                        @endif

                        <a class="btn btn-warning waves-effect mb-2 btn-block" 
                            href="{{ route('admin.supplies.edit', $supply->id) }}">
                            Edit
                        </a>

                        @if ($supply->canConfirm())
                        <button class="btn btn-success btn-block waves-effect mb-2 confirm_supply_btn"
                            data-url="{{ route('admin.supplies.confirm', $supply->id) }}"
                            data-name="{{ $supply->reference }}"
                        >
                            Confirm Supply
                        </button>
                        @endif

                        @if ($supply->canDelete())
                        <button class="btn btn-danger btn-block waves-effect mb-2 delete_supply_btn" 
                            data-url="{{ route('admin.supplies.destroy', $supply->id) }}"
                            data-name="{{ $supply->reference }}"
                        >
                            Delete
                        </button>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card-box">
                @if ($supply->items->isNotEmpty())
                    <div>
                        <table class="table table-responsive-sm datatable table-hover">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Cost Price</th>
                                    <th>Selling Price</th>
                                    <th>Item Sup.</th>
                                    <th>Item Left</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($supply->items as $item)
                                    <tr>
                                        <td>{{ $item->product->name }}</td>
                                        <td>{{ number_format($item->cost_price, 2) }}</td>
                                        <td>{{ number_format($item->selling_price, 2) }}</td>
                                        <td>{{ number_format($item->quantity_supplied) }}</td>
                                        <td>{{ number_format($item->quantity) }}</td>
                                        <td>
                                            <a href="{{ route('admin.supplies.show.item', [$supply, $item->id]) }}" class="btn btn-sm btn-dark waves-effect item_details_btn">
                                                Details
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="text-center my-5">
                        <h3 class="text-muted">No supply item found!</h3>
                        <button class="btn btn-secondary waves-effect px-2 mt-2 add_item_btn">
                            Add item to supply
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>

    {{-- modal --}}
    @include('admin.supplies.items.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/supply.js') }}"></script>
@endpush