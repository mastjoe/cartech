@extends('layouts.admin.app')

@section('title', 'Job Orders')
@section('page_title', 'Job Orders')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Job Orders
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($job_orders->isNotEmpty())
                <div class="text-right mb-2">
                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.job-orders.create') }}">New Job Order</a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Job orders</h4>
                        <p class="font-13 sub-header"></p>
                        <div>
                            <table class="table table-hover nowrap w-100 dt-responsive datatable">
                                <thead>
                                    <tr>
                                        <th>Ref</th>
                                        <th>Vehicle</th>
                                        <th>Customer</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($job_orders as $order)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.job-orders.show', $order->id) }}">{{ $order->reference }}</a>
                                            </td>
                                            <td>{{ $order->vehicle->plate_number }}</td>
                                            <td>{{ $order->customer->name }}</td>
                                            <td>{{ $order->status->name }}</td>
                                            <td>{{ $order->created_at->format('jS M, Y') }}</td>
                                            <td></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted my-2">No job order found!</h2>
                    <a href="{{ route('admin.job-orders.create') }}" class="btn btn-secondary waves-effect">Add New Job Order</a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/job-order.js') }}"></script>
@endpush