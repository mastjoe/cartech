@extends('layouts.admin.app')

@section('title', 'Job Orders - '.$job_order->reference)
@section('page_title', 'Job Orders - '.$job_order->reference)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        {{ $job_order->reference }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .jumbotron-bg-1 {
            background-image: url('../../images/repairs1.jpg');
        }
    </style>
@endpush

@section('content')
    <div class="jumbotron profile-jumbotron jumbotron-bg-1">
        <div class="text-center">
            <div class="">
                <h1 class="text-white">{{ $job_order->reference }}</h1>
                <h5 class="text-white">{{ $job_order->vehicle->plate_number }}</h5>
                <h6 class="text-warning">{{ $job_order->customer->name }}</h6>
            </div>
            <div class="py-2">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="div col-md-9">
            {{-- job order details --}}
            <div class="card mb-3">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardInfo" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0"> Job Order Details</h5>
                    <div id="cardInfo" class="collapse pt-3 show">
                        <h4 class="font-13 text-muted text-uppercase">Reference :</h4>
                        <p class="mb-3">{{ $job_order->reference }}</p>

                        <h4 class="font-13 text-muted text-uppercase">vehicle:</h4>
                        <p class="mb-3">
                            @can('view', $job_order->vehicle)
                                <a href="{{ route('admin.job-orders.show', $job_order->id) }}">
                                    {{ $job_order->vehicle->plate_number }}
                                </a>
                            @else
                                {{ $job_order->vehicle->plate_number }}
                            @endcan
                        </p>

                        <h4 class="font-13 text-muted text-uppercase">Customer :</h4>
                        <p class="mb-3">
                            @can ('view', $job_order->customer)
                                <a href="{{ route('admin.customers.show', $job_order->id) }}">{{ $job_order->customer->name }}</a>
                            @else
                                {{ $job_order->customer->name }}
                            @endcan
                        </p>
                        <h4 class="font-13 text-muted text-uppercase">Manager :</h4>
                        <p class="mb-3">
                            @if ($job_order->manager_id)
                                @can ('view', $job_order->manager)
                                    <a href="{{ route('admin.users.show', $job_order->manager_id) }}">{{ $job_order->manager->full_names }}</a>
                                @else
                                    {{ $job_order->manager->full_names }}
                                @endcan
                            @else
                                <span class="text-muted">No manager</span>
                                <button class="assign_manager_btn btn btn-link btn-sm waves-effect ml-2">
                                    Assign Manager<i class="fas fa-tag pl-2"></i>
                                </button>
                            @endif
                        </p>
                        <h4 class="font-13 text-muted text-uppercase">Created By :</h4>
                        <p class="mb-3">
                            @can ('view', $job_order->createdBy)
                                <a href="{{ route('admin.users.show', $job_order->created_by) }}">{{ $job_order->createdBy->full_names }}</a>
                            @else
                                {{ $job_order->createdBy->full_names }}
                            @endcan
                        </p>
                        <h4 class="font-13 text-muted text-uppercase">Created On :</h4>
                        <p class="mb-3">
                            {{ $job_order->created_at->format('jS F, Y @ h:i a') }}
                        </p>
                    </div>
                </div>
            </div>
            {{-- job order inspection --}}
            <div class="card mb-3">
                <div class="card-body">
                    <div class="card-widgets">
                        <a data-toggle="collapse" href="#cardInspection" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">Inspection</h5>
                    <div id="cardInfo" class="collapse pt-3 show">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="div col-md-3">
            <button class="btn btn-secondary btn-block d-md-none mb-3"><i class="fa fa-nav"></i>Toggle Actions</button>
            <div class="mobile-menu d-none d-md-block">
                @can('update', $job_order)
                    @if ($job_order->canEdit())
                        <a class="btn btn-warning waves-effect btn-block mb-2" href="{{ route('admin.job-orders.edit', $job_order->id) }}">
                            Edit
                        </a>
                    @endif
                @endcan
                @can('delete', $job_order)
                    @if ($job_order->canDelete())
                        <button class="btn btn-danger btn-block waves-effect mb-2 delete_job_order_btn"
                            data-name="{{ $job_order->reference }}"
                            data-url="{{ route('admin.job-orders.destroy', $job_order->id) }}"
                        >
                            Delete Order
                        </button>
                    @endif
                @endcan
                <button class="btn btn-secondary btn-block waves-effect mb-2 assign_manager_btn">
                    Assign Manager
                </button>
                
                <a href="{{ route('admin.job-orders.inspection', $job_order->id) }}" class="btn btn-secondary btn-block waves-effect mb-2">
                    Inspection
                </a>
                <a href="{{ route('admin.job-orders.faults', $job_order->id) }}" class="btn btn-secondary btn-block waves-effect mb-2">
                    Faults
                </a>
                <a href="{{ route('admin.job-orders.services', $job_order->id) }}" class="btn btn-secondary btn-block mb-2 wave-effect">
                    Services
                </a>
            </div>
        </div>
    </div>

    {{-- modals --}}
    @include('admin.job-orders.assign-manager')

@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/job-order.js') }}"></script>
@endpush