@extends('layouts.admin.app')

@section('title', 'Job Orders - '.$job_order->reference.' Inspection')
@section('page_title', 'Job Orders - '.$job_order->reference.' Inspection')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.show', $job_order->id) }}">{{ $job_order->reference }}</a>
    </li>
    <li class="breadcrumb-item active">
        Inspection
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .result-actions {
            opacity: 0;
            transition: all 0.3s ease-in;
        }
        .result-row:hover .result-actions{
            opacity:1;
        }
    </style>
@endpush

@section('content')
   <div class="row">
       <div class="col-12">
           <div class="card mb-4">
               <div class="card-body">
                   <h4 class="header-title">Inspection Details</h4>
                   @if ($job_order->inspection_note)
                        <h4 class="text-muted">Note</h4>
                        {{ $job_order->inspection_note }}
                        <br>
                        <br>
                        <h4 class="text-muted">Inspected By</h4>
                        @can('view', $job_order->inspector)
                            <a href="{{ route('admin.staff.show', $job_order->inpsected_by) }}">
                                {{ $job_order->inspector->full_name }}
                            </a>
                        @else                           
                            {{ $job_order->inspector->full_name }}
                        @endcan
                        <div class="mt-3 text-right">
                            <a href="javascript:;" class="update_inspection_btn">
                                <span class="fas fa-pencil-alt"></span>
                            </a>
                        </div>
                   @else
                       <div class="text-center my-4">
                           <h4 class="text-muted">No inspection note found!</h4>
                           <button class="btn btn-dark waves-effect update_inspection_btn my-3">
                               Add Note
                           </button>
                       </div>
                   @endif
               </div>
           </div>
           @if ($job_order->inspected_by)
                <div class="card">
                    <div class="card-body">
                        <div class="card-widgets">
                            <a data-toggle="collapse" href="#cardRes" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                            <a class="add_result_btn" href="#" role="button" title="Add New Result" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-plus"></i></a>
                        </div>
                        <h5 class="card-title mb-0"> Inspection Result(s)</h5>
                        <div id="cardRes" class="mt-2">
                            @if ($job_order->inspectionItems->isNotEmpty())
                                @foreach ($job_order->inspectionItems as $item)
                                    <div class="row my-2 result-row">
                                        <div class="col-9 col-md-10">
                                            <p class="my-2">{{ $item->result }}</p>
                                        </div>
                                        <div class="col-md-2 col-3 result-actions">
                                            <a href="javascript:;" class="m-1 text-warning update_result_btn"
                                                data-url="{{ route('admin.job-orders.inspection.update-item', [$job_order->id, $item->id]) }}"
                                                data-result="{{ $item->result }}"
                                            >
                                                <span class="fas fa-pencil-alt"></span>
                                            </a>
                                            <a  href="javascript:;" class="m-1 text-danger delete_result_btn"
                                                data-url="{{ route('admin.job-orders.inspection.remove-item',[$job_order->id, $item->id]) }}"
                                            >
                                                <span class="fas fa-trash-alt"></span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                <div class="text-center my-4">
                                    <h4 class="text-muted">No inspection result entered yet!</h4>
                                    <button class="btn btn-dark waves-effect add_result_btn my-3">
                                        Add Result
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
           @endif
       </div>
   </div>

   {{-- modals --}}
   @include('admin.job-orders.inspection.edit')
   @include('admin.job-orders.inspection.add-result')
   @include('admin.job-orders.inspection.update-result')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/job-order.js') }}"></script>
@endpush