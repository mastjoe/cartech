<div class="modal fade" id="update_result_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h4 class="modal-title">Update Result</h4>
                    <button class="close" type="button"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="uresult">Result</label>
                        <x-form.textarea
                            name="result"
                            id="uresult"
                            rows="3"
                        ></x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" data-dismiss="modal" type="button">
                        Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>