<div class="modal fade" id="update_inspection_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('admin.job-orders.inspection.update', $job_order->id) }}">
            @csrf
            @method('put')
                <div class="modal-header">
                    <h4 class="modal-title">Inspection</h4>
                    <button class="close" data-dismiss="modal" type="button">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="note">Inspection Note</label>
                        <x-form.textarea
                            name="note"
                            id="note"
                            rows="4"
                            required
                        >{{ $job_order->inspection_note }}</x-form.textarea>
                    </div>
                    <div class="form-group">
                        <label for="inspector">Inpsector</label>
                        <x-form.select
                            name="inspector"
                            id="inspector"
                            required
                        >
                            <option value="">Choose Inspector</option>
                            @foreach ($branch_users as $user)
                                <option value="{{ $user->id }}"
                                    @if ($job_order->inspected_by == $user->id)
                                        selected                                        
                                    @endif    
                                >
                                    {{ $user->full_names }}
                                </option>
                            @endforeach
                        </x-form.select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" 
                        data-dismiss="modal"
                        type="button"
                    >
                    Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect"
                        type="submit"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>