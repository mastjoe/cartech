<div class="modal fade" id="add_result_modal" tabindex="-13">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('admin.job-orders.inspection.add-item', $job_order->id) }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">Add Result</h4>
                    <button class="close" data-dismiss="modal" type="button">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="result">Result</label>
                        <x-form.textarea
                            name="result"
                            id="result"
                            rows="3"
                            required
                        ></x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" data-dismiss="modal" type="button">
                        Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>