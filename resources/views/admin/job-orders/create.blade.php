@extends('layouts.admin.app')

@section('title', 'Job Orders')
@section('page_title', 'Job Orders')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        New
    </li>
@endpush

@push('css')
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <form action="{{ route('admin.job-orders.store') }}" method="POST">
                    @csrf
                    <div class="card-body">
                        <h4 class="header-title">New Job Order</h4>
                        <p class="font-13 subheader-title"></p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        {{ $errors->first() }}
                                        <button class="close"type="button" data-dismiss="alert">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                @endif
                                {{-- vehicles --}}
                                <div class="form-group">
                                    <label for="vehicle">Vehicle</label>
                                    <x-form.select
                                        name="vehicle_id"
                                        id="vehicle"
                                    >
                                        <option value="">Choose Vehicle</option>
                                        @foreach ($vehicles as $vehicle)
                                            <option value="{{ $vehicle->id }}"
                                                data-name="{{ $vehicle->customer->name }}"
                                                data-phone="{{ $vehicle->customer->phone }}"
                                                @if (old('vehicle_id') && old('vehicle_id') == $vehicle->id)
                                                    selected                                                    
                                                @endif   
                                            >
                                                {{ $vehicle->plate_number }} - {{ $vehicle->customer->name }}
                                            </option>
                                        @endforeach
                                    </x-form.select>
                                </div>
                                {{-- customer name --}}
                                <div class="form-group d-none hiddeable-form-group">
                                    <label for="customerName">Customer Name</label>
                                    <x-form.input
                                        name="customer_name"
                                        id="customerName"
                                        disabled
                                    ></x-form.input>
                                </div>
                                {{-- customer phone --}}
                                <div class="form-group d-none hiddeable-form-group">
                                    <label for="customerPhone">Customer Phone</label>
                                    <x-form.input
                                        name="customer_phone"
                                        id="customerPhone"
                                        disabled
                                    ></x-form.input>
                                </div>

                                <div class="form-group text-center my-3">
                                    <button class="btn btn-secondary waves-effect px-3" type="submit">
                                        Create New Job Order
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/admin/job-order.js') }}"></script>
    <script>
        $(document).ready(function() {
            showCustomerDetailWhenVehicleIsPicked($('#vehicle'));

            $('#vehicle').on('change', function() {
                showCustomerDetailWhenVehicleIsPicked($(this));
            });
        });
    </script>
@endpush