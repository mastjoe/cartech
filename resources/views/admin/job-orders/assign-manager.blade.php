<div class="modal fade" tableindex="-1" id="assign_manager_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.job-orders.manager', $job_order->id) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">
                        Assign Manager
                    </h4>
                    <button class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="manager">Manager</label>
                        <x-form.select name="manager" id="manager">
                            <option value="">Choose Manager</option>
                            @foreach ($branch_users as $user)
                                <option value="{{ $user->id }}"
                                    @if ($user->id == $job_order->manager_id)
                                        selected
                                    @endif    
                                >
                                    {{ $user->full_names }}
                                </option>
                            @endforeach
                        </x-form.select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" 
                        data-dismiss="modal"
                        type="button"
                    >
                    Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect"
                        type="submit"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>