@extends('layouts.admin.app')

@section('title', 'Job Orders - '.$job_order->reference.' Fault')
@section('page_title', 'Job Orders - '.$job_order->reference.' Fault')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.show', $job_order->id) }}">{{ $job_order->reference }}</a>
    </li>
    <li class="breadcrumb-item active">
        Faults
    </li>
@endpush

@push('css')
    <style>
        .fault-actions {
            opacity: 0;
            transition: all 0.3s ease-in;
        }
        .fault-row:hover .fault-actions{
            opacity:1;
        }
    </style>
@endpush

@section('content')
   <div class="row">
       <div class="col-12">
           @if ($job_order->faultItems->isNotEmpty())
                <div class="text-right mb-2">
                    <button class="btn btn-secondary waves-effect add_fault_btn">
                        Add More Fault
                    </button>
                </div>
                <div class="card">
                   <div class="card-body">
                       <h4 class="header-title">Vehicle Fault ({{ $job_order->reference }})</h4>
                       <div class="my-3">
                           @foreach ($job_order->faultItems as $key => $item)
                               <div class="row my-2 fault-row">
                                   <div class="col-9">
                                       <p>
                                          <span class="pr-2">{{ $key + 1 }}.</span>
                                          {{ $item->fault }}
                                       </p>
                                   </div>
                                   <div class="col-3 fault-actions">
                                        <a href="javascript:void(0)" class="m-1 text-warning update_fault_btn"
                                            data-url="{{ route('admin.job-orders.faults.update', [$job_order->id, $item->id]) }}"
                                            data-fault="{{ $item->fault }}"
                                        >
                                            <span class="fas fa-pencil-alt"></span>
                                        </a>
                                        <a href="javascript:void(0)" class="m-1 text-danger delete_fault_btn"
                                            data-name="{{ $item->fault }}"
                                            data-url="{{ route('admin.job-orders.faults.destroy', [$job_order->id, $item->id]) }}"
                                        >
                                            <span class="fas fa-trash"></span>
                                        </a>
                                   </div>
                               </div>
                           @endforeach
                       </div>
                   </div>
                </div>
           @else
               <div class="text-center my-5">
                   <h2 class="text-muted">No fault found for job order {{ $job_order->reference }}</h2>
                   <button class="btn btn-secondary waves-effect add_fault_btn my-3">
                       Add Fault
                   </button>
               </div>
           @endif
       </div>
   </div>

   {{-- modals --}}
   @include('admin.job-orders.faults.create')
   @include('admin.job-orders.faults.edit')
   
@endsection

@push('js')
    <script src="{{ asset('js/admin/job-order.js') }}"></script>
@endpush