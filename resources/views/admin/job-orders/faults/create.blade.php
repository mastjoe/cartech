<div class="modal fade" id="add_fault_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form method="POST" action="{{ route('admin.job-orders.faults', $job_order->id) }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">
                        New Fault
                    </h4>
                    <button class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="fault">Fault</label>
                        <x-form.input
                            name="fault"
                            id="fault"
                            required
                        ></x-form.input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" 
                        data-dismiss="modal"
                        type="button"
                    >
                    Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect"
                        type="submit"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>