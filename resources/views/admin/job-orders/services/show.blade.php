@extends('layouts.admin.app')

@section('title', 'Job Orders - '.$job_order->reference.' Services')
@section('page_title', 'Job Orders - '.$job_order->reference.' Services')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.show', $job_order->id) }}">{{ $job_order->reference }}</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.job-orders.services', $job_order->id) }}">Services</a>
    </li>
    <li class="breadcrumb-item active">
        {{ $service->service->name }}
    </li>

@endpush

@push('css')
@endpush

@section('content')
   <div class="row">
       <div class="col-md-9">
           <div class="card">
               <div class="card-body">
                   <h5>Details of {{ $job_order->reference }} <span class="text-muted">{{ $service->service->name }}</span> Service</h5>
                   <div class="my-3">
                        <table class="table table-borderless">
                            <thead>
                                <tr>
                                    <th width="25%"></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Job Order</td>
                                    <td>{{ $job_order->reference }}</td>
                                </tr>
                                <tr>
                                    <td>Service</td>
                                    <td>{{ $service->service->name }}</td>
                                </tr>
                                <tr>
                                    <td>Manager</td>
                                    <td>
                                        @if (!is_null($service->managed_by))
                                            {{ $service->managedBy->full_name }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{!! \App\Helpers\HtmlStatus::jobOrderService($service->status_id) !!}</td>
                                </tr>
                                <tr>
                                    <td>Note</td>
                                    <td>
                                        @if (!is_null($service->note))
                                            {{ $service->note }}
                                        @else
                                            -
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Outsourced</td>
                                    <td>
                                        @if ($service->outsourced)
                                            <span class="badge badge-success">Yes</span>
                                        @else
                                            <span class="badge badge-secondary">No</span>
                                        @endif
                                    </td>
                                </tr>
                                @if ($service->outsourced)
                                    <tr>
                                        <td>Outsourced Company</td>
                                        <td>{{ $service->outsource_company }}</td>
                                    </tr>
                                @endif
                                <tr>
                                    <td>Added</td>
                                    <td>
                                        {{ $service->created_at->format('jS M, Y @ h:i a') }}
                                        <span class="text-muted pl-2">({{ $service->created_at->diffForHumans() }})</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Last Updated</td>
                                    <td>
                                        {{ $service->updated_at->format('jS M, Y @ h:i a') }}
                                        <span class="text-muted pl-2">({{ $service->updated_at->diffForHumans() }})</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                   </div>
               </div>
           </div>
       </div>
       <div class="col-md-3">
            @if ($service->canEdit())
            <button class="btn btn-warning btn-block waves-effect mb-2 edit_service_btn">
               Edit
            </button>
            @endif

            @if ($service->canDelete())
            <button class="btn btn-danger waves-effect btn-block mb-2 delete_service_btn"
                data-name="{{ $service->service->name }}"
                data-url="{{ route('admin.job-orders.services.destroy', [$job_order->id, $service->id]) }}"
            >
               Delete
            </button>
            @endif
       </div>
   </div>

   {{-- modals --}}
   @include('admin.job-orders.services.edit')
   
@endsection

@push('js')
    <script src="{{ asset('js/admin/job-order.service.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('edit'))
                showEditServiceModal();
            @endif
        })
    </script>
@endpush