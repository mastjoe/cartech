{{-- edit job order service --}}
<div class="modal fade" tabindex="-1" id="edit_service_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.job-orders.services.update', [$job_order->id, $service->id]) }}" method="POST">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h4 class="modal-title">Update Service</h4>
                    <button class="close" type="button" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="service">Service</label>
                        <x-form.select name="service" required>
                            <option value="">Choose Service</option>
                            @foreach ($services as $s)
                                <option value="{{ $s->id }}"
                                    @if ($s->id == $service->service_id) selected @endif
                                >
                                    {{ $s->name }}
                                </option>
                            @endforeach
                        </x-form.select>
                    </div>
                    <div class="form-group">
                        <label for="note">Note</label>
                        <x-form.textarea
                            name="note"
                            id="note"
                        >{{ $service->note }}</x-form.textarea>
                    </div>
                    <div class="form-group">
                        <label for="manager">Service Manager</label>
                        <x-form.select
                            name="manager"
                            id="manager"
                        >
                            <option value="">Choose Manager</option>
                            @foreach ($branch_users as $user)
                                <option value="{{ $user->id }}"
                                    @if ($user->id == $service->managed_by) selected @endif
                                >
                                    {{ $user->full_name }}
                                </option>
                            @endforeach
                        </x-form.select>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" id="outsourced" @if($service->outsourced) checked @endif>
                            <label class="form-check-label" for="outsourced">
                              Is service outsourced
                            </label>
                        </div>
                    </div>
                    <div class="form-group outsource_target d-none">
                        <label for="outsource_company">Outsource Company</label>
                        <x-form.input
                            name="outsource_company"
                            id="outsource_company"
                            value="{{ $service->outsource_company }}"
                        ></x-form.input>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" 
                        data-dismiss="modal"
                        type="button"
                    >
                    Cancel
                    </button>
                    <button class="btn btn-secondary waves-effect"
                        type="submit"
                    >
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>