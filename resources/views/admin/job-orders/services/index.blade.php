@extends('layouts.admin.app')

@section('title', 'Job Orders - '.$job_order->reference.' Services')
@section('page_title', 'Job Orders - '.$job_order->reference.' Services')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.index') }}">Job Orders</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.job-orders.show', $job_order->id) }}">{{ $job_order->reference }}</a>
    </li>
    <li class="breadcrumb-item active">
        Services
    </li>
@endpush

@push('css')
    <style>
        .fault-actions {
            opacity: 0;
            transition: all 0.3s ease-in;
        }
        .fault-row:hover .fault-actions{
            opacity:1;
        }
    </style>
@endpush

@section('content')
   <div class="row">
       <div class="col-12">
           @if ($job_order->services->isNotEmpty())
                <div class="text-right mb-2">
                    <button class="btn btn-secondary waves-effect add_service_btn">
                        Add Service
                    </button>
                </div>
                <h4 class="header-title">Job Order Services</h4>
                <p class="font-13 subheader-title"></p>

                @foreach ($job_order->services as $key => $service)
                    <div class="card">
                        <div class="card-body">
                            <div class="card-widgets">
                                <a data-toggle="collapse" href="#cardPriv{{ $key }}" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                            </div>
                            <h4 class="card-title mb-0">{{ $service->service->name }}</h4>
                            <div id="cardPriv{{ $key }}" class="collapse pt-3 show">
                                <div class="text-right mt-3">
                                    @if ($service->canDelete())
                                    <button class="btn btn-danger waves-effect m-1 delete_service_btn"
                                        data-name="{{ $service->service->name }}"
                                        data-url="{{ route('admin.job-orders.services.destroy',[$job_order->id, $service->id]) }}"
                                    >
                                        Remove
                                    </button>
                                    @endif

                                    @if ($service->canEdit())
                                    <a class="btn btn-warning waves-effect m-1" href="{{ route('admin.job-orders.services.edit', [$job_order->id, $service->id]) }}">
                                        Edit
                                    </a>
                                    @endif

                                    <a class="btn btn-dark waves-effect m-1" href="{{ route('admin.job-orders.services.show', [$job_order->id, $service->id]) }}">
                                        View
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
           @else
               <div class="text-center my-5">
                   <h2 class="text-muted">No services assigned for job order {{ $job_order->reference }}</h2>
                   <button class="btn btn-secondary waves-effect add_service_btn my-3">
                       Add Service
                   </button>
               </div>
           @endif
       </div>
   </div>

   {{-- modals --}}
   @include('admin.job-orders.services.create')
   
@endsection

@push('js')
    <script src="{{ asset('js/admin/job-order.service.js') }}"></script>
@endpush