@extends('layouts.admin.app')

@section('title', 'Customer - '.$customer->name)
@section('page_title', 'Customer - '.$customer->name)

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.customers.index') }}">Customers</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.customers.show', $customer->id) }}">{{ Str::limit($customer->name, 15) }}</a>
    </li>
    <li class="breadcrumb-item active">
        Vehicles
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
   <div class="row">
       <div class="col-12">
           @if ($customer->vehicles->isNotEmpty()) 
                <div class="text-right mb-2">
                    <a class="btn btn-secondary waves-effect" href="{{ route('admin.vehicles.create', ['customer' => $customer->id]) }}">
                        Add Vehicle
                    </a>    
                </div>              
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">{{ $customer->name }} - Vehicles</h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table nowrap w-100 dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Reference</th>
                                    <th>Number</th>
                                    <th>Make</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customer->vehicles as $vehicle)
                                    <tr>
                                        <td><a href="{{ route('admin.vehicles.show', $vehicle->customer_id) }}">{{ $vehicle->reference }}</a></td>
                                        <td>{{ $vehicle->plate_number }}</td>
                                        <td>{{ $vehicle->make->name }}</td>
                                        <td>{{ $vehicle->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
           @else
               <div class="text-center my-5">
                   <h2 class="text-muted my-2">
                       Customer has no vehicle record
                   </h2>
                   <a href="{{ route('admin.vehicles.create', ['customer'=>$customer->id]) }}" class="btn btn-secondary waves-effect">
                       Add Vehicle Record
                   </a>
               </div>
           @endif
       </div>
   </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/customer.js') }}"></script>
@endpush