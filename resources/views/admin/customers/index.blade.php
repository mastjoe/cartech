@extends('layouts.admin.app')

@section('title', 'Customers')
@section('page_title', 'Customers')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        Customers
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($customers->isNotEmpty())
                <div class="text-right mb-2">
                    <a href="{{ route('admin.customers.create') }}" class="btn btn-secondary waves-effect">
                        New Customer
                    </a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Customers</h4>
                        <p class="font-13 sub-header"></p>
                        <div>
                            <table class="table w-100 nowrap datatable dt-responsive">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Vehicles</th>
                                        <th>Phone</th>
                                        <th>Email</th>
                                        <th>Branch</th>
                                        <th>Created</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $customer)
                                        <tr>
                                            <td>
                                                <a href="{{ route('admin.customers.show', $customer->id) }}">{{ $customer->name }}</a>
                                            </td>
                                            <td>{{ $customer->vehicles->count() }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>{{ $customer->email }}</td>
                                            <td>{{ $customer->branch->name }}</td>
                                            <td>{{ $customer->created_at->diffForHumans() }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">
                        No customer record found!
                    </h2>
                    <a href="{{ route('admin.customers.create') }}" class="btn btn-secondary px-4 waves-effect my-3">
                        Create New Customer Record
                    </a>
                </div>
            @endif
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/customer.js') }}"></script>
@endpush