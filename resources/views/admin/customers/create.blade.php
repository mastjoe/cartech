@extends('layouts.admin.app')

@section('title', 'New Customer')
@section('page_title', 'New Customer')

@push('breadcrumb')
    <li class="breadcrumb-item active">
        <a href="{{ route('admin.customers.index') }}">Customers</a>
    </li>
    <li class="breadcrumb-item active">
        New
    </li>
@endpush

@push('css')
    <link rel="stylesheet" href="{{ asset('css/drop_uploader.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
           <div class="card">
               <form action="{{ route('admin.customers.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <h4 class="header-title">New Customer Record</h4>
                        <p class="font-13 sub-header">Fill all form fields to create a customer's record</p>
                        <div class="row">
                            <div class="offset-md-3 col-md-6">
                                @if ($errors->any())
                                    <div class="alert alert-danger alert-dismissable">
                                        {{ $errors->first() }}
                                        <button class="close" type="button">
                                            <span>&times;</span>
                                        </button>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="name">Name</label>
                                    <x-form.input
                                        name="name"
                                        id="name"
                                        required
                                        value="{{ old('name') }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="branch">Branch</label>
                                    <x-form.input
                                        value="{{ $admin->branch->name }}"
                                        readonly
                                        id="branch"
                                    ></x-form.input>
                                </div>
        
                                <div class="form-group">
                                    <label for="phone">Phone</label>
                                    <x-form.input
                                        name="phone"
                                        id="phone"
                                        type="tel"
                                        required
                                        value="{{ old('phone') }}"
                                    ></x-form.input>
                                </div>
        
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <x-form.input
                                        name="email"
                                        id="email"
                                        type="email"
                                        value="{{ old('email') }}"
                                    ></x-form.input>
                                </div>

                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <x-form.textarea
                                        name="address"
                                        id="address"
                                        rows=2
                                    >{{ old('address') }}</x-form.textarea>
                                </div>

                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <x-form.input
                                        name="image"
                                        id="image"
                                        type="file"
                                    ></x-form.input>
                                </div>

                                <div class="form-group my-5 text-center">
                                    <button type="submit" class="btn btn-secondary btn-lg waves-effect px-5">
                                        Save Record
                                    </button>
                                </div>
                            </div>
                        </div>

                   </div>
               </form>
           </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('js/drop_uploader.js') }}"></script>
    <script src="{{ asset('js/admin/customer.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#image').drop_uploader({
                uploader_text: 'Drop image to upload, or',
                browse_css_class: 'btn btn-dark',
            });
        })
    </script>
@endpush