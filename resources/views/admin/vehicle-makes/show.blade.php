@extends('layouts.admin.app')

@section('title', 'Vehicle | Makes')
@section('page_title', 'Vehicle - Makes')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">vehicles</a>
    </li>
    <li class="breadcrumb-item">
        <a href="{{ route('admin.vehicle-makes.index') }}">Makes</a>
    </li>
    <li class="breadcrumb-item active">
        {{ Str::limit($vehicle_make->name, 15) }}
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
       <div class="col-md-4">
           <div class="card-box">
               <h4 class="border-bottom pb-2 header-title">{{ $vehicle_make->name }}</h4>
                <p class="mb-2 mt-2">
                    <strong class="text-muted">Description</strong>
                    <h5 class="mt-0">{{ $vehicle_make->description ?? '-' }}</h5>
                </p>
                <p class="mb-2">
                    <strong class="text-muted">Added</strong>
                    <h5 class="mt-0">{{ $vehicle_make->created_at->format('jS M, Y @ h:i a') ?? '-' }}</h5>
                </p>
                <div class="my-3">
                    <button href="" class="btn btn-warning btn-block mb-2 waves-effect edit_vehicle_make_btn">Edit</button>
                    @if ($vehicle_make->canDelete())                        
                        <button href="" class="btn btn-danger btn-block mb-2 waves-effect delete_vehicle_make_btn"
                            data-url="{{ route('admin.vehicle-makes.destroy', $vehicle_make->id) }}"
                            data-name="{{ $vehicle_make->name }}"
                        >Delete</button>
                    @endif
                </div>
           </div>
       </div>
       <div class="col-md-8">
           <div class="card">
               <div class="card-body">
                   <h4 class="header-title">Vehicles</h4>
                   @if ($vehicle_make->vehicles->isNotEmpty())
                       <div>
                           <table class="table no-wrap dt-responsive w-100 datatable">
                                <thead>
                                    <tr>
                                        <th>Vehicle</th>
                                        <th>Number</th>
                                        <th>Customer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($vehicle_make->vehicle as $vehicle)
                                        <tr>
                                            <td>{{ $vehicle->reference }}</td>
                                            <td>{{ $vehicle->plate_number }}</td>
                                            <td>{{ $vehicle->customer->name }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                           </table>
                       </div>
                   @else
                       <div class="text-center my-4">
                           <h3 class="text-muted">No vehicle found!</h3>
                       </div>
                   @endif
               </div>
           </div>
       </div>
    </div>

    {{-- modals --}}
    @include('admin.vehicle-makes.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/vehicle.make.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddVehicleMakeModal();
            @endif
        })
    </script>
@endpush