@extends('layouts.admin.app')

@section('title', 'Vehicle | Makes')
@section('page_title', 'Vehicle - Makes')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="">vehicles</a>
    </li>
    <li class="breadcrumb-item active">
        Makes
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            @if ($vehicle_makes->isNotEmpty())
                <div class="text-right mb-2">
                    <button class="btn btn-secondary waves-effect add_vehicle_make_btn">
                        New Make
                    </button>
                </div>
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Vehicle Makes</h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 no-wrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Make</th>
                                    <th>Vehicles</th>
                                    <th>Created</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($vehicle_makes as $make)
                                    <tr>
                                        <td><a href="{{ route('admin.vehicle-makes.show', $make->id) }}">{{ $make->name }}</a></td>
                                        <td>{{ $make->vehicles->count() }}</td>
                                        <td>{{ $make->created_at->diffForHumans() }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @else
                <div class="text-center my-5">
                    <h2 class="text-muted">No vehicle make found!</h2>
                    <button class="btn btn-secondary waves-effect my-2 add_vehicle_make_btn">
                        Add New Make
                    </button>
                </div>
            @endif
        </div>
    </div>

    {{-- modals --}}
    @include('admin.vehicle-makes.create')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/admin/vehicle.make.js') }}"></script>
    <script>
        $(document).ready(function() {
            @if(session('create'))
                showAddVehicleMakeModal();
            @endif
        })
    </script>
@endpush