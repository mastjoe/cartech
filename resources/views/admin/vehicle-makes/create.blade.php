<div class="modal fade" id="add_vehicle_make_modal" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.vehicle-makes.store') }}" method="POST" >
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">
                        New Make
                    </h4>
                    <button class="close" data-dismiss="modal" type="button">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            name="name"
                            id="name"
                            required
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            name="description"
                            id="description"
                        ></x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" type="button" data-dismiss="modal">
                        Dismiss
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>