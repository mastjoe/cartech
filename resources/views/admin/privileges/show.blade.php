@extends('layouts.admin.app')

@section('title', 'Privileges')
@section('page_title', 'Privileges')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>

    <li class="breadcrumb-item">
        <a href="{{ route('admin.privileges.index') }}">Privileges</a>
    </li>

    <li class="breadcrumb-item active">
        {{ Str::limit($privilege->name, 15) }}
    </li>
@endpush

@push('css')
@endpush

@section('content')
    <div class="row">
        <div class="col-md-4">
            <div class="card-box">
                <p class="mb-2">
                    <strong class="text-muted">Privilege</strong>
                    <h5 class="mt-0">{{ $privilege->name }}</h5>
                </p>
                <p class="mb-2">
                    <strong class="text-muted">Description</strong>
                    <h5 class="mt-0">{{ $privilege->description ?? '-' }}</h5>
                </p>
                <p class="mb-2">
                    <strong class="text-muted">Created</strong>
                    <h5 class="mt-0">{{ $privilege->created_at->diffForHumans() }}</h5>
                </p>
                <div class="my-3">
                    <button class="btn btn-block waves-effect btn-warning edit_privilege_btn">
                        <span class="fe-edit mr-2"></span>
                        Edit
                    </button>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            {{-- roles --}}
            <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardRole" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">
                        Roles
                    </h5>
                    <div id="cardRole" class="collapse pt-3 show">
                        @if ($privilege->roles->isNotEmpty())
                            <div class="row">
                                @foreach ($privilege->roles as $role)
                                    <div class="col-md-3 col-6">
                                        <div class="chip">{{ $role->name }}</div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="text-center my-3">
                                <h4 class="text-muted">
                                    No role found!
                                </h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            {{-- users --}}
             <div class="card">
                <div class="card-body">
                    <div class="card-widgets">
                        <a href="javascript:;" data-toggle="reload"><i class="mdi mdi-refresh"></i></a>
                        <a data-toggle="collapse" href="#cardUser" role="button" aria-expanded="false" aria-controls="cardCollpase1"><i class="mdi mdi-minus"></i></a>
                    </div>
                    <h5 class="card-title mb-0">
                        Staff
                    </h5>
                    <div id="cardUser" class="collapse pt-3 show">
                        @if ($privilege->users->isNotEmpty())
                           <table class="table w-100 nowrap dt-responsive datatable">
                               <thead>
                                   <tr>
                                       <th>Staff</th>
                                       <th>Email</th>
                                   </tr>
                               </thead>
                               <tbody>
                                   @foreach ($privilege->users as $user)
                                       <tr>
                                           <td>{{ $user->full_name }}</td>
                                           <td>{{ $user->email }}</td>
                                       </tr>
                                   @endforeach
                               </tbody>
                           </table>
                        @else
                            <div class="text-center my-3">
                                <h4 class="text-muted">
                                    No staff found with this privilege!
                                </h4>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- update modal --}}
    @include('admin.privileges.edit')
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/privilege.js') }}"></script>

    <script>
        $(document).ready(function() {
            @if(session('edit'))
                showEditPrivilegeModal();
            @endif
        })
    </script>
@endpush