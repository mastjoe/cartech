@extends('layouts.admin.app')

@section('title', 'Privileges')
@section('page_title', 'Privileges')

@push('breadcrumb')
    <li class="breadcrumb-item">
        <a href="{{ route('admin.staff.index') }}">Staff</a>
    </li>

    <li class="breadcrumb-item active">
        Privileges
    </li>
@endpush

@push('css')
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables/select.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')
    @if ($privileges->isNotEmpty())
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">All Privileges</h4>
                        <p class="font-13 sub-header"></p>
                        <table class="table w-100 nowrap dt-responsive datatable">
                            <thead>
                                <tr>
                                    <th>Privilege</th>
                                    <th>Roles</th>
                                    <th>Users</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($privileges as $privilege)
                                    <tr>
                                        <td>
                                            <a href="{{ route('admin.privileges.show', $privilege->id) }}">{{ $privilege->name }}</a>
                                        </td>
                                        <td>{{ $privilege->roles->count() }}</td>
                                        <td>
                                           {{ $privilege->users->count() }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="my-5 text-center">
            <h2 class="text-muted my-2">
                No privilege was found!
            </h2>
        </div>
    @endif
@endsection

@push('js')
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('js/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('js/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('js/datatables/vfs_fonts.js') }}"></script>

    <script src="{{ asset('js/admin/role.js') }}"></script>
@endpush