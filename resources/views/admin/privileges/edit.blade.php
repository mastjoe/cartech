{{-- create new role --}}
<div class="modal fade" id="edit_privilege_modal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="{{ route('admin.privileges.update', $privilege->id) }}" method="POST">
                @csrf
                @method('put')
                <div class="modal-header">
                    <h4 class="modal-title">
                        Edit Privilege
                    </h4>
                    <button class="close" type="button" data-dismiss="modal">
                        <span>&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <x-form.input
                            name="name"
                            id="description"
                            readonly
                            required
                            value="{{ old('name') ?? $privilege->name }}"
                        ></x-form.input>
                    </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <x-form.textarea
                            name="description"
                            id="description"
                        >{{ old('description') ?? $privilege->description }}</x-form.textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-secondary waves-effect" type="button" data-dismiss="modal">
                        Dismiss
                    </button>
                    <button class="btn btn-secondary waves-effect" type="submit">
                        Save
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>