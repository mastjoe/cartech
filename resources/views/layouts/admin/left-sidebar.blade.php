 <div class="h-100" data-simplebar>

                    <!-- User box -->
    <div class="user-box text-center">
        <img src="../assets/images/users/user-1.jpg" alt="user-img" title="Mat Helme"
            class="rounded-circle avatar-md">
        <div class="dropdown">
            <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block"
                data-toggle="dropdown">{{ $admin->full_name }}</a>
            <div class="dropdown-menu user-pro-dropdown">

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="fe-user mr-1"></i>
                    <span>My Account</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="fe-settings mr-1"></i>
                    <span>Settings</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" class="dropdown-item notify-item">
                    <i class="fe-lock mr-1"></i>
                    <span>Lock Screen</span>
                </a>

                <!-- item-->
                <a href="javascript:void(0);" onclick="logOutApp(event)" class="dropdown-item notify-item">
                    <i class="fe-log-out mr-1"></i>
                    <span>Logout</span>
                </a>

            </div>
        </div>
        <p class="text-muted">Admin Head</p>
    </div>

    <!--- Sidemenu -->
    <div id="sidebar-menu">

        <ul id="side-menu">

            <li class="menu-title">Navigation</li>

            <li>
                <a href="{{ route('admin.dashboard') }}">
                    <i data-feather="airplay"></i>
                    <span class="badge badge-success badge-pill float-right">4</span>
                    <span> Dashboards </span>
                </a>
            </li>

            <li class="menu-title mt-2">Apps</li>
            @can('viewAny', App\Models\Customer::class)
                <li>
                    <a href="#sidebarCrm" data-toggle="collapse">
                        <i class="fas fa-user-friends"></i>
                        <span> Customers </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarCrm">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.customers.index') }}">All</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.customers.create') }}">New</a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan

            @can('viewAny', App\Models\Vehicle::class)
                <li>
                    <a href="#sidebarVehicles" data-toggle="collapse">
                        <i class="mdi mdi-taxi"></i>
                        <span> Vehicles </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarVehicles">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.vehicles.index') }}">All</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.vehicles.create') }}">New</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.vehicle-makes.index') }}">Makes</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.vehicle-fuels.index') }}">Fuels</a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan

            @can('viewAny', App\Models\JobOrder::class)
                <li>
                    <a href="#sidebarProjects" data-toggle="collapse">
                        <i data-feather="briefcase"></i>
                        <span> Job Orders </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarProjects">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.job-orders.index') }}">All</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.job-orders.create') }}">New</a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan

            <li>
                <a href="#sidebarProducts" data-toggle="collapse">
                    <i data-feather="award"></i>
                    <span>Products</span>
                    <span class="menu-arrow"></span>
                </a>
                <div class="collapse" id="sidebarProducts">
                    <ul class="nav-second-level">
                        <li>
                            <a href="{{ route('admin.products.index') }}">All</a>
                        </li>
                        <li>
                            <a href="{{ route('admin.product-type.index') }}">Types</a>
                        </li>
                    </ul>
                </div>
            </li>

            @can('viewAny', App\Models\Supply::class)
                <li>
                    <a href="#sidebarSupplies" data-toggle="collapse">
                        <i data-feather="repeat"></i>
                        <span>Supplies</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarSupplies">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.supplies.index') }}">All</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.suppliers.index') }}">Suppliers</a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan

            <li class="menu-title mt-2">Custom</li>
            
            @can('viewAny', App\Models\User::class)
                <li>
                    <a href="#sidebarStaff" data-toggle="collapse">
                        <i data-feather="users"></i>
                        <span> Staff </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <div class="collapse" id="sidebarStaff">
                        <ul class="nav-second-level">
                            <li>
                                <a href="{{ route('admin.staff.index') }}">All</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.staff.create') }}">New</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.privileges.index') }}">Privileges</a>
                            </li>
                            <li>
                                <a href="{{ route('admin.roles.index') }}">Roles</a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan

            @if ($admin->hasAnyPrivilege(['view branch']))
                <li>
                    <a href="#sidebarAuth" data-toggle="collapse">
                        <i data-feather="settings"></i>
                        <span> Settings</span>
                        <span class="menu-arrow"></span>
                    </a>
                    @can('viewAny', App\Models\Branch::class)
                        <div class="collapse" id="sidebarAuth">
                            <ul class="nav-second-level">
                                <li>
                                    <a href="{{ route('admin.branches.index') }}">Branches</a>
                                </li>
                                <li>
                                    <a href="{{ route('admin.services.index') }}">Services</a>
                                </li>
                            </ul>
                        </div>
                    @endcan
                </li>
            @endif

            <li>
                <a href="#">
                    <i data-feather="calendar"></i>
                    <span>Audit</span>
                </a>
            </li>
        </ul>

    </div>
    <!-- End Sidebar -->

    <div class="clearfix"></div>

</div>