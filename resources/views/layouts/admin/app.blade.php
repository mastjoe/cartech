<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="" name="description" />
        <meta content="Newbbmsms" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="../assets/images/favicon.ico">

		<!-- App css -->
		<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
		<link href="{{ asset('css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

		<link href="{{ asset('css/bootstrap-dark.min.css') }}" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" disabled />
		<link href="{{ asset('css/app-dark.min.css') }}" rel="stylesheet" type="text/css" id="app-dark-stylesheet"  disabled />

		<!-- icons -->
        <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
        
        @stack('css')

    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Topbar Start -->
            <div class="navbar-custom">
                @include('layouts.admin.topbar')
            </div>
            <!-- end Topbar -->

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left-side-menu">
               @include('layouts.admin.left-sidebar')
            </div>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">

                    <!-- Start Content-->
                    <div class="container-fluid">
                        
                        <!-- start page title -->
                        <div class="row">
                            <div class="col-12">
                                <div class="page-title-box">
                                    <div class="page-title-right">
                                        <ol class="breadcrumb m-0">
                                            <li class="breadcrumb-item"><a href="">Dashboard</a></li>
                                            @stack('breadcrumb')
                                        </ol>
                                    </div>
                                    <h4 class="page-title">
                                        @hasSection('page_title')                                       
                                            @yield('page_title')
                                        @else
                                            @yield('title')
                                        @endif
                                    </h4>
                                </div>
                            </div>
                        </div>     
                        <!-- end page title --> 

                        @yield('content')
                        
                    </div> <!-- container -->

                </div> <!-- content -->

                <!-- Footer Start -->
                <footer class="footer">
                   @include('layouts.admin.footer')
                </footer>
                <!-- end Footer -->

            </div>

            <!-- =====================================`========================= -->
            <!-- End Page content -->
            <!-- ============================================================== -->


        </div>
        <!-- END wrapper -->

        <!-- Right Sidebar -->
        <div class="right-bar">
            @include('layouts.admin.right-sidebar')
        </div>
        <!-- /Right-bar -->

        {{-- log out form --}}
        <form id="_log_out_form" method="POST" action="{{ route('logout') }}">
            @csrf
        </form>

        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>

        <!-- Vendor js -->
        <script src="{{ asset('js/vendor.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('js/app.min.js') }}"></script>
        <script src="{{ asset('js/axios.min.js') }}"></script>
        <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-notify.min.js') }}"></script>
        <script src="{{ asset('js/jquery.masknumber.js') }}"></script>
        <script src="{{ asset('js/util.js') }}"></script>
        <script src="{{ asset('js/util.request.js') }}"></script>
        <script src="{{ asset('js/util.form.js') }}"></script>
        
        <script>
            $(document).ready(function() {
                // success
                @if (session('success'))
                    @if (is_array(session('success')))
                        Swal.fire(
                            '{{ session('success')['title'] }}',
                            '{{ session('success')['message'] }}',
                            'success',
                        )
                    @else
                        Swal.fire('Success', '{{ session('success') }}', 'success')
                    @endif
                @endif
                // warning
                @if (session('warning'))
                    @if (is_array(session('warning')))
                        Swal.fire(
                            '{{ session('warning')['title'] }}',
                            '{{ session('warning')['message'] }}',
                            'warning'
                        ),
                    @else
                        Swal.fire('Hey!', '{{ session('warning') }}', 'warning')
                    @endif
                @endif
                // error
                @if (session('error'))
                    @if (is_array(session('error')))
                        Swal.fire(
                            '{{ session('error')['title'] }}',
                            '{{ session('error')['message'] }}',
                            'error',
                        )
                    @else
                        Swal.fire('Oops!', '{{ session('error') }}', 'error')
                    @endif
                @endif
                // info
                @if (session('info'))
                    @if (is_array(session('info')))
                        Swal.fire(
                            '{{ session('info')['title'] }}',
                            '{{ session('info')['message'] }}',
                            'success',
                        )
                    @else
                        Swal.fire('Kindly Note!', '{{ session('info') }}', 'info')
                    @endif
                @endif

            });

        </script>

        @stack('js')
    </body>
</html>