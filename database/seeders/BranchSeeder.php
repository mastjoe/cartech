<?php

namespace Database\Seeders;

use App\Models\Branch;
use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $branches = [
            [
                'name'    => 'ONT1',
                'city'    => 'Onitsha',
                'state'   => 'Anambara',
                'country' => 'Nigeria',
                'address' => '200 upper iweka road, onitsha',
                'phone'   => '09000000000'
            ],
            [
                'name'    => 'ABJ1',
                'city'    => 'FCT Abuja',
                'country' => 'Nigeria',
                'address' => '20 maitama road, abuja',
                'phone'   => '09000000000'
            ]
        ];

        foreach ($branches as $branch) {
            Branch::create($branch);
        }
    }
}
