<?php

namespace Database\Seeders;

use App\Models\Privilege;
use Illuminate\Database\Seeder;

class PrivilegeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $privileges = [
            // staff
            [
                'name'     => 'view branch',
                'category' => 'branch',
            ],
            [
                'name'     => 'create branch',
                'category' => 'branch',
            ],
            [
                'name'     => 'update branch',
                'category' => 'branch'
            ],
            [
                'name'     => 'delete branch',
                'category' => 'branch',
            ],

            // staff
            [
                'name'     => 'view staff',
                'category' => 'staff',
            ],
            [
                'name'     => 'create staff',
                'category' => 'staff',
            ],
            [
                'name'     => 'update staff',
                'category' => 'staff'
            ],
            [
                'name'     => 'delete staff',
                'category' => 'staff',
            ],

            // customer
            [
                'name'     => 'view customer',
                'category' => 'customer',
            ],
            [
                'name'     => 'create customer',
                'category' => 'customer',
            ],
            [
                'name'     => 'update customer',
                'category' => 'customer',
            ],
            [
                'name'     => 'delete customer',
                'category' => 'customer',
            ],

            // vehicle
            [
                'name'     => 'view vehicle',
                'category' => 'vehicle',
            ],
            [
                'name'     => 'create vehicle',
                'category' => 'vehicle',
            ],
            [
                'name'     => 'update vehicle',
                'category' => 'vehicle',
            ],
            [
                'name'     => 'delete vehicle',
                'category' => 'vehicle',
            ],

            // job order
            [
                'name'     => 'view job order',
                'category' => 'job order',
            ],
            [
                'name'     => 'create job order',
                'category' => 'job order',
            ],
            [
                'name'     => 'update job order',
                'category' => 'job order',
            ],
            [
                'name'     => 'delete job order',
                'category' => 'job order',
            ],

            // supplier
            [
                'name'     => 'view supplier',
                'category' => 'supplier',
            ],
            [
                'name'     => 'create supplier',
                'category' => 'supplier',
            ],
            [
                'name'     => 'update supplier',
                'category' => 'supplier',
            ],
            [
                'name'     => 'delete supplier',
                'category' => 'supplier',
            ],

            // supply
            [
                'name'     => 'view supply',
                'category' => 'supply',
            ],
            [
                'name'     => 'create supply',
                'category' => 'supply',
            ],
            [
                'name'     => 'update supply',
                'category' => 'supply'
            ],
            [
                'name'     => 'delete supply',
                'category' => 'supply'
            ],
            [
                'name'     => 'manage supply account',
                'category' => 'supply'
            ],

            // services
            [
                'name'     => 'view service',
                'category' => 'service',
            ],
            [
                'name'     => 'create service',
                'category' => 'service',
            ],
            [
                'name'     => 'update service',
                'category' => 'service',
            ],
            [
                'name'     => 'delete service',
                'category' => 'service',
            ],

            // product
            [
                'name'     => 'view product',
                'category' => 'product',
            ],
            [
                'name'     => 'create product',
                'category' => 'product',
            ],
            [
                'name'     => 'update product',
                'category' => 'product',
            ],
            [
                'name'     => 'delete product',
                'category' => 'product',
            ],

            // view audit
            [
                'name'     => 'view audit',
                'category' => 'audit',
            ],
        ];

        foreach ($privileges as $privilege) {
            Privilege::firstOrCreate($privilege);
        }
    }
}
