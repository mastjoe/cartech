<?php

namespace Database\Seeders;

use App\Models\Privilege;
use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'superadmin',
            'accountant',
            'human resource',
            'technician',
        ];

        foreach ($roles as $role) {
            $created_role = Role::firstOrCreate(['name' => $role]);
            if ($created_role && $created_role->name == 'superadmin') {
                $created_role->privileges()->attach(Privilege::all()->pluck('id')->toArray());
            }
        }
    }
}
