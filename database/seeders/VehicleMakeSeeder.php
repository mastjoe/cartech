<?php

namespace Database\Seeders;

use App\Models\VehicleMake;
use Illuminate\Database\Seeder;

class VehicleMakeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $makes = [
            'Abarth',
            'Alfa Romeo',
            'Ashok Leyland',
            'Aston Martin',
            'Audi',
            'Bently',
            'BMW',
            'Bugatti',
            'Cadillac',
            'Caterham',
            'CHERY',
            'Chevrolet',
            'Chinkara',
            'Chrysler',
            'Citroen',
            'Corvette',
            'Dacia',
            'Daewoo',
            'Daihatsu',
            'DAIMLER',
            'Datsun',
            'Dodge',
            'Dodge/Commer',
            'FAW',
            'Ferrari',
            'Fiat',
            'Force Motors',
            'Ford',
            'Foton',
            'Freightliners',
            'GM',
            'Great Wall',
            'Gumpert',
            'GWM',
            'Hindustan Motors',
            'Honda',
            'Hummer',
            'Hyundai',
            'Infiniti',
            'Isuzu',
            'Jaguar',
            'Jeep',
            'Jensen',
            'JMC',
            'Kia',
            'Koenigsegg',
            'Lamborghini' .
            'Land Rover',
            'Lexus',
            'Lotus',
            'Mahindra',
            'Maruthi Suzuki',
            'Maserati',
            'Maybach',
            'Mazda',
            'McLaren',
            'Mercedes-Benz',
            'MG',
            'MG Motor UK',
            'MG-MGA',
            'MINI',
            'Mitsubishi',
            'Morgan',
            'Naza',
            'New Tyer',
            'Nissan',
            'Opel',
            'Perodua',
            'Peterbilt',
            'Peugeot',
            'Porsche',
            'Premier',
            'Proton',
            'Renault',
            'REVA',
            'Rolls-Royce',
            'Saab',
            'Seat',
            'Skoda',
            'Smart',
            'SsangYong',
            'Subaru',
            'Suzuki',
            'Tata',
            'TEST',
            'Toyota',
            'TVR',
            'Vauxhall',
            'Volkswagen',
            'Volvo',
            'Westfield',
            'Yamaha'
        ];

        foreach ($makes as $make) {
            VehicleMake::firstOrCreate(['name' => $make]);
        }
    }
}
