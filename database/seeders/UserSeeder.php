<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Branch;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin_email = 'admin@email.com';
        $users = [
            [
                'first_name'  => 'admin',
                'last_name'   => 'admin',
                'middle_name' => 'admin',
                'email'       => $superadmin_email,
                'phone'       => '09030000000',
                'gender'      => 'male',
                'password'    => 'password',
                'branch_id'   => Branch::first()->id,
            ]
        ];

        foreach ($users as $user) {
            $created_user = User::firstOrCreate($user);
            // assign branch
            $created_user->branches()->attach(Branch::first()->id);

            if ($created_user->email == $superadmin_email) {
                $created_user->assignRoles([Role::where('name', 'superadmin')->pluck('id')->first()]);
            }
        }
    }
}
