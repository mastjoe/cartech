<?php

namespace Database\Seeders;

use App\Helpers\Util;
use App\Models\VehicleFuel;
use Illuminate\Database\Seeder;

class VehicleFuelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $fuels = [
            'petrol',
            'diesel',
            'electric',
            'hybrid',
            'lpg',
            'cng',
            'other',
        ];

        $active_id = Util::getStatusId('active');

        foreach ($fuels as $fuel) {
            VehicleFuel::firstOrCreate(['name' => $fuel, 'status_id' => $active_id]);
        }
    }
}
