<?php

namespace Database\Seeders;

use App\Models\Privilege;
use App\Models\VehicleFuel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(StatusSeeder::class);
        $this->call(BranchSeeder::class);
        $this->call(PrivilegeSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(VehicleFuelSeeder::class);
        $this->call(VehicleMakeSeeder::class);
        $this->call(UserSeeder::class);
    }
}
