<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('branch_id')->nullable();
            $table->unsignedBigInteger('vehicle_make_id');
            $table->unsignedBigInteger('vehicle_fuel_id');
            $table->string('reference')->unique()->index();
            $table->string('model')->nullable();
            $table->string('model_year')->nullable();
            $table->string('engine_number')->nullable();
            $table->string('plate_number')->index()->unique();
            $table->string('gearbox_number')->nullable();
            $table->string('color')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();

            $table->timestamps();

            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('vehicle_make_id')->references('id')->on('vehicle_makes');
            $table->foreign('vehicle_fuel_id')->references('id')->on('vehicle_fuels');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
