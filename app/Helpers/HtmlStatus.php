<?php

namespace App\Helpers;

class HtmlStatus
{
    public static function jobOrder()
    {
        
    }

    public static function jobOrderService($statusName)
    {
        // possible job order service status can be 'pending', 'completed', 'part request',

        switch ($statusName) {
            case 'completed':
                $html = "<span class=\"badge badge-success\">Completed</span>";
                break;

            case 'parts request':
                $html = "<span class=\"badge badge-secondary\">Part Request</span>";
            
            default:
                $html = "<span class=\"badge badge-warning\">Pending</span>";
                break;
        }

        return $html;
    }

    public static function supplyStatus($statusName)
    {
        switch ($statusName) {
            case 'completed':
                $html = "<span class=\"badge badge-success\">Completed</span>";
                break;
            
            case 'incomplete':
                $html = "<span class=\"badge badge-warning\">Incomplete</span>";
                break;

            default:
                
                $html = "<span class=\"badge badge-secondary\">Processing</span>";
                break;
        }

        return $html;
    }
}