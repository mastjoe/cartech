<?php

namespace App\Helpers;

use App\Models\Role;
use App\Models\Status;
use App\Models\Privilege;

class Util {


    /**
     * get status id with status string
     *
     * @param string $status
     * @return void
     */
    public static function getStatusId(string $status)
    {
        $status = strtolower($status);
        return Status::where('name', $status)->pluck('id')->first();
    }


    /**
     * get status name with id
     *
     * @param integer $statusId
     * @return string|null
     */
    public static function getStatusName(int $statusId)
    {
        return Status::where('id', $statusId)->pluck('name')->first();
    }

    public static function flash($key, $value)
    {
        request()->session()->flash($key, $value);
    }

    public static function normalizeAmount($amount)
    {
        return floatval(str_replace(",", "", $amount));
    }

    /**
     * get unique privileges from role combinations in array
     *
     * @param array $roleIds
     * @return collection
     */
    public static function getUniquePrivileges(array $roleIds)
    {
        $privilegeIds = [];
        foreach ($roleIds as $roleId) {
            $role = Role::find($roleId);
            $roleIds = $role->privileges->pluck('id')->toArray();
            array_push($privilegeIds, ...$roleIds);
        }
        return Privilege::findMany(array_unique($privilegeIds));
    }

    public static function sortPrivilegesByCategory($privileges)
    {
        $privileges = collect($privileges);
        return $privileges->groupBy('category');
    }
}