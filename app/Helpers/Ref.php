<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

 class Ref 
 {
    /**
     * generates unique random strings
     *
     * @param string|null $table
     * @return string
     */
    private static function genRandRef($table = null)
    {
        $num  = microtime(true) . self::getTableNextId($table);
        $num  = str_replace(".", "", $num);
        $rand = rand(11, 99);
        return strtoupper(base_convert($rand . $num, 10, 36));
    }

    public static function getTableNextId($table)
    {
        $sql    = "SHOW TABLE STATUS LIKE '$table'";
        $result = DB::select($sql);
        return $result[0]->Auto_increment;
    }

    public static function genUserRef()
    {
        return static::getRefPrefix('users').static::genRandRef('users');
    }

    public static function genUserNokRef()
    {
        return static::getRefPrefix('user_noks').static::genRandRef('user_noks');
    }

    public static function genCustomerRef()
    {
        return static::getRefPrefix('customers').static::genRandRef('customers');
    }

    public static function genVehicleRef()
    {
        return static::getRefPrefix('vehicles').static::genRandRef('vehicles');
    }

    public static function genJobOrderRef()
    {
        return static::getRefPrefix('job_orders').static::genRandRef('job_orders');
    }

    public static function genSupplierRef()
    {
        return static::getRefPrefix('suppliers').static::genRandRef('suppliers');
    }

    public static function genSuppplyRef()
    {
        return static::getRefPrefix('supplies').static::genRandRef('supplies');
    }

    private static function getRefPrefix(string $table)
    {
        switch (strtolower($table)) {
            case 'users':
                $prefix = 'usr';
                break;
            case 'customers':
                $prefix = 'cst';
                break;
            case 'job_orders':
                $prefix = 'job';
                break;
            case 'user_noks':
                $prefix = "nok";
                break;
            case 'vehicles':
                $prefix = "vhc";
                break;
            case 'suppliers':
                $prefix = "sup";
                break;
            case 'supplies':
                $prefix = "spy";
                break;

            default:
               $prefix = null;
                break;
        }
        return strtoupper($prefix);
    }

    /**
     * provides fallback ref string
     *
     * @return string
     */
    public static function fallBackRef($length = 5)
    {
        if (function_exists('random_bytes')) {
            $byte = random_bytes($length);
        } else if (function_exists('openssl_random_pseudo_bytes')) {
            $byte = openssl_random_pseudo_bytes($length);
        } else {
            throw new \Exception('');
        }
        return strtoupper(bin2hex($byte));
    }
 }