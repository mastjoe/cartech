<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_privileges');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_privileges');
    }

    public function canDelete()
    {
        if ($this->users->count()) return false;
        return false;
    }
}
