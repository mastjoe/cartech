<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrder extends Model
{
    use HasFactory, Auditable;

    protected $guarded = [];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function vehicle()
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function manager()
    {
        return $this->belongsTo(User::class, 'manager_id');
    }

    public function inspector()
    {
        return $this->belongsTo(User::class, 'inspected_by');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function inspectionItems()
    {
        return $this->hasMany(JobOrderInspectionItem::class, 'job_order_id');
    }

    public function faultItems()
    {
        return $this->hasMany(JobOrderFaultItem::class);
    }

    public function canDelete()
    {
        return true;
    }

    public function services()
    {
        return $this->hasMany(JobOrderService::class)->with('service');
    }

    public function canEdit()
    {
        return true;
    }
}
