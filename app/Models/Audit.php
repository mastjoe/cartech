<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function setOldValuesAttribute($value) {
        $this->attributes['old_values'] = serialize($value);
    }

    public function setNewValuesAttribute($value)
    {
        $this->attributes['new_values'] = serialize($value);
    }

    public function getOlValuesAttribute()
    {
        return unserialize($this->old_values);
    }

    public function getNewValuesAttribute()
    {
        return unserialize($this->new_values);
    }
}
