<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany(User::class, 'branch_users');
    }

    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    public function canDelete()
    {
        return true;
    }

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }
}
