<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Privileged;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use HasFactory, Notifiable, InteractsWithMedia, Privileged, Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'dob',
        'email',
        'phone',
        'password',
        'email_verified_at',
        'created_by',
        'current_login',
        'last_login',
        'branch_id',
        'address',
        'suspended_at',
        'suspended_by',
    ];

    protected $appends = [
        'full_name',
        'full_names',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'current_login',
        'last_login',
        'dob',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    const genders = [
        'male'   => 'Male',
        'female' => 'Female',
    ];

    public function noks()
    {
        return $this->hasMany(UserNok::class);
    }

    public function privileges()
    { 
        return $this->belongsToMany(Privilege::class, 'user_privileges');
    }

    public function getFullNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getFullNamesAttribute()
    {
        return $this->first_name.' '.$this->middle_name.' '.$this->last_name;
    }

    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_users')
            ->withTimestamps();
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'created_by');
    }

    public function vehiclesRecorded()
    {
        return $this->hasMany(Vehicle::class, 'created_by');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }

    public function canDelete()
    {
        return true;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
        ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
        ->height(100)
            ->width(100);
    }

    public function getAvatarAttribute()
    {
        $file = $this->getMedia('avatar')->last();
        if ($file) {
            $file->url    = $file->getUrl();
            $file->thumb = $file->getUrl('thumb');
        }
        return $file;
    }

    public function fallbackAvatar()
    {
        return \asset('images/user.svg');
    }

    public function avatarThumbnail()
    {
        if ($this->avatar && $this->avatar->thumb) {
            return $this->avatar->thumb;
        }
        return $this->fallbackAvatar();
    }

    public function isSuspended()
    {
        if ($this->suspended_at) return true;
        return false;
    }
}
