<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Product extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    protected $guarded = [];

    public function productType()
    {
        return $this->belongsTo(ProductType::class);
    }

    public function supplyItems()
    {
        return $this->hasMany(SupplyItem::class);
    }

    public function canDelete()
    {
        if ($this->supplyItems->count()) return false;
        return true;
    }

    public function registerMediaConversions(?Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->height(100)
            ->width(100);

        // $this->addMediaConversion('');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('gallery');
    }
}
