<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrderService extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function jobOrder()
    {
        return $this->belongsTo(JobOrder::class);
    }

    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function managedBy()
    {
        return $this->belongsTo(User::class, 'managed_by');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function canDelete()
    {
        return true;
    }

    public function canEdit()
    {
        return true;
    }
}
