<?php

namespace App\Models;

use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory, Auditable;

    protected $guarded = [];


    public function supplies()
    {
        return $this->hasMany(Supply::class);
    }

    public function accountRecords()
    {
        return $this->hasMany(SupplyAccount::class);
    }

    public function canDelete()
    {
        return true;
    }
}
