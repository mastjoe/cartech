<?php

namespace App\Models;

use App\Traits\Auditable;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Customer extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, Auditable;

    protected $guarded = [];

    protected $appends = [
        'avatar',
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class);
    }

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function jobOrders()
    {
        return $this->hasMany(JobOrder::class);
    }

    public function canDelete()
    {
        if ($this->vehicles->isEmpty() && $this->jobOrders->isEmpty()) {
            return true;
        }
        return false;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
        ->singleFile();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->height(100)
            ->width(100);
    }

    public function getAvatarAttribute()
    {
        $file = $this->getMedia('avatar')->last();
        if ($file) {
            $file->url    = $file->getUrl();
            $file->thumb = $file->getUrl('thumb');
        }
        return $file;
    }

    public function fallbackAvatar()
    {
        return \asset('images/user.svg');
    }

    public function avatarThumbnail()
    {
        if ($this->avatar && $this->avatar->thumb) {
            return $this->avatar->thumb;
        }
        return $this->fallbackAvatar();
    }
}
