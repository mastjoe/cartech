<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VehicleFuel extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function vehicles()
    {
        return $this->hasMany(Vehicle::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function canDelete()
    {
        if ($this->vehicles->isEmpty()) return true;
        return false;
    }
}
