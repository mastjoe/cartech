<?php

namespace App\Models;

use App\Helpers\Util;
use App\Traits\Auditable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supply extends Model
{
    use HasFactory, Auditable;

    protected $guarded = [];

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function items()
    {
        return $this->hasMany(SupplyItem::class);
    }

    public function accountRecords()
    {
        return $this->hasMany(SupplyAccount::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function canDelete()
    {
        if ($this->items->count()) return false;
        return true;
    }

    public function canEdit()
    {
        return true;
    }

    public function canAddItems()
    {
        if (
            $this->status_id == Util::getStatusId('completed') ||
            $this->status_id == Util::getStatusId('incomplete')
        ) {
            return false;
        }
        return true;
    }

    public function canConfirm()
    {
        if ($this->items->count()&& !$this->accountRecords->count()) return true;
        return false;
    }

    public function lastAccountBalance()
    {
        return $this->accountRecords()->latest('id')->pluck('balance')->first();
    }
}
