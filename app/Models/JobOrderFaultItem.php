<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobOrderFaultItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function jobOrder()
    {
        return $this->belongsTo(JobOrder::class);
    }
}
