<?php

namespace App\Models;

use App\Helpers\Util;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SupplyItem extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function supply()
    {
        return $this->belongsTo(Supply::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function setSellingPriceAttribute($value)
    {
        $this->attributes['selling_price'] = Util::normalizeAmount($value);
    }

    public function setCostPriceAttribute($value)
    {
        $this->attributes['cost_price'] = Util::normalizeAmount($value);
    }

    public function canDelete()
    {
        return true;
    }

    public function canEdit()
    {
        
    }
}
