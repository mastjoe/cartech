<?php

namespace App\Policies;

use App\Models\JobOrder;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class JobOrderPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPrivilege('view job order');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\JobOrder  $jobOrder
     * @return mixed
     */
    public function view(User $user, JobOrder $jobOrder)
    {
        return $user->hasPrivilege('view job order');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPrivilege('create job order');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\JobOrder  $jobOrder
     * @return mixed
     */
    public function update(User $user, JobOrder $jobOrder)
    {
        return $user->hasPrivilege('update job order');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\JobOrder  $jobOrder
     * @return mixed
     */
    public function delete(User $user, JobOrder $jobOrder)
    {
        return $user->hasPrivilege('delete job order');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\JobOrder  $jobOrder
     * @return mixed
     */
    public function restore(User $user, JobOrder $jobOrder)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\JobOrder  $jobOrder
     * @return mixed
     */
    public function forceDelete(User $user, JobOrder $jobOrder)
    {
        //
    }
}
