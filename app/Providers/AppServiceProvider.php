<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();

        $this->customValidationRules();
    }

    public function customValidationRules()
    {
        // custom validation rule for does_not_exist
        Validator::extend('unexist', function($attribute, $value, $parameters, $validators) {
            $table = $parameters[0];
            $column = $parameters[1];
            
            return !(DB::table($table)->where($column, $value)->exists());
        });
    }
}
