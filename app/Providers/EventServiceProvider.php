<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Supply;
use App\Models\UserNok;
use App\Models\Vehicle;
use App\Models\Customer;
use App\Models\JobOrder;
use App\Models\Supplier;
use App\Models\SupplyAccount;
use App\Models\JobOrderService;
use App\Observers\UserObserver;
use App\Observers\SupplyObserver;
use App\Observers\UserNokObserver;
use App\Observers\VehicleObserver;
use App\Observers\CustomerObserver;
use App\Observers\JobOrderObserver;
use App\Observers\SupplierObserver;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use App\Observers\SupplyAccountObserver;
use App\Observers\JobOrderServiceObserver;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
        User::observe(UserObserver::class);
        UserNok::observe(UserNokObserver::class);
        Customer::observe(CustomerObserver::class);
        Vehicle::observe(VehicleObserver::class);
        Supplier::observe(SupplierObserver::class);
        Supply::observe(SupplyObserver::class);
        JobOrder::observe(JobOrderObserver::class);
        JobOrderService::observe(JobOrderServiceObserver::class);
        SupplyAccount::observe(SupplyAccountObserver::class);
    }
}
