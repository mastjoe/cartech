<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository
{
    /**
     * get users that are specific branch
     *
     * @param int $branchId
     * @return collection
     */
    public function getUsersInBranch($branchId)
    {
        return User::where('branch_id', $branchId)
            ->orderByDesc('id')
            ->get();
    }

    /**
     * get users that are not in specific branch
     *
     * @param int $branchId
     * @return collection
     */
    public function getUsersNotInBranch($branchId)
    {
        return User::where('branch_id', $branchId)
            ->orderByDesc('id')
            ->get();
    }

    /**
     * get users in current user's branch
     *
     * @return collection
     */
    public function getUsersInCurrentBranch()
    {
        $branchId = auth()->user()->branch_id;
        return $this->getUsersInBranch($branchId);
    }

    /**
     * get users that are not in current user's branch
     *
     * @return collection
     */
    public function getUsersNotInCurrentBranch()
    {
        $branchId = auth()->user()->branch_id;
        return $this->getUsersNotInBranch($branchId);
    }
}