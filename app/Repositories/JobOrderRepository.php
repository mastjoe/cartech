<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Models\JobOrder;
use Illuminate\Http\Request;

class JobOrderRepository
{
    public function activeBranch()
    {
        return auth()->user()->branch;
    }

    /**
     * get job orders for branch
     *
     * @return collection
     */
    public function getJobOrders()
    {
        return  $this->activeBranch()->jobOrders;
    }

    /**
     * create new job order record
     *
     * @param Request $request
     * @return App\Models\JobOrder $jobOrder
     */
    public function store(Request $request)
    {
        $request->validate([
            'vehicle_id'  => 'required',
        ], [], [
            'vehicle_id'  => 'vehicle',
        ]);

        $vehicle = Vehicle::findOrFail($request->vehicle_id);

        $jobOrder = $vehicle->jobOrders()->create([
            'customer_id' => $vehicle->customer_id,
        ]);

        return $jobOrder;
    }


    /**
     * update job order
     *
     * @param Request $request
     * @param JobOrder $jobOrder
     * @return boolean
     */
    public function update(Request $request, JobOrder $jobOrder)
    {
        $request->validate([
            'vehicle_id'  => 'required',
        ], [], [
            'vehicle_id'  => 'vehicle',
        ]);

        $vehicle = Vehicle::findOrFail($request->vehicle_id);

        return $jobOrder->update([
            'vehicle_id' => $vehicle->id,
            'customer_id' => $vehicle->customer_id,
        ]);
    }
}