<?php

namespace App\Repositories;

use App\Models\Vehicle;
use App\Models\Customer;
use Illuminate\Http\Request;

class VehicleRepository
{
    /**
     * get vehicle record by id
     *
     * @param int $id
     * @return Vehicle|null
     */
    public function findById($id)
    {
        return Vehicle::where('id', $id)->first();
    }

    /**
     * get vehicle record by reference
     *
     * @param string $ref
     * @return Vehicle|null
     */
    public function findByRef($ref)
    {
        return Vehicle::where('reference', $ref)->first();
    }

    /**
     * validates the storage request
     *
     * @param Request $request
     * @return void
     */
    protected function validateVehicleForStorage(Request $request)
    {
        $request->validate([
            'vehicle_make_id' => 'required',
            'vehicle_fuel_id' => 'required',
            'model'           => 'nullable|string|max:191',
            'model_year'      => 'nullable|digits:4',
            'engine_number'   => 'nullable|string|max:50',
            'plate_number'    => 'required|string|max:20|unique:vehicles,plate_number',
            'gearbox_number'  => 'nullable|string|max:50',
            'color'           => 'required',
            'description'     => 'nullable|string',
        ], [], [
            'vehicle_make_id' => 'vehicle_make'
        ]);
    }

    /**
     * stores new customer vehicle record
     *
     * @param Request $request
     * @param Customer $customer
     * 
     * @return Vehicle
     */
    public function store(Request $request, Customer $customer)
    {
        $this->validateVehicleForStorage($request);

        $vehicle = $customer->vehicles()->create($request->all());

        return $vehicle;
    }

    protected function validateVehicleForUpdate(Request $request)
    {
        $request->validate([
            'vehicle_make_id' => 'required',
            'vehicle_fuel_id' => 'required',
            'model'           => 'nullable|string|max:191',
            'model_year'      => 'nullable|digits:4',
            'engine_number'   => 'nullable|string|max:50',
            'plate_number'    => 'required|string|max:20|unique:vehicles,plate_number,'.$request->plate_number.',plate_number',
            'gearbox_number'  => 'nullable|string|max:50',
            'color'           => 'required',
            'description'     => 'nullable|string',
        ], [], [
            'vehicle_make_id' => 'vehicle_make',
            'vehicle_fuel_id' => 'vehicle_id',
        ]);
    }

    /**
     * update a vehicle record
     *
     * @param Request $request
     * @param Vehicle $vehicle
     * 
     * @return boolean
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        $this->validateVehicleForUpdate($request);

        return $vehicle->update($request->all());
    }
}