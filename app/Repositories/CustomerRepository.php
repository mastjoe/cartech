<?php

namespace App\Repositories;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerRepository
{
    /**
     * get customer record by id
     *
     * @param int $id
     * @return Customer|null
     */
    public function findById($id)
    {
        return Customer::where('id', $id)->first();
    }

    /**
     * get customer record by reference
     *
     * @param string $ref
     * @return Customer|null
     */
    public function findByRef($ref)
    {
        return Customer::where('reference', $ref)->first();
    }

    /**
     * create a new customer's record
     *
     * @param Request $request
     * @return Customer 
     */
    public function create(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|max:191',
            'phone'   => 'required|string|max:20',
            'email'   => 'nullable|email|string|max:100',
            'address' => 'nullable|string',
            'image'   => 'nullable|image|mimes:jpeg,jpg,gif,png|max:250'
        ]);

        $customer = Customer::create($request->only('name', 'phone', 'email', 'address'));
        $this->handleImageUpload($request, $customer);

        return $customer;
    }

    public function update(Request $request, Customer $customer)
    {
        $request->validate([
            'name'    => 'required|string|max:191',
            'phone'   => 'required|string|max:20',
            'email'   => 'nullable|email|string|max:100',
            'address' => 'nullable|string',
            'image'   => 'nullable|image|mimes:jpeg,jpg,gif,png|max:250'
        ]);

        $this->handleImageUpload($request, $customer);

        return $customer->update($request->only('name', 'phone', 'email', 'address'));
    }

    protected function handleImageUpload(Request $request, Customer $customer)
    {
        if ($request->hasFile('image')) {
            $customer->addMedia($request->image)->toMediaCollection('avatar');
        }
    }

    public function getAllCustomers()
    {
        return Customer::orderByDesc('id')->get();
    }

    public function getAllBranchCustomers($branch_id)
    {
        return Customer::where('branch_id', $branch_id)
            ->orderByDesc('id')
            ->get();
    }
}