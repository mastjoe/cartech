<?php

namespace App\Observers;

use App\Helpers\Util;
use App\Models\JobOrderService;

class JobOrderServiceObserver
{
    public function creating(JobOrderService $jobOrderService)
    {
        $jobOrderService->status_id = Util::getStatusId('pending');
    }


    /**
     * Handle the JobOrderService "created" event.
     *
     * @param  \App\Models\JobOrderService  $jobOrderService
     * @return void
     */
    public function created(JobOrderService $jobOrderService)
    {
        //
    }

    /**
     * Handle the JobOrderService "updated" event.
     *
     * @param  \App\Models\JobOrderService  $jobOrderService
     * @return void
     */
    public function updated(JobOrderService $jobOrderService)
    {
        //
    }

    /**
     * Handle the JobOrderService "deleted" event.
     *
     * @param  \App\Models\JobOrderService  $jobOrderService
     * @return void
     */
    public function deleted(JobOrderService $jobOrderService)
    {
        //
    }

    /**
     * Handle the JobOrderService "restored" event.
     *
     * @param  \App\Models\JobOrderService  $jobOrderService
     * @return void
     */
    public function restored(JobOrderService $jobOrderService)
    {
        //
    }

    /**
     * Handle the JobOrderService "force deleted" event.
     *
     * @param  \App\Models\JobOrderService  $jobOrderService
     * @return void
     */
    public function forceDeleted(JobOrderService $jobOrderService)
    {
        //
    }
}
