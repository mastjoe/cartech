<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Models\Vehicle;

class VehicleObserver
{
    public function creating(Vehicle $vehicle)
    {
        $vehicle->created_by = auth()->user()->id;
        $vehicle->branch_id  = auth()->user()->branch_id;
        $vehicle->reference  = Ref::genVehicleRef();
    }

    /**
     * Handle the Vehicle "created" event.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return void
     */
    public function created(Vehicle $vehicle)
    {
        $vehicle->logAudit([
            'event'       => 'create',
            'description' => 'added new vehicle with plate number, '.$vehicle->plate_number,
        ]);
    }

    /**
     * Handle the Vehicle "updated" event.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return void
     */
    public function updated(Vehicle $vehicle)
    {
        $vehicle->logAudit([
            'event'       => 'update',
            'description' => 'vehicle was updated',
        ]);
    }

    /**
     * Handle the Vehicle "deleted" event.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return void
     */
    public function deleted(Vehicle $vehicle)
    {
        $vehicle->logAudit([
            'event'       => 'delete',
            'description' => 'deleted vehicle with plate number, '.$vehicle->plate_number,
        ]);
    }

    /**
     * Handle the Vehicle "restored" event.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return void
     */
    public function restored(Vehicle $vehicle)
    {
        //
    }

    /**
     * Handle the Vehicle "force deleted" event.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return void
     */
    public function forceDeleted(Vehicle $vehicle)
    {
        //
    }
}
