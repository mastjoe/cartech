<?php

namespace App\Observers;

use App\Models\Supply;
use App\Models\SupplyAccount;

class SupplyAccountObserver
{
    /**
     * Handle the SupplyAccount "creating" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function creating(SupplyAccount $supplyAccount)
    {
        $user = auth()->user();
        $supply = Supply::find($supplyAccount->supply_id);

        if ($supply->accountRecords->isEmpty()) {
            $supplyAccount->balance = $supplyAccount->amount;
        }

        $supplyAccount->user_id     = $user->id;
        $supplyAccount->branch_id   = $user->branch_id;
        $supplyAccount->supplier_id = $supply->supplier_id;
    }

    /**
     * Handle the SupplyAccount "created" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function created(SupplyAccount $supplyAccount)
    {
        //
    }

    /**
     * Handle the SupplyAccount "updated" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function updated(SupplyAccount $supplyAccount)
    {
        //
    }

    /**
     * Handle the SupplyAccount "deleted" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function deleted(SupplyAccount $supplyAccount)
    {
        //
    }

    /**
     * Handle the SupplyAccount "restored" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function restored(SupplyAccount $supplyAccount)
    {
        //
    }

    /**
     * Handle the SupplyAccount "force deleted" event.
     *
     * @param  \App\Models\SupplyAccount  $supplyAccount
     * @return void
     */
    public function forceDeleted(SupplyAccount $supplyAccount)
    {
        //
    }
}
