<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Helpers\Util;
use App\Models\Supply;

class SupplyObserver
{
    /**
     * Handle the Supply "creating" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function creating(Supply $supply)
    {
        $supply->reference = Ref::genSuppplyRef();
        $supply->branch_id = auth()->user()->branch_id;
        $supply->status_id = Util::getStatusId('processing');
    }


    /**
     * Handle the Supply "created" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function created(Supply $supply)
    {
        $supply->logAudit([
            'event' => 'create',
            'description' => 'supply with ref, '.$supply->reference.' was created',
        ]);
    }

    /**
     * Handle the Supply "updated" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function updated(Supply $supply)
    {
        $supply->logAudit([
            'event' => 'update',
            'decription' => 'supply was updated',
        ]);
    }

    /**
     * Handle the Supply "deleted" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function deleted(Supply $supply)
    {
        $supply->logAudit([
            'event' => 'delete',
            'description' => 'supply with ref,'.$supply->reference.' for supplier, '.$supply->supplier->name.' was deleted',
        ]);
    }

    /**
     * Handle the Supply "restored" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function restored(Supply $supply)
    {
        //
    }

    /**
     * Handle the Supply "force deleted" event.
     *
     * @param  \App\Models\Supply  $supply
     * @return void
     */
    public function forceDeleted(Supply $supply)
    {
        //
    }
}
