<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Models\UserNok;

class UserNokObserver
{

    public function creating(UserNok $userNok)
    {
        $userNok->reference = Ref::genUserNokRef();
    }

    /**
     * Handle the UserNok "created" event.
     *
     * @param  \App\Models\UserNok  $userNok
     * @return void
     */
    public function created(UserNok $userNok)
    {
        $userNok->logAudit([
            'event'       => 'create',
            'description' => 'Next of Kin record, '.$userNok->full_name.' was added for '.$userNok->user->full_name,
        ]);
    }

    /**
     * Handle the UserNok "updated" event.
     *
     * @param  \App\Models\UserNok  $userNok
     * @return void
     */
    public function updated(UserNok $userNok)
    {
        $userNok->logAudit([
            'event' => 'update',
            'description' => 'Next of Kin, '.$userNok->full_name.' updated',
        ]);
    }

    /**
     * Handle the UserNok "deleted" event.
     *
     * @param  \App\Models\UserNok  $userNok
     * @return void
     */
    public function deleted(UserNok $userNok)
    {
        $userNok->logAudit([
            'event' => 'delete',
            'description' => 'Next of Kin, '.$userNok->full_name.' for '.$userNok->user->full_name.' was deleted',
        ]);
    }

    /**
     * Handle the UserNok "restored" event.
     *
     * @param  \App\Models\UserNok  $userNok
     * @return void
     */
    public function restored(UserNok $userNok)
    {
        //
    }

    /**
     * Handle the UserNok "force deleted" event.
     *
     * @param  \App\Models\UserNok  $userNok
     * @return void
     */
    public function forceDeleted(UserNok $userNok)
    {
        //
    }
}
