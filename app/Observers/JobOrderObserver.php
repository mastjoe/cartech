<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Helpers\Util;
use App\Models\JobOrder;

class JobOrderObserver
{
     /**
     * Handle the JobOrder "creating" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function creating(JobOrder $jobOrder)
    {
        $jobOrder->reference  = Ref::genJobOrderRef();
        $jobOrder->created_by = auth()->user()->id;
        $jobOrder->status_id  = Util::getStatusId('active');
        $jobOrder->branch_id  = auth()->user()->branch_id;
    }

    /**
     * Handle the JobOrder "created" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function created(JobOrder $jobOrder)
    {
        // log
        $jobOrder->logAudit([
            'event'       => 'create',
            'description' => 'job order, '.$jobOrder->reference.' was created',
        ]);
    }

    /**
     * Handle the JobOrder "updated" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function updated(JobOrder $jobOrder)
    {
        // when manager is assigned
        if ($jobOrder->isDirty('manager_id')) {
            $jobOrder->logAudit([
                'event' => 'update',
                'description' => 'job order was assigned to manager, '.$jobOrder->manager->full_name,
            ]);
            return false;
        }
        
        $jobOrder->logAudit([
            'event' => 'update',
            'description' => 'job order was updated',
        ]);
    }

    /**
     * Handle the JobOrder "deleted" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function deleted(JobOrder $jobOrder)
    {
        // log
        $jobOrder->logAudit([
            'event' => 'delete',
            'description' => 'job order, '.$jobOrder->reference.' was deleted',
        ]);
    }

    /**
     * Handle the JobOrder "restored" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function restored(JobOrder $jobOrder)
    {
        //
    }

    /**
     * Handle the JobOrder "force deleted" event.
     *
     * @param  \App\Models\JobOrder  $jobOrder
     * @return void
     */
    public function forceDeleted(JobOrder $jobOrder)
    {
        //
    }
}
