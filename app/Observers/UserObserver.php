<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Models\User;
use App\Helpers\Util;

class UserObserver
{
    public function creating(User $user)
    {
        $user->password  = \bcrypt($user->password);
        $user->status_id = Util::getStatusId('active');
        $user->reference = Ref::genUserRef();
    }

    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        $user->logAudit([
            'event'       => 'create',
            'description' => 'user account with name, '.$user->full_names.' was created',
        ]);
    }

    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $user->logAudit([
            'event'       => 'update',
            'description' => 'user account was updated',
        ]);
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $user->logAudit([
            'event'       => 'delete',
            'description' => 'user with name, '.$user->full_names.' was deleted',
        ]);
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }
}
