<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Models\Customer;

class CustomerObserver
{
    public function creating(Customer $customer)
    {
        $customer->reference  = Ref::genCustomerRef();
        $customer->branch_id  = auth()->user()->branch_id;
        $customer->created_by = auth()->user()->id;
    }


    /**
     * Handle the Customer "created" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function created(Customer $customer)
    {
        // log audit
        $customer->logAudit([
            'event'       => 'create',
            'description' => 'created new customer, '.$customer->name,
        ]);
    }

    /**
     * Handle the Customer "updated" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function updated(Customer $customer)
    {
        $customer->logAudit([
            'event'       => 'update',
            'description' => 'customer details was updated',
        ]);
    }

    /**
     * Handle the Customer "deleted" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function deleted(Customer $customer)
    {
        $customer->logAudit([
            'event'       => 'delete',
            'description' => 'customer account was deleted',
        ]);
    }

    /**
     * Handle the Customer "restored" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function restored(Customer $customer)
    {
        //
    }

    /**
     * Handle the Customer "force deleted" event.
     *
     * @param  \App\Models\Customer  $customer
     * @return void
     */
    public function forceDeleted(Customer $customer)
    {
        //
    }
}
