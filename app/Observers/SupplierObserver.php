<?php

namespace App\Observers;

use App\Helpers\Ref;
use App\Models\Supplier;

class SupplierObserver
{
    public function creating(Supplier $supplier)
    {
        $supplier->reference = Ref::genSupplierRef();
    }


    /**
     * Handle the Supplier "created" event.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return void
     */
    public function created(Supplier $supplier)
    {
        $supplier->logAudit([
            'event'       => 'create',
            'description' => 'supplier with name, '.$supplier->name.' was created',
        ]);
    }

    /**
     * Handle the Supplier "updated" event.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return void
     */
    public function updated(Supplier $supplier)
    {
        $supplier->logAudit([
            'event'       => 'update',
            'description' => 'supplier details was updated',
        ]);
    }

    /**
     * Handle the Supplier "deleted" event.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return void
     */
    public function deleted(Supplier $supplier)
    {
        $supplier->logAudit([
            'event'       => 'delete',
            'description' => 'supplier, '.$supplier->name.' was deleted',
        ]);
    }

    /**
     * Handle the Supplier "restored" event.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return void
     */
    public function restored(Supplier $supplier)
    {
        //
    }

    /**
     * Handle the Supplier "force deleted" event.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return void
     */
    public function forceDeleted(Supplier $supplier)
    {
        //
    }
}
