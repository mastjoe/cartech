<?php

namespace App\Traits;

Trait BranchHandler
{
    public function currentBranch()
    {
        return auth()->user()->branch;
    }
}