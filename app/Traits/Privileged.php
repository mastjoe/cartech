<?php

namespace App\Traits;

use App\Helpers\Util;
use App\Models\RolePrivilege;
use App\Models\UserPrivilege;

trait Privileged
{
    /**
     * gets all privileges id in array
     *
     * @return array
     */
    protected function privilegeIdsArray()
    {
        return $this->privileges->pluck('id')->toArray();
    }

    protected function privilegeNamesArray()
    {
        return $this->privileges->pluck('name')->toArray();
    }

    public function hasPrivilege($privilege)
    {
        if (is_int($privilege)) {
            return in_array($privilege, $this->privilegeIdsArray());
        }
        return in_array(strtolower($privilege), $this->privilegeNamesArray());
    }

    /**
     * checks if model has all privileges in array
     *
     * @param array $privileges
     * @return boolean
     */
    public function hasAllPrivileges(array $privileges)
    {
        foreach ($privileges as $privilege) {
            if (!$this->hasPrivilege($privilege)) {
                return false;
            }
        }
        return true;
    }

    /**
     * checks if model has any privilege in array
     *
     * @param array $privileges
     * @return boolean
     */
    public function hasAnyPrivilege(array $privileges)
    {
        foreach ($privileges as $privilege) {
            if ($this->hasPrivilege($privilege)) {
                return true;
            }
        }
        return false;
    }

    /**
     * get all role ids in array
     *
     * @return array
     */
    protected function roleIdsArray() :array
    {
        return $this->roles->pluck('id')->toArray();
    }

    /**
     * get all role names in array
     *
     * @return array
     */
    public function roleNamesArray() :array
    {
        return $this->roles->pluck('name')->toArray();
    }

    /**
     * checks if model has a role
     *
     * @param int|string $role
     * @return boolean
     */
    public function hasRole($role)
    {
        if (is_int($role)) {
            return in_array($role, $this->roleIdsArray());
        }
        return in_array($role, $this->roleNamesArray());
    }

    /**
     * check if model has all roles
     *
     * @param array $roles
     * @return boolean
     */
    public function hasAllRoles(array $roles)
    {
        foreach ($roles as $role) {
            if (!$this->hasRole($role)) return false;
        }
        return true;
    }


    /**
     * checks if model has any of the role
     *
     * @param array $roles
     * @return boolean
     */
    public function hasAnyRole(array $roles)
    {
        foreach ($roles as $role) {
            if ($this->hasRole($role)) return true;
        }
        return false;
    }



    /**
     * assigns to model role ids
     *
     * @param array $roleIds
     * @return void
     */
    public function assignRoles(array $roleIds)
    {
        $this->roles()->attach($roleIds);
        // sync with privileges
        $this->assignPrivileges($roleIds);
    }

    public function updateRoles(array $roleIds)
    {
        // sync roles
        $this->roles()->sync($roleIds);
        // get unique privileges id
        $privilegeIds = Util::getUniquePrivileges($roleIds)->pluck('id')->toArray();
        $this->privileges()->sync($privilegeIds);
    }

    /**
     * assign privileges to user based on role
     *
     * @param array $roleIds
     * @return void
     */
    public function assignPrivileges($roleIds = [])
    {
        $privilegeIds = [];

        if (!count($roleIds)) {
            $roleIds = $this->roleIdsArray();
        }

        foreach ($roleIds as $roleId) {
            $ids = RolePrivilege::where('role_id', $roleId)->pluck('privilege_id')->toArray();
            array_push($privilegeIds, ...$ids);
        }

        $privilegeIds = array_unique($privilegeIds);

        foreach ($privilegeIds as $privilegeId) {
            $exist = UserPrivilege::where('user_id', $this->id)
                ->where('privilege_id', $privilegeId)
                ->exists();

            // exit if created
            if ($exist) return false;
            // create if does not exist
            UserPrivilege::create([
                'user_id' => $this->id,
                'privilege_id' => $privilegeId,
            ]);
        }
    }
}