<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

trait Auditable
{
    /**
     * gets the old/new value of create event
     * 
     * @return array
     */
    protected function getCreateEventAttributes(): array
    {
        $new = [];
        foreach ($this->attributes as $attribute => $value) {
            $new[$attribute] = $value;
        }

        return [[], $new];
    }

    protected function getUpdateEventAttributes(): array
    {
        $new = [];
        $old = [];
    
        foreach ($this->getDirty() as $attribute => $value) {
            $new[$attribute] = $this->attributes[$attribute];
            $old[$attribute] = $this->original[$attribute];
        }

        return [$new, $old];
    }

    protected function getDeleteEventAttributes(): array
    {
        $old = [];
        foreach ($this->original as $attribute => $value) {
            $old[$attribute] = $value;
        }
        return [$old, []];
    }

    protected function getRestoreEventAttributes(): array
    {
        return array_reverse($this->getDeleteEventAttributes());
    }

    protected function getReadEventAttributes(): array
    {
        $old = [];
        foreach ($this->original as $attribute => $value) {
            $old[$attribute] = $this->original[$attribute];
            $old[$attribute] = $value;
        }

        return [$old, $old];
    }

    protected function resolveEventAttributes($event)
    {
        switch ($event) {
            case 'create':
                return $this->getCreateEventAttributes();
                break;

            case 'update':
                return $this->getUpdateEventAttributes();
                break;

            case 'delete':
                return $this->getDeleteEventAttributes();
                break;

            case 'restore':
                return $this->getRestoreEventAttributes();
                break;

            default:
                return $this->getReadEventAttributes();
                break;
        }
    }

    /**
     * prepares the overall audit data
     * 
     * @return array
     */
    protected function toAudit(array $data)
    {
        list($old, $new) = $this->resolveEventAttributes($data['event']);

        return [
            'user_id'     => Auth::user()->id ?? null,
            'ip_address'  => Request::ip(),
            'http_method' => Request::method(),
            'url'         => Request::url(),
            'user_agent'  => Request::userAgent(),
            'old_values'  => $old,
            'new_values'  => $new,
            'description' => $data['description'] ?? null,
            'event'       => $data['event'],
            'branch_id'   => $this->getCurrentBranchId(),
        ];
    }

    protected function getCurrentBranchId()
    {
        if (auth()->user()) {
            return auth()->user()->branch_id;
        }
        return null;
    }

    public function audits()
    {
        return $this->morphMany('App\Models\Audit', 'auditable');
    }

    /**
     * log audit record for model
     * 
     * @return \App\Models\Audit
     */
    public function logAudit(array $data)
    {
        return $this->audits()->create($this->toAudit($data));
    }

}
