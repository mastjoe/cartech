<?php

namespace App\Http\Controllers\Admin;

use Exception;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use App\Helpers\Util;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $per_page = 9;
        $search = strtolower($request->search) ?? null;

        if ($search) {
            $users = User::where('first_name', 'LIKE', $search.'%')
                ->orWhere('last_name', 'LIKE', $search,'%')
                ->orWhere('middle_name', 'LIKE', $search.'%')
                ->orWhere('email', 'LIKE', $search.'%')
                ->orWhere('phone', 'LIKE', $search.'%')
                ->paginate($per_page);
        } else {
            $users = User::orderByDesc('id')->paginate($per_page);
        }
        
        return view('admin.staff.index')
            ->with('users', $users)
            ->with('search', $search);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::has('privileges')->get();
        $genders = User::genders;
        $branches = Branch::all();

        return view('admin.staff.create')
            ->with('roles', $roles)
            ->with('genders', $genders)
            ->with('branches', $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name'    => 'required|string|max:191',
            'middle_name'   => 'nullable|string|max:191',
            'last_name'     => 'required|string|max:191',
            'email'         => 'required|string|max:191|email|unique:users,email',
            'phone'         => 'required|string|digits_between:7,15',
            'gender'        => 'required|string',
            'dob'           => 'nullable|date',
            'address'       => 'nullable|string',
            'roles'         => 'required',
            'image'         => 'nullable|image|mimes:jpg,png,gif,jpeg|max:500',
        ], [], [
            'dob' => 'date of birth',
        ]);

        $request->request->add(['branch_id' => $this->currentBranch()->id ]);
        
        try {
            //code...
            $user = User::create($request->all());
            
            // assign role/privileges
            $user->assignRoles($request->roles);
            $this->handleImageUpload($request, $user);

            // assign branch
            $user->branches()->attach($this->currentBranch()->id);

            $msg = [
                'title'    => 'Staff Account Created',
                'message' => 'staff account for , '.$user->full_name.'  was successfully created',
            ];

            return redirect()->route('admin.staff.show', $user->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            //log error
            Log::error($ex->getMessage());

            $msg = [
                'title'   => 'Account Creation Failed',
                'message' => 'account was not created! Try again!'
            ];

            return redirect()->back()
                ->with('error', $msg)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $branches  = Branch::all();

        return view('admin.staff.show')
            ->with('user', $user)
            ->with('branches', $branches);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with(['roles'])->findOrFail($id);
        $genders = User::genders;

        return view('admin.staff.edit')
            ->with('user', $user)
            ->with('genders', $genders);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name'  => 'required|string|max:191',
            'middle_name' => 'nullable|string|max:191',
            'last_name'   => 'required|string|max:191',
            'email'       => 'required|string|max:191|email|unique:users,email,'.$request->email.',email',
            'phone'       => 'required|string|digits_between:7,15',
            'gender'      => 'required|string',
            'dob'         => 'nullable|date',
            'address'     => 'nullable|string',
            'image'       => 'nullable|image|mimes:jpg,jpeg,png,gif|max:500',
        ], [], [
            'dob' => 'date of birth',
        ]);

        $user = User::findOrFail($id);

        try {
            
        $user->update($request->all());
            $this->handleImageUpload($request, $user);

            $msg = [
                'title'   => 'Staff Account Updated',
                'message' => 'staff account was successfully updated'
            ];

            return redirect()->route('admin.staff.show', $id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            //log error
            Log::error($ex->getMessage());

            $msg = [
                'title'   => 'Update Failed',
                'message' => 'staff update failed! Try again'
            ];

            return redirect()->back()
                ->withInput()
                ->with('error', $msg);
        }
    }

    /**
     * handles image upload for user
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    protected function handleImageUpload(Request $request, User $user)
    {
        if ($request->hasFile('image')) {
            $user->addMedia($request->image)->toMediaCollection('avatar');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        if ($user->canDelete()) {
            $user->roles()->detach();
            $user->privileges()->detach();
            $user->delete();

            $msg = [
                'title'   => 'Staff Account Deleted',
                'message' => 'staff account was successfully deleted'
            ];

            request()->session()->flash('success', $msg);

            return response()->json([
                'success'  => true,
                'redirect' => route('admin.staff.index'),
                'message'  => $msg['message']
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Operation failed! Try again'
        ]);
    }

    /**
     * suspends staff account
     *
     * @param User $staff
     * @return \Illuminate\Http\Response
     */
    public function suspend(User $staff)
    {
        if ($staff->isSuspended()) {
            return response()->json([
                'success' => false,
                'message' => 'staff already suspended'
            ], 400);
        }

        $staff->update([
            'suspended_at' => Carbon::now(),
            'suspended_by' => auth()->user()->id,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'staff, '.$staff->full_name.' was successfully suspended'
        ]);
    }

    /**
     * unsuspends staff account
     *
     * @param User $staff
     * @return \Illuminate\Http\Response
     */
    public function unsuspend(User $staff)
    {
        if (!$staff->suspended_at) {
            return response()->json([
                'success' => false,
                'message' => 'staff was never suspended',
            ], 400);
        }

        $staff->update([
            'suspended_at' => null,
            'suspended_by' => null,
        ]);

        return response()->json([
            'success' => true,
            'message' => 'staff, '.$staff->full_name.' was successfully unsuspended',
        ]);
    }

    public function updateBranches(Request $request, User $staff)
    {
        $request->validate([
            'branches' => 'required',
        ]);

        try {
            $staff->branches()->sync($request->branches);

            $msg = [
                'title' => 'Staff Branches Updated',
                'message' => 'staff branches was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Ops! branch assignment failed! Try again',
            ]);
        }
    }
}
