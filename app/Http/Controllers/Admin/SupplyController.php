<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Supply;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class SupplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplies = Supply::latest()->get();

        return view('admin.supplies.index')
            ->with('supplies', $supplies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $suppliers = Supplier::all();

        return view('admin.supplies.create')
            ->with('suppliers', $suppliers);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'supplier' => 'required',
            'note'     => 'nullable|string',
        ]);

        try {
            $supplier = Supplier::findOrFail($request->supplier);

            $supply = $supplier->supplies()->create($request->only('note'));

            $msg = [
                'title' => 'Supply Created',
                'message' => 'supply was successfully created'
            ];

            return redirect()->route('admin.supplies.show', $supply->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error', 'Something went wrong! Try again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supply = Supply::with(['supplier'])->findOrFail($id);
        $products = Product::all();

        return view('admin.supplies.show')
            ->with('supply', $supply)
            ->with('products', $products);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supply = Supply::with('supplier')->findOrFail($id);
        $suppliers = Supplier::all();

        return view('admin.supplies.edit')
            ->with('supply', $supply)
            ->with('suppliers', $suppliers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'supplier' => 'required',
            'note'     => 'nullable|string',
        ]);

        try {
            $supply = Supply::findOrFail($id);
            
            $request->request->add(['supplier_id' => $id]);

            $supply->update($request->only('note', 'supplier_id'));

            $msg = [
                'title' => 'Supply Updated',
                'message' => 'supply was successfully updated',
            ];

            return redirect()->route('admin.supplies.show', $supply->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error', 'Oops! something went wrong! Try again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supply = Supply::findOrFail($id);

        if ($supply->canDelete()) {
            $supply->delete();

            $msg = [
                'title' => 'Supply Deleted',
                'message' => 'supply was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => 'supply was successfully deleted',
                'redirect' => route('admin.supplies.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'supply was not deleted! Try again'
        ]);

    }

    /**
     * confirms supplies session
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function confirmSupply($id)
    {
        $supply = Supply::find($id);

        if ($supply->items->isEmpty()) {
            return response()->json([
                'success' => false,
                'message' => 'cannot confirm supply as no item was found'
            ], 400);
        }

        // get total cost sum for items in supply
        $totalCost = 0;

        foreach ($supply->items as $item) {
            $totalCost += $item->quantity * $item->cost_price;
        }

        // get previous balance
        $balance = $supply->lastAccountBalance();

        // create a debit credit record for supplier
        $supply->accountRecords()->create([
            'amount'  => $totalCost,
            'credit'  => true,
            'balance' => $balance ?? 0,
        ]);

        $msg = [
            'title'   => 'Supply Confirmed',
            'message' => 'supply, '.$supply->reference.' was successfully confirmed',
        ];

        Util::flash('success', $msg);

        return response()->json([
            'success'  => true,
            'message'  => 'supply successfully confirmed',
            'redirect' => route('admin.supplies.show', $id),
        ]);
    }
}
