<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches  = Branch::all();

        return view('admin.branches.index')
            ->with('branches', $branches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.branches.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'       => 'required|string|max:191|unique:branches,name',
            'city'       => 'required|string|max:191',
            'state'      => 'required|string|max:191',
            'country'    => 'required|string|max:191',
            'address'    => 'required|string',
            'phone'      => 'required|string',
            'email'      => 'required|string|email|max:191',
            'other_info' => 'nullable|string'
        ]);

        try {
            
            $branch = Branch::create($request->all());
            $msg = [
                'title' => 'Branch Created',
                'message' => 'branch '.$branch->name.' was successfully created',
            ];

            return redirect()->route('admin.branch.show', $branch->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            $msg = [
                'title' => 'Operation Failed',
                'message' => 'branch was not created! Try again'
            ];

            return redirect()->back()
                ->withInput()
                ->with('error', $msg);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $branch = Branch::findOrFail($id);

        return view('admin.branches.show')
            ->with('branch', $branch);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);

        return view('admin.branches.edit')
            ->with('branch', $branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'       => 'required|string|max:191|unique:branches,name,'.$request->name.',name',
            'city'       => 'required|string|max:191',
            'state'      => 'required|string|max:191',
            'country'    => 'required|string|max:191',
            'address'    => 'required|string',
            'phone'      => 'required|string',
            'email'      => 'required|string|email|max:191',
            'other_info' => 'required|string'
        ]);

        try {

            $branch = Branch::findOrFail($id);
            $branch->update($request->all());

            $msg = ['title' => 'Branch Updated', 'message' => 'branch was successfully created'];

            return redirect()->route('admin.branch.show', $id)->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            $msg = ['title' => 'Operation Failed', 'message' => 'branch was not updated! Try again'];

            return redirect()->back()->withInput()->with('error', $msg);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $branch = Branch::findOrFail($id);

        if ($branch->canDelete()) {
            $branch->delete();
            $msg = ['title' => 'Branch Deleted', 'message' => 'branch was successfully deleted'];
            Util::flash('success', $msg);

            return response()->json([
                'success'=> true,
                'message' => $msg['message'],
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'delete operation failed! Try again later'
        ]);
    }


    /**
     * show branch customers
     *
     * @param int $branch_id
     * @return \Illuminate\Http\Response
     */
    public function customers($branch_id)
    {
        $branch = Branch::with(['customers'])->findOrFail($branch_id);

        return view('admin.branches.customers')
            ->with('branch', $branch);
    }

    /**
     * show branch staff
     *
     * @param int $branch_id
     * @return \Illuminate\Http\Response
     */
    public function staff($branch_id)
    {
        $branch = Branch::with(['users'])->findOrFail($branch_id);

        return view('admin.branches.staff')
            ->with('branch', $branch);
    }
}
