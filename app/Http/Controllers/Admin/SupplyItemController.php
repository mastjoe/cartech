<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Supply;
use App\Models\Product;
use App\Models\SupplyItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class SupplyItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Supply $supply
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Supply $supply)
    {
        $request->validate([
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required|numeric|min:1',
            'product_id'    => 'required'
        ], [], [
            'product_id' => 'product',
        ]);

        
        try {
            // check if supply already has a product
            $productExists = $supply->items()
                ->where('product_id', $request->product_id)
                ->exists();
            
            if ($productExists) {
                return response()->json([
                    'success' => false,
                    'message' => 'product is already taken',
                ], 400);
            }

            $request->request->add(['quantity_supplied' => $request->quantity]);
            
            $item = $supply->items()->create($request->all());

            $msg = [
                'title'   => 'Items Added',
                'message' => 'item was successfully added to supply '.$supply->reference,
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => 'item was successfully added to supply '.$supply->reference,
                'data'    => $item
            ]);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'item was not added! Try again'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Supply $supply
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Supply $supply, $id)
    {
        $products = Product::all();
        $supplyItem = SupplyItem::with(['supply', 'supply.supplier', 'product', 'product.productType'])
            ->findOrFail($id);

        return view('admin.supplies.items.show')
            ->with('supply_item', $supplyItem)
            ->with('products', $products);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param Supply $supply
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supply $supply, $id)
    {
        // return $request->all();
        $request->validate([
            'cost_price'    => 'required',
            'selling_price' => 'required',
            'quantity'      => 'required|numeric',
            'product_id'    => 'required'
        ], [], [
            'product_id' => 'product',
        ]);

        $supplyItem = SupplyItem::findOrFail($id);

        try {
            $supplyItem->update($request->except('_token'));

            $msg = [
                'title' => 'Supply Item Updated',
                'message' => 'supply item was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'updated failed! Try again',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Supply $supply
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supply $supply, $id)
    {
        $supplyItem = SupplyItem::findOrFail($id);

        if ($supplyItem->canDelete()) {
            $supplyItem->delete();
            
            $msg = [
                'title' => 'Supply Item Deleted',
                'message' => 'supply item was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.supplies.show', $supply->id),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'deletion failed! Try again'
        ], 500);
    }
}
