<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\VehicleFuel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class VehicleFuelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fuels = VehicleFuel::all();

        return view('admin.vehicle-fuels.index')
            ->with('vehicle_fuels', $fuels);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('admin.vehicle-fuels.index')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:vehicle_fuels,name',
            'description' => 'nullable|string',
        ]);

        try {
            //add status_id to request
            $request->request->add(['status_id' => Util::getStatusId('active')]);

            $fuel = VehicleFuel::create($request->only('name', 'description', 'status_id'));

            $msg = [
                'title'   => 'New Fuel Added',
                'message' => 'New fuel, '.$fuel->name.' was successfully added'
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.vehicle-fuels.show', $fuel->id),
            ]);

        } catch (Exception $ex) {
            // log error
            Log::error($ex->getMessage());

            return response()->json([
                'success' => 'false',
                'message' => 'fuel was not added try again'
            ]);
        }

    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fuel = VehicleFuel::findOrFail($id);

        return view('admin.vehicle-fuels.show')
            ->with('vehicle_fuel', $fuel);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('admin.vehicle-fuels.show', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:vehicle_fuels,name,'.$request->name.',name',
            'description' => 'nullable|string',
        ]);

        $fuel = VehicleFuel::findOrFail($id);

        try {
            $fuel->update($request->only('name', 'description'));
            $msg = [
                'title'   => 'Fuel Updated',
                'message' => 'fuel, '.$fuel->name.' was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => 'fuel successfully updated'
            ]);

        } catch (Exception $ex) {
            //log error
            Log::error($ex->getMessage());
            // send response
            return response()->json([
                'success' => false,
                'message' => 'vehicle fuel was not updated! Try again'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fuel = VehicleFuel::findOrFail($id);

        if($fuel->canDelete()) {
            $fuel->delete();
            $msg = [
                'title' => 'Vehicle Fuel Deleted',
                'message' => 'Vehicle fuel, '.$fuel->name.' was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.vehicle-fuels.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'operation failed! Try again'
        ], 400);
    }
}
 