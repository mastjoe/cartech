<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\JobOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\JobOrderFaultItem;

class JobOrderFaultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($jobOrderId)
    {
        $jobOrder = JobOrder::with('faultItems')->findOrFail($jobOrderId);

        return view('admin.job-orders.faults.index')
            ->with('job_order', $jobOrder);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function create($jobOrderId)
    {
        return redirect()->route('admin.job-orders.faults.index', $jobOrderId)
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $jobOrderId)
    {
        $request->validate([
            'fault' => 'required|string|max:191',
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {

            $fault = $jobOrder->faultItems()->create($request->only('fault'));
            $msg = [
                'title' => 'Fault Addedd',
                'message' => "fault successfully added",
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);
        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Oops! Something went wrong! Try again!',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $jobOrderId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jobOrderId, $id)
    {
        $request->validate([
            'fault' => 'required|string|max:191',
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);
        $jobOrderFault = JobOrderFaultItem::findOrFail($id);

        try {
            $jobOrderFault->update($request->only('fault'));

            $msg = [
                'title' => 'Fault Updated',
                'message' => 'fault was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],

            ]);
        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Oops! something went wrong! Try again later',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $jobOrderId
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($jobOrderId, $id)
    {
        $jobOrderFault  = JobOrderFaultItem::findOrFail($id);

        try {
            $jobOrderFault->delete();

            $msg = [
                'title' => 'Fault Deleted',
                'message' => 'fault was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
                'redirect' => route('admin.job-orders.faults', $jobOrderId),
            ]);

        } catch (Exception $ex) {
            //
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'fault was not deleted! Try again later',
            ], 500);
        }
    }
}
