<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CustomerRepository;

class CustomerController extends Controller
{
    public $customer_repo;

    public function __construct(CustomerRepository $repo)
    {
        $this->customer_repo = $repo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = $this->customer_repo->getAllCustomers();

        return view('admin.customers.index')
            ->with('customers', $customers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($customer = $this->customer_repo->create($request)) {
            $msg = [
                'title'   => 'Customer Record Created',
                'message' => 'customer, '.$customer->name.' was successfully created'
            ];

            Util::flash('success', $msg);

            return redirect()->route('admin.customers.show', $customer->id);
        }

        return redirect()->back()->withInput()->with('error', 'Record not created! Try again');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customer = Customer::findOrFail($id);
        
        return view('admin.customers.show')
            ->with('customer', $customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        return view('admin.customers.edit')
            ->with('customer', $customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        if ($this->customer_repo->update($request, $customer)) {
            $msg = [
                'title' => 'Customer Updated',
                'message' => 'customer update was successful',
            ];

            return redirect()->route('admin.customers.show', $customer->id)
                ->with('success', $msg);
        }

        return redirect()->back()->with('error', 'Update failed! Try again');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);
        if ($customer->canDelete()) {
            $customer->delete();
            $msg = [
                'title' => 'Customer Deleted',
                'message' => 'customer, '.$customer->name.' was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
                'redirect' => route('admin.customers.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'customer was not deleted! Try again'
        ]);
    }
}
