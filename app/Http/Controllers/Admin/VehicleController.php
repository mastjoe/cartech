<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Vehicle;
use App\Models\Customer;
use App\Models\VehicleFuel;
use App\Models\VehicleMake;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Repositories\VehicleRepository;
use App\Repositories\CustomerRepository;

class VehicleController extends Controller
{
    public $vehicleRepo;
    public $customerRepo;

    public function __construct(VehicleRepository $vehicleRepo, CustomerRepository $customerRepo)
    {
        $this->vehicleRepo = $vehicleRepo;
        $this->customerRepo = $customerRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::orderByDesc('id')->get();

        return view('admin.vehicles.index')
            ->with('vehicles', $vehicles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * 
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $customers = Customer::all();

        if (!$customers->count()) {
            return redirect()->route('admin.customers.create')
                ->with('warning', 'Create a customer to add a vehicle record');
        }
        
       
        $vehicle_makes = VehicleMake::all();
        $vheicle_fuels = VehicleFuel::all();

        return view('admin.vehicles.create')
            ->with('vehicle_fuels', $vheicle_fuels)
            ->with('vehicle_makes', $vehicle_makes)
            ->with('customers', $customers)
            ->with('customer_id', $request->customer ?? null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // remove unwanted keys
        $request->request->remove('branch');
        // find customer
        $customer = $this->customerRepo->findById($request->customer_id);

        if ($vehicle = $this->vehicleRepo->store($request, $customer)) {
            $msg = [
                'title'   => 'Vehicle Record Created',
                'message' => 'vehicle with number '.$vehicle->plate_number.' was successfully created'
            ];

            return redirect()->route('admin.vehicles.show', $vehicle->id)
                ->with('success', $msg);
        }

        return redirect()->back()->withInput()->with('error', 'Vehicle was not created! Try again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::with(['customer', 'make', 'fuel'])->findOrFail($id);

        return view('admin.vehicles.show')
            ->with('vehicle', $vehicle);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::findOrFail($id);

        $vehicle_makes = VehicleMake::all();
        $vehicle_fuels = VehicleFuel::all();
        $customers = Customer::all();

        return view('admin.vehicles.edit')
            ->with('vehicle', $vehicle)
            ->with('vehicle_makes', $vehicle_makes)
            ->with('vehicle_fuels', $vehicle_fuels)
            ->with('customers', $customers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehicle = Vehicle::findOrFail($id);
        try {
            $this->vehicleRepo->update($request, $vehicle);

            $msg = [
                'title' => 'Vehicle Updated',
                'message' => 'Vehicle was successfully updated',
            ];

            return redirect()->route('admin.vehicles.show', $vehicle->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error', 'Oops! something went wrong! Try again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicle::findOrFail($id);

        if ($vehicle->canDelete()) {
            $vehicle->delete();

            $msg = [
                'title'   => 'Vehicle Deleted',
                'message' => 'vehicle with plate number, '.$vehicle->plate_number.' was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
                'redirect' => route('admin.vehicles.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'deletion of vehicle failed! Try again!',
        ], 400);
    }
}
