<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Branch;
use App\Models\Supply;
use App\Models\Product;
use App\Models\Vehicle;
use App\Models\Customer;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class DashboardController extends Controller
{
    public function index()
    {
        $branchesCount  = Branch::count();
        $customersCount = Customer::count();
        $suppliersCount = Supplier::count();
        $suppliesCount  = Supply::count();
        $usersCount     = User::count();
        $vehiclesCount  = Vehicle::count();
        $productsCount  = Product::count();

        $userRepo = new UserRepository;

        $branchUsersCount = $userRepo->getUsersInCurrentBranch()->count();

        return view('admin.dashboard.index')
            ->with('branchesCount', $branchesCount)
            ->with('customersCount', $customersCount)
            ->with('suppliersCount', $suppliersCount)
            ->with('suppliesCount', $suppliesCount)
            ->with('usersCount', $usersCount)
            ->with('vehiclesCount', $vehiclesCount)
            ->with('productsCount', $productsCount)
            ->with('branchUsersCount', $branchUsersCount);
    }
}
