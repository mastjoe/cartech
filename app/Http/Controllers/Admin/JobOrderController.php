<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use Exception;
use App\Models\Vehicle;
use App\Models\JobOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Repositories\JobOrderRepository;
use App\Repositories\UserRepository;

class JobOrderController extends Controller
{
    public $jobOrderRepo;
    public $userRepo;

    public function __construct(JobOrderRepository $jobOrderRepo, UserRepository $userRepo)
    {
        $this->jobOrderRepo = $jobOrderRepo;
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', JobOrder::class);

        $jobOrders = $this->jobOrderRepo->getJobOrders();

        return view('admin.job-orders.index')
            ->with('job_orders', $jobOrders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicles = Vehicle::with('customer')->get();

        return view('admin.job-orders.create')
            ->with('vehicles', $vehicles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $jobOrder = $this->jobOrderRepo->store($request);
            
            $msg = [
                'title' => 'Job Order Created',
                'message' => 'new job order with reference '.$jobOrder->reference.' was successfully created',
            ];
            return redirect()->route('admin.job-orders.show', $jobOrder->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return redirect()->back()->with('error', 'job order was not created! Try again!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $jobOrder = JobOrder::with(['vehicle', 'customer'])->findOrFail($id);
        $branchUsers = $this->userRepo->getUsersInCurrentBranch();

        return view('admin.job-orders.show')
            ->with('job_order', $jobOrder)
            ->with('branch_users', $branchUsers);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jobOrder = JobOrder::with(['customer', 'vehicle'])->findOrFail($id);
        $this->authorize('update', $jobOrder);

        $vehicles = Vehicle::with('customer')->get();

        return view('admin.job-orders.edit')
            ->with('job_order', $jobOrder)
            ->with('vehicles', $vehicles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jobOrder = JobOrder::findOrFail($id);

        try {
            $this->jobOrderRepo->update($request, $jobOrder);
            $msg = [
                'title'   => 'Job Order Updated',
                'message' => 'job order, '.$jobOrder->reference.' was successfully updated',
            ];

            return redirect()->route('admin.job-orders.show', $jobOrder->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error', 'Oops! seems something went wrong! Try again later!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobOrder = JobOrder::findOrFail($id);

        if ($jobOrder->canDelete()) {
            $jobOrder->delete();
            $msg = [
                'title' => 'Job Order Deleted',
                'message' => 'Job order, '.$jobOrder->reference.' was successfully deleted', 
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
                'redirect' => route('admin.job-orders.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'something went wrong! Try again',
        ], 400);
    }

    /**
     * assign manager to job order
     *
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function assignManager(Request $request, $jobOrderId)
    {
        $request->validate([
            'manager' => 'required',
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {
            $jobOrder->update(['manager_id' => $request->manager]);

            $msg = [
                'title'   => 'Manager Assigned',
                'message' => 'Job Order, '.$jobOrder->reference.' was successfully assigned to '.$jobOrder->manager->full_name,
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'status' => false,
                'message' => 'Oops! Something went wrong! Try again',
            ], 500);
        }
    }
}
