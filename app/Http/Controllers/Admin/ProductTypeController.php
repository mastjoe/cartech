<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productTypes = ProductType::all();

        return view('admin.product-types.index')
            ->with('product_types', $productTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('admin.product-type.index')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:product_types,name',
            'description' => 'nullable|string',
        ]);

        try {
            //code...
            $type = ProductType::create($request->all());

            $msg = [
                'title'   => 'Product Type Created',
                'message' => 'Product type, '.$type->name.' was successfully created',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => 'product type was successfully created',
                'data' => $type,
            ]);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'success' => 'false',
                'message' => 'product type creation failed! Try again'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function show(ProductType $productType)
    {
        return view('admin.product-types.show')->with('product_type', $productType);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  productType  $productType
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductType $productType)
    {
        return redirect()->route('admin.product-type.show', $productType)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:product_types,name,'.$request->name.',name',
            'description' => 'nullable|string',
        ]);

        $productType = ProductType::find($id);

        try {
            $productType->update($request->all());

            $msg = [
                'title' => 'Product Type Updated',
                'message' => 'product '.$productType->name.' was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());
            
            return response()->json([
                'success' => false,
                'message' => 'update failed! try again!',
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $productType = ProductType::find($id);

        if ($productType->canDelete()) {
            $productType->delete();

            $msg = [
                'title' => 'Product Type Deleted',
                'message' => 'product type, '.$productType->name.' was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.product-type.index')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'product type was not deleted try again'
        ], 400);
    }
}
