<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Role;
use App\Models\Privilege;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();

        return view('admin.roles.index')
            ->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $privileges = Privilege::all();

        return view('admin.roles.create')
            ->with('privileges', $privileges);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:roles,name',
            'description' => 'nullable|string',
            'privileges' => 'required'
        ]);

        $role = Role::create($request->only('name', 'description'));
        $role->privileges()->attach($request->privileges);
        
        $msg = [
            'title'   => 'Role Created',
            'message' => 'Role '.$role->name.' was successfully created',
        ];

        return redirect()->route('admin.roles.show', $role->id)
            ->with('success', $msg);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::with(['privileges', 'users'])->findOrFail($id);

        return view('admin.roles.show')
            ->with('role', $role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::with(['privileges'])->findOrFail($id);
        $privileges = Privilege::all();

        return view('admin.roles.edit')
            ->with('role', $role)
            ->with('privileges', $privileges);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:roles,name,'.$request->name.',name',
            'description' => 'nullable|string',
            'privileges'  => 'required'
        ]);

        $role = Role::findOrFail($id);

        try {
            
            $role->update($request->only('name', 'description'));
            $role->privileges()->sync($request->privileges);

            $msg = [
                'title'   => 'Role Updated',
                'message' => 'Role '.$role->name.' was successfully updated',
            ];

            return redirect()->route('admin.roles.show', $id)
                ->with('success', $msg);

        } catch (Exception $ex) {  
            // on update failure   
            Log::error($ex->getMessage());

            $msg = [
                'title'   => 'Role Update Failed',
                'message' => 'Role update was not successful! Try again',
            ];

            return redirect()->back()
                ->with('error', $msg);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);

        if ($role->canDelete()) {
            $role->privileges()->detach();
            $role->delete();

            $msg = [
                'title'   => 'Role Deleted',
                'message' => 'Role '.$role->name.' was successfully deleted',
            ];

            request()->session()->flash('success', $msg);
            return response()->json([
                'success'  => true,
                'redirect' => route('admin.roles.index'),
                'message'  => $msg['message'],
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Role '.$role->name.' was not deleted! Try again'
        ]);

    }
}
