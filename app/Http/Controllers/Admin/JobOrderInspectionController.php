<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\JobOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\JobOrderInspectionItem;
use App\Repositories\UserRepository;

class JobOrderInspectionController extends Controller
{
    public $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * get job order inspection record
     *
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function index($jobOrderId)
    {
        $jobOrder = JobOrder::with(['vehicle', 'customer', 'inspectionItems'])
            ->findOrFail($jobOrderId);
        $branchUsers = $this->userRepo->getUsersInCurrentBranch();

        return view('admin.job-orders.inspection.index')
            ->with('job_order', $jobOrder)
            ->with('branch_users', $branchUsers);
    }

    /**
     * updates inspection details
     *
     * @param Request $request
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jobOrderId)
    {
        $request->validate([
            'note'      => 'required',
            'inspector' => 'required',
        ]);


        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {
            $jobOrder->update([
                'inspection_note' => $request->note,
                'inspected_by'    => $request->inspector,
            ]);

            $msg = [
                'title' => 'Inspection Updated',
                'message' => 'Inspection on job order, '.$jobOrder->reference.' successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'something went wrong! Try again!',
            ], 400);
        }
    }


    /**
     * add a result Item for inspection
     *
     * @param Request $request
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function addItem(Request $request, $jobOrderId)
    {
        $request->validate([
            'result' => 'required',
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {
            $jobOrder->inspectionItems()->create($request->only('result'));

            $msg = [
                'title' => 'Inspection Item Added',
                'message' => 'inspection result was successfully added',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'item was not added! Try again!',
            ], 500);
        }
    }

    public function removeItem($jobOrderId, $id)
    {
        try {
            $jobOrder = JobOrder::findOrFail($jobOrderId);

            $item = JobOrderInspectionItem::findOrFail($id);

            $item->delete();

            $msg = [
                'title' => 'Item Removed',
                'message' => 'inspection result was successfull removed',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
                'redirect' => route('admin.job-orders.inspection', $jobOrderId)
            ]);
        } catch (Exception $ex) {
            //
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'something went wrong! Try again',
            ]);
        }
    }

    public function updateItem(Request $request, $jobOrderId, $id)
    {
        $request->validate([
            'result' => 'required|string',
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {
            $item = JobOrderInspectionItem::findOrFail($id);

            $item->update($request->only('result'));
            $msg = [
                'title' => 'Inspection Result Updated',
                'message' => 'inspection result was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);
            
        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Oops! seems something went wrong! Try again!'
            ]);
        }
    }
}
