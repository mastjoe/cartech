<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\User;
use App\Helpers\Util;
use App\Models\UserNok;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class StaffNokController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param User $staff
     * @return \Illuminate\Http\Response
     */
    public function index(User $staff)
    {
        return view('admin.staff.noks.index')
            ->with('user', $staff);
    }

    /**
     * Show the form for creating a new resource.
     * @param User $staff
     * @return \Illuminate\Http\Response
     */
    public function create(User $staff)
    {
        return view('admin.staff.noks.create')
            ->with('user', $staff);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param User $staff
     * 
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $staff)
    {
        $request->validate([
            'first_name' => 'required|string|max:191',
            'last_name'  => 'required|string|max:191',
            'email'      => 'nullable|string|max:191|email',
            'phone'      => 'required|digits_between:7,16',
            'occupation' => 'required|string',
            'address'    => 'required|string',
            'image'      => 'nullable|image|mimes:jpg,jpeg,png,gif|max:500',
        ]);

        try {

            $user_nok = $staff->noks()->create($request->all());

            $this->handleImageUpload($request, $user_nok);

            $msg = [
                'title'   => 'Next of Kin Added',
                'message' => 'Next of Kin was successfully added',
            ];

            return redirect()->route('admin.staff.noks.show',[$staff->id, $user_nok->id])
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            $msg = [
                'title'   => 'Oops',
                'message' => 'Next of kin creation failed',
            ];
            return redirect()->back()
                ->with('error', $msg)
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param User $staff
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $staff, $id)
    {
        $nok = UserNok::with(['user'])->findOrFail($id);

        if ($nok->user_id != $staff->id) abort(404);

        return view('admin.staff.noks.show')
            ->with('user', $nok->user)
            ->with('nok', $nok);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $staff
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $staff, $id)
    {
        $nok = UserNok::findOrFail($id);
        if ($staff->id != $nok->user_id) abort(404);

        return view('admin.staff.noks.edit')
            ->with('user', $staff)
            ->with('nok', $nok);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param User $staff
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $staff, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:191',
            'last_name'  => 'required|string|max:191',
            'email'      => 'nullable|string|max:191|email',
            'phone'      => 'required|digits_between:7,16',
            'occupation' => 'required|string',
            'address'    => 'required|string',
            'image'      => 'nullable|image|mimes:jpg,jpeg,gif,png|max:500'
        ]);

        $nok = UserNok::findOrFail($id);

        try {

            $nok->update($request->all());
            $this->handleImageUpload($request, $nok);

            $msg = [
                'title'   => 'Next of Kin Updated',
                'message' => 'Next of kin was successfully updated',
            ];

            return redirect()->route('admin.staff.noks.show', [$staff->id, $id])
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            $msg = [
                'title'   => 'Update Failed',
                'message' => 'Next of Kin was not updated',
            ];

            return redirect()->back()
                ->withInput()
                ->with('error', $msg);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  UserNok  $nok
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserNok $nok)
    {
        if ($nok->delete()) {
            Util::flash('success', 'Next of Kin was successfully deleted');

            return response()->json([
                'success' => true,
                'message' => 'Next of Kin successfully deleted'
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'next of kin delete failed! Try again'
        ]);
    }

    /**
     * handles the upload of image
     *
     * @param Request $request
     * @param UserNok $nok
     * @return void
     */
    protected function handleImageUpload(Request $request, UserNok $nok)
    {
        if ($request->hasFile('image')) {
            $nok->addMedia($request->image)->toMediaCollection('avatar');
        }
    }
}
