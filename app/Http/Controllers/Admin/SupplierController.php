<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::orderByDesc('id')->get();

        return view('admin.suppliers.index')
            ->with('suppliers', $suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'    => 'required|string|max:191|unique:suppliers,name',
            'phone'   => 'required|string|max:20',
            'email'   => 'nullable|string|max:100|email',
            'website' => 'nullable|url|string|max:191',
            'address' => 'required|string',
        ]);

        try {
            $supplier = Supplier::create($request->all());
            $msg = [
                'title' => 'Supplier Created',
                'message' => 'supplier account was successfully created',
            ];

            return redirect()->route('admin.suppliers.show', $supplier->id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            // log error
            Log::error($ex->getMessage());

            $msg = [
                'title' => 'Failed Operation',
                'message' => 'supplier not created! Try again!'
            ];

            return redirect()->back()->withInput()->with('error', $msg);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);

        return view('admin.suppliers.show')
            ->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::findOrFail($id);

        return view('admin.suppliers.edit')
            ->with('supplier', $supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'    => 'required|string|max:191|unique:suppliers,name,'.$request->name.',name',
            'phone'   => 'required|string|max:20',
            'email'   => 'nullable|string|max:100|email',
            'website' => 'nullable|url|string|max:191',
            'address' => 'required|string',
        ]);

        $supplier = Supplier::findOrFail($id);

        try {
            $supplier->update($request->all());

            $msg = [
                'title' => 'Supplier Updated',
                'message' => 'suppliers record was successfully updated'
            ];

            return redirect()->route('admin.suppliers.show', $id)
                ->with('success', $msg);

        } catch (Exception $ex) {
            // log error
            Log::error($ex->getMessage());

            $msg = [
                'title' => 'Update Failed',
                'message' => 'update operation failed! Try again'
            ];

            return redirect()->back()->withInput()->with('error', $msg);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $supplier = Supplier::findOrFail($id);

        if ($supplier->canDelete()) {
            $supplier->delete();
            $msg = [
                'title'   => 'Supplier Deleted',
                'message' => 'supplier, '.$supplier->name.' was successfully deleted',
            ];

            Util::flash('success', $msg);
            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.suppliers.index'),
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'supplier was not deleted! Try again'
        ], 400);
    }
}
