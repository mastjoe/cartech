<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Product;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $productTypes = ProductType::all();

        return view('admin.products.index')
            ->with('products', $products)
            ->with('product_types', $productTypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('admin.products.index')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'code'            => 'required|string|max:20|unique:products,code',
            'name'            => 'required|string|max:191|unique:products,name',
            'description'     => 'nullable|string',
            'product_type_id' => 'required',
            'images.*'        => 'nullable|image|mimes:png,jpeg,jpg,svg,gif',
        ], [], [
            'product_type_id' => 'product type',
        ]);

        
        
        try {
            $product = Product::create($request->except('images'));

            $this->handleImageUpload($request, $product);
    
            $msg = [
                'title'   => 'Product Created',
                'message' => 'Product, '.$product->name.' was successfully created',
            ];
            
            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'product was not created! Try again later'
            ], 500);
        }
    }

    protected function handleImageUpload(Request $request, Product $product)
    {
        if ($request->has('images')) {
            foreach ($request->images as $image) {
                $product->addMedia($image)->toMediaCollection('gallery');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $productTypes = ProductType::all();

        return view('admin.products.show')
            ->with('product', $product)
            ->with('product_types', $productTypes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('admin.products.show', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'code'            => 'required|string|max:20|unique:products,code,'.$request->code.',code',
            'name'            => 'required|string|max:191|unique:products,name,'.$request->name.',name',
            'description'     => 'nullable|string',
            'product_type_id' => 'required',
        ], [], [
            'product_type_id' => 'product type',
        ]);

        try {
            $product = Product::find($id);

            $product->update($request->all());

            $msg = [
                'title'   => 'Product Updated',
                'message' => 'Product, '.$product->name.' was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            
            Log::error($ex->getMessage());

            return response()->json([
                'success' => true,
                'message' => 'product was not updated! Try again'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if ($product->canDelete()) {
            $product->delete();
            $msg = [
                'title' => 'Product Deleted',
                'message'=> 'Product, '.$product->name.' was successfully deleted',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'deletion of product failed! Try again',
        ], 500);
    }
}
