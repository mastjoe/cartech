<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Util;
use App\Models\VehicleMake;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleMakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicle_makes = VehicleMake::all();

        return view('admin.vehicle-makes.index')
            ->with('vehicle_makes', $vehicle_makes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('admin.vehicle-makes.index')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:191|unique:vehicle_makes,name',
            'description' => 'nullable|string',
        ]);

        $make = VehicleMake::create($request->only('name', 'description'));

        $msg = [
            'title'   => 'Vehicle Make Created',
            'message' => 'vehicle make, '.$make->name.' was successfully created',
        ];

        Util::flash('success', $msg);

        return response()->json([
            'success' => true,
            'message' => $msg['message']
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle_make = VehicleMake::findOrFail($id);

        return view('admin.vehicle-makes.show')
            ->with('vehicle_make', $vehicle_make);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('admin.vehicle-makes.show', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:vehicle_makes,name,'.$request->name.',name',
            'description' => 'nullable|string'
        ]);

        $make = VehicleMake::findOrFail($id);
        $make->update($request->only('name', 'description'));

        $msg = [
            'title'   => 'Vehicle Make Updated',
            'message' => 'vehicle make was successfully updated',
        ];

        Util::flash('success', $msg['message']);

        return response()->json([
            'success' => true,
            'message' => $msg['message']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $make = VehicleMake::findOrFail($id);

        if ($make->canDelete()) {
            $make->delete();

            $msg = [
                'title'   => 'Vehicle Make Deleted!',
                'message' => 'vehicle make, '.$make->name.' was successfully deleted'
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success'  => true,
                'message'  => $msg['message'],
                'redirect' => route('admin.vehicle-makes.index')
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'something went wrong! Try again'
        ]);
    }
}
