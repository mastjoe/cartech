<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Models\Role;
use App\Models\User;
use App\Helpers\Util;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class StaffRoleController extends Controller
{
    /**
     * show staff roles and privileges
     *
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function index($userId)
    {
        $user = User::with(['roles', 'privileges'])->findOrFail($userId);

        return view('admin.staff.roles.index')
            ->with('user', $user);
    }

    public function edit($userId)
    {
        $user = User::with(['roles', 'privileges'])->findOrFail($userId);
        $roles = Role::all();
        $privileges = Util::getUniquePrivileges($user->roles->pluck('id')->toArray());
        // return Util::sortPrivilegesByCategory($privileges);

        return view('admin.staff.roles.edit')
            ->with('user', $user)
            ->with('roles', $roles)
            ->with('privileges', $privileges);
    }

    /**
     * update staff roles and privileges
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $userId)
    {
        $request->validate([
            'roles' => 'required',
        ]);

        $user = User::findOrFail($userId);

        try {
            $user->updateRoles($request->roles);
            $msg = [
                'title' => 'Roles Updated',
                'message' => 'role was successfully updated for '.$user->full_name,
            ];
            
            return redirect()->back()->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error', 'something went wrong! Try again later!');
        }
    }

    /**
     * update privileges
     *
     * @param Request $request
     * @param int $userId
     * @return \Illuminate\Http\Response
     */
    public function updatePrivileges(Request $request, $userId)
    {
        $request->validate([
            'privileges' => 'required',
        ]);

        $user = User::findOrFail($userId);

        try {
            $user->privileges()->sync($request->privileges);

            $msg = [
                'title' => 'Privileges Updated',
                'message' => 'privilege was successfully updated',
            ];

            return redirect()->back()
                ->with('success', $msg);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return redirect()->back()->withInput()
                ->with('error','Seems something went wrong! Try again later');
        }
    }
}
