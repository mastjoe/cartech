<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Privilege;
use Illuminate\Http\Request;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $privileges = Privilege::all();

        return view('admin.privileges.index')
            ->with('privileges', $privileges);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect()->route('admin.privileges.index')
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required|string|max:191|unique:privileges,name',
            'description' => 'nullable|string'
        ]);

        $privilege = Privilege::create($request->only('name', 'description'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $privilege = Privilege::with(['roles', 'users'])->findOrFail($id);

        return view('admin.privileges.show')
            ->with('privilege', $privilege);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect()->route('admin.privileges.show', $id)
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'        => 'required',
            'description' => 'nullable|string',
        ]);

        $privilege = Privilege::findOrFail($id);

        $privilege->update($request->only('description'));

        $msg = [
            'title'   => 'Privilege Updated',
            'message' => 'Privilege was successfully updated',
        ];

        $request->session()->flash('success', $msg);

        return response()->json([
            'success' => true,
            'message' => $msg['message'],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $privilege = Privilege::findOrFail($id);

        if ($privilege->canDelete()) {
            $privilege->delete();

            $msg = [
                'title'   => 'Privilege Deleted!',
                'message' => 'Privilege "'.$privilege->name.'" was successfully deleted',
            ];

            request()->session()->flash('success', $msg);

            return response()->json([
                'success'  => true,
                'redirect' => route('admin.privileges.index'),
                'message'  => $msg['message'],
            ]);
        }
    }
}
