<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Helpers\Util;
use App\Models\Service;
use App\Models\JobOrder;
use Illuminate\Http\Request;
use App\Models\JobOrderService;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class JobOrderServiceController extends Controller
{
    public $userRepo;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function index($jobOrderId)
    {
        $jobOrder = JobOrder::findOrFail($jobOrderId);
        $services = Service::all();
        $branchUsers = $this->userRepo->getUsersInCurrentBranch();

        return view('admin.job-orders.services.index')
            ->with('job_order', $jobOrder)
            ->with('services', $services)
            ->with('branch_users', $branchUsers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function create($jobOrderId)
    {
        return redirect()->route('admin.job-orders.services', $jobOrderId)
            ->with('create', true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $jobOrderId
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $jobOrderId)
    {
        $request->validate([
            'note'              => 'nullable|string',
            'outsource_company' => 'required_with:outsourced',
            'service'           => 'required|unexist:job_order_services,service_id'
        ]);

        $jobOrder = JobOrder::findOrFail($jobOrderId);

        try {
            
            $service = $jobOrder->services()->create([
                'service_id'        => $request->service,
                'outsourced'        => $request->outsourced ?? 0,
                'note'              => $request->note,
                'outsource_company' => $request->outsource_company ?? null,
                'managed_by'        => $request->manager,
            ]);
            
            $msg = [
                'title'   => 'Service Added',
                'message' => 'service, '.$service->service->name.' was successfully added',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {
            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Oops! something went wrong! Try again later',
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $jobOrderId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($jobOrderId, $id)
    {
        $jobOrder = JobOrder::findOrFail($jobOrderId);
        $service = JobOrderService::findOrFail($id);
        $services = Service::all();
        $branchUsers = $this->userRepo->getUsersInCurrentBranch();

        return view('admin.job-orders.services.show')
            ->with('job_order', $jobOrder)
            ->with('service', $service)
            ->with('services', $services)
            ->with('branch_users', $branchUsers);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $jobOrderId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($jobOrderId, $id)
    {
        return redirect()->route('admin.job-orders.services.show', [$jobOrderId, $id])
            ->with('edit', true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $jobOrderId
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $jobOrderId, $id)
    {
        $request->validate([
            'service'           => 'required',
            'note'              => 'nullable|string',
            'outsource_company' => 'required_with:outsourced',
        ]);
        
        try {
            $jobOrder = JobOrder::findOrFail($jobOrderId);

            $service = JobOrderService::findOrFail($id);

            $service->update([
                'service_id'        => $request->service,
                'outsourced'        => $request->outsourced ?? 0,
                'note'              => $request->note,
                'outsource_company' => $request->outsource_company ?? null,
                'managed_by'        => $request->manager,
            ]);

            $msg = [
                'title'   => 'Job Order Service Updated',
                'message' => 'job order service, '.$service->service->name.' was successfully updated',
            ];

            Util::flash('success', $msg);

            return response()->json([
                'success' => true,
                'message' => $msg['message'],
            ]);

        } catch (Exception $ex) {

            Log::error($ex->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Oops! something went wrong! Try again!'
            ], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $jobOrderId
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($jobOrderId, $id)
    {
        $jobOrderId = JobOrder::findOrFail($jobOrderId);
        $jobOrderService = JobOrderService::findOrFail($id);
        $errorMsg = "Oops! something went wrong! Try again!";

        if ($jobOrderService->canDelete()) {
            try {
                $jobOrderService->delete();

                $msg = [
                    'title'   => 'Job Order Service Deleted',
                    'message' => 'job order service was successfully deleted',
                ];

                Util::flash('success', $msg);

                return response()->json([
                    'success'  => true,
                    'message'  => $msg['message'],
                    'redirect' => route('admin.job-orders.services', $jobOrderId),
                ]);

            } catch (Exception $ex) {
                Log::error($ex->getMessage());

                return response()->json([
                    'success' => false,
                    'message' => $errorMsg,
                ], 500);
            }
        }

        return response()->json([
            'success' => false,
            'message' => $errorMsg,
        ], 400);
    }
}
